![NDescribe](https://bytebucket.org/kahsje/ndescribe/raw/98935f8fdc093e8f48eec064dd32ae87e299ca6f/Docs/NugetLogo.png)
### NDescribe ###

* NDescribe provides fluent interfaces for modelling data quickly and easily without having to duplicate domain model objects, and at the same time keeping a distinction between the two layers.
* Version 1.2.0

### How do I get set up? ###

NDescribe currently comes in three core Nuget packages:
	* NDescribe-Core - the core libraries, intended to be installed if you want to use your own custom data bridge
	* NDescribe-Couchbase - the Couchbase version of the library (currently the only supported nosql implementation)
	* NDescribe-InMemory - an in-memory Databridge, primarily used for offline testing, but can be used for very simple caching

To get started and used to the API, try out the NDescribe-InMemory API.

#### Basic usage ####
TODO
#### Couchbase implementation ####
TODO
##### Direct databridge provider #####
TODO
##### File-based databridge provider #####	
TODO
#### Test details ####
TODO
### Contribution guidelines ###

Feel free to contribute.

All pull requests will be code reviewed and may be rejected.

### Who do I talk to? ###

* Feel free to raise an issue or contact me with suggestions. Email to be setup.
@echo off
setlocal enabledelayedexpansion

SET solution_name=NDescribe.sln

SET build_configuration=net45 net451 net452 net46 net461
SET nuspecs=NDescribe.Core\NDescribe.Core.nuspec NDescribe.Couchbase\NDescribe.Couchbase.nuspec NDescribe.InMemory\NDescribe.InMemory.nuspec

REM USE QUOTES ALWAYS <----
SET msbuild_fallback="C:\Program Files (x86)\MSBuild\14.0\Bin\MSBuild.exe"

echo.
echo Starting.
echo.

SET mypath=%~dp0
echo Running build.bat from %mypath:~0,-1%

echo Looking for MSBuild in PATH

for %%X in (MSBuild.exe) do (SET FOUND_A=%%~$PATH:X)
if defined FOUND_A (
	echo MSBuild.exe is in PATH
	SET msbuild="%FOUND_A%"
) ELSE (
	echo MSBuild.exe is in not in PATH
	echo Fallback to default MSBuild.exe location
	SET msbuild=%msbuild_fallback%
	echo Looking for MSBuild in: %msbuild_fallback%
)

IF EXIST %msbuild:~1,-1% (
	echo MSBuild found.

	SET solution=%mypath:~0,-1%\%solution_name%
	echo Looking Solution file: %solution%

	IF EXIST %solution% (
		echo Solution file found.

		echo.
		echo Building.
		echo.
		@echo on
		@for %%A in (%build_configuration%) do (
			%msbuild% "%solution%" /m /p:Configuration=%%A /v:m
		)
		@echo off

		SET version=-Version %APPVEYOR_BUILD_VERSION%

		if defined dst_folder (
			cd !dst_folder!
		)
	
		@echo on
	
		@for %%P in (%nuspecs%) do (
			NuGet.exe pack "%mypath:~0,-1%\%%P" -OutputDirectory "%mypath:~0,-1%\packages" !version!
		)

		@echo off
		EXIT /B 0
	)
)
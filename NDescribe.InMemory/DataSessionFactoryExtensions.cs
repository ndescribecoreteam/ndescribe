﻿using System.Collections.Generic;
using NDescribe.Core;
using NDescribe.Core.Configuration;

namespace NDescribe.InMemory
{
    /// <summary>
    /// Extensions to create a session with a MemoryStoreConfigurationProvider
    /// </summary>
    public static class DataSessionFactoryExtensions
    {
        /// <summary>
        /// Create a session with the default options
        /// </summary>
        /// <param name="dataSessionFactory">Factory to use for creating a session</param>
        /// <param name="dataContexts">Data contexts to associate with this session</param>
        /// <returns>A data session created with the default options</returns>
        public static IDataSession CreateSession(this DataSessionFactory dataSessionFactory,
            IEnumerable<DataContext> dataContexts)
        {
            return dataSessionFactory.CreateSession(dataContexts,
                new ConfigurationOptions(new MemoryStoreConfigurationProvider()));
        }
    }
}

﻿using NDescribe.Core.DataBridge;

namespace NDescribe.InMemory
{
    /// <summary>
    /// Create an in-memory databridge for testing purposes
    /// </summary>
    public class MemoryStoreConfigurationProvider : IDataBridgeConfigurationProvider
    {
        /// <summary>
        /// Create an in-memory databridge
        /// </summary>
        /// <returns>An in-memory databridge</returns>
        public IDataBridge Create()
        {
            return new MemoryStoreDataBridge();
        }
    }
}
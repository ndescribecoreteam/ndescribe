﻿using System;
using System.Collections.Generic;
using System.Linq;
using NDescribe.Core.DataBridge;
using NDescribe.Core.DocumentGeneration.Traits;

namespace NDescribe.InMemory
{
    /// <summary>
    /// Data bridge for in-memory operations
    /// </summary>
    public class MemoryStoreDataBridge : IDataBridge
    {
        private readonly Dictionary<string, Dictionary<string, object>> dataStore =
            new Dictionary<string, Dictionary<string, object>>();

        /// <summary>
        /// Create a lock document
        /// </summary>
        /// <param name="dataLocation">Location to create the lock</param>
        /// <param name="key">Key to create the lock with</param>
        /// <param name="expiry">Expiry time for creating a lock</param>
        /// <param name="dataTraits">Any data traits for this document</param>
        /// <returns>Success of the operation and whether the document exists or not</returns>
        public DataRetrievalResult<bool> CreateLock(string dataLocation, string key, long expiry, IEnumerable<IDataTrait> dataTraits)
        {
            if (!dataStore.ContainsKey(dataLocation))
            {
                dataStore[dataLocation] = new Dictionary<string, object>();
            }

            if (dataStore[dataLocation].ContainsKey(key))
            {
                if ((DateTime) dataStore[dataLocation][key] > DateTime.UtcNow)
                {
                    return new DataRetrievalResult<bool>(false, true, string.Empty, null);
                }
            }

            dataStore[dataLocation][key] = DateTime.UtcNow.AddSeconds(expiry);

            return new DataRetrievalResult<bool>(true, true, string.Empty, null);
        }

        /// <summary>
        /// Delete the specified document
        /// </summary>
        /// <param name="dataLocation">Location of the document to delete</param>
        /// <param name="documentKey">Document key to delete</param>
        /// <param name="dataTraits">Any data traits for this document</param>
        /// <returns>Result of the operation</returns>
        public DataResult DeleteDocument(string dataLocation, string documentKey, IEnumerable<IDataTrait> dataTraits)
        {
            if (!dataStore.ContainsKey(dataLocation))
            {
                return new DataResult(false, $"Datastore not found: {dataLocation}",
                    null);
            }

            if (!dataStore[dataLocation].ContainsKey(documentKey))
            {
                return new DataResult(false,
                    $"Document not found (data location: {dataLocation}): {documentKey}",
                    null);
            }

            dataStore[dataLocation].Remove(documentKey);

            return new DataResult(true, string.Empty, null);
        }

        /// <summary>
        /// Attempt to get multiple documents
        /// </summary>
        /// <param name="dataLocation">Location of the data to retrieve</param>
        /// <param name="keys">Collection of keys to retrieve</param>
        /// <param name="traits">Any data traits for this document</param>
        /// <returns></returns>
        public DataRetrievalResult<IDictionary<string, DataRetrievalResult<string>>> GetDocuments(string dataLocation, IEnumerable<string> keys, IEnumerable<IDataTrait> traits)
        {
            if (!dataStore.ContainsKey(dataLocation))
            {
                dataStore[dataLocation] = new Dictionary<string, object>();
            }

            var data = from key in keys.AsParallel()
                let found = dataStore[dataLocation].ContainsKey(key)
                select new
                {
                    Key = key,
                    Value =
                        new DataRetrievalResult<string>(dataStore[dataLocation][key] as string, found,
                            found ? default(string) : $"Document not found (data location: {dataLocation}): {key}", null)
                };

            var dictionary = data.ToDictionary(x => x.Key, x => x.Value);

            return new DataRetrievalResult<IDictionary<string, DataRetrievalResult<string>>>(dictionary, true, string.Empty,
                null);
        }

        /// <summary>
        /// Get a counter value
        /// </summary>
        /// <param name="dataLocation">Location of the counter value</param>
        /// <param name="key">Key of the counter value</param>
        /// <param name="dataTraits">Any data traits for this document</param>
        /// <returns>The value of the counter</returns>
        public DataRetrievalResult<long> GetCounterValue(string dataLocation, string key, IEnumerable<IDataTrait> dataTraits)
        {
            if (!dataStore.ContainsKey(dataLocation))
            {
                dataStore[dataLocation] = new Dictionary<string, object>();
            }

            if (!dataStore[dataLocation].ContainsKey(key))
            {
                return new DataRetrievalResult<long>(default(long), false,
                    $"Document not found (data location: {dataLocation}): {key}",
                    null);
            }

            return new DataRetrievalResult<long>((long) dataStore[dataLocation][key], true, string.Empty,
                null);
        }

        /// <summary>
        /// Attempt to get a document
        /// </summary>
        /// <param name="key">Key of the document to get</param>
        /// <param name="dataLocation">Location of the document to get</param>
        /// <param name="dataTraits">Any data traits for this document</param>
        /// <returns>The result of the operation, and serialized document if retrieved successfully</returns>
        public DataRetrievalResult<string> GetDocument(string dataLocation, string key, IEnumerable<IDataTrait> dataTraits)
        {
            if (!dataStore.ContainsKey(dataLocation))
            {
                return new DataRetrievalResult<string>(string.Empty, false,
                    $"Datastore not found: {dataLocation}", null);
            }

            if (!dataStore[dataLocation].ContainsKey(key))
            {
                return new DataRetrievalResult<string>(string.Empty, false,
                    $"Document not found (data location: {dataLocation}): {key}",
                    null);
            }

            return new DataRetrievalResult<string>((string) dataStore[dataLocation][key], true, string.Empty,
                null);
        }

        /// <summary>
        /// Increments a counter
        /// </summary>
        /// <param name="dataLocation">Location of the counter to increment</param>
        /// <param name="key">Key of the counter to increment</param>
        /// <param name="dataTraits">Any data traits for this document</param>
        /// <param name="incrementValue">Value to increment by</param>
        /// <returns>Result of the operation</returns>
        public DataRetrievalResult<long> IncrementCounter(string dataLocation, string key, IEnumerable<IDataTrait> dataTraits, long incrementValue = 1)
        {
            if (!dataStore.ContainsKey(dataLocation))
            {
                dataStore[dataLocation] = new Dictionary<string, object>();
            }

            if (!dataStore[dataLocation].ContainsKey(key))
            {
                dataStore[dataLocation][key] = 0L;
            }

            dataStore[dataLocation][key] = (long) dataStore[dataLocation][key] + 1;

            return new DataRetrievalResult<long>((long) dataStore[dataLocation][key], true, string.Empty,
                null);
        }

        /// <summary>
        /// Check if the specified key exists in the datastore
        /// </summary>
        /// <param name="dataLocation">Location to search</param>
        /// <param name="key">Key to check</param>
        /// <param name="dataTraits">Any data traits for this document</param>
        /// <returns>True if the key exists</returns>
        public DataRetrievalResult<bool> KeyExists(string dataLocation, string key, IEnumerable<IDataTrait> dataTraits)
        {
            if (!dataStore.ContainsKey(dataLocation))
            {
                dataStore[dataLocation] = new Dictionary<string, object>();
            }

            var result = dataStore[dataLocation].ContainsKey(key);
            return new DataRetrievalResult<bool>(result, true, string.Empty, null);
        }

        /// <summary>
        /// Set a counter value explicitly
        /// </summary>
        /// <param name="dataLocation">Location of the counter to set</param>
        /// <param name="key">Key of the counter to set</param>
        /// <param name="value">Value of the counter</param>
        /// <param name="dataTraits">Any data traits for this document</param>
        /// <returns>Result of the set operation</returns>
        public DataResult SetCounterValue(string dataLocation, string key, long value, IEnumerable<IDataTrait> dataTraits)
        {
            if (!dataStore.ContainsKey(dataLocation))
            {
                return new DataResult(false, $"Datastore not found: {dataLocation}",
                    null);
            }

            if (!dataStore[dataLocation].ContainsKey(key))
            {
                return new DataResult(false,
                    $"Document not found (data location: {dataLocation}): {key}",
                    null);
            }

            dataStore[dataLocation][key] = value;

            return new DataResult(true, string.Empty, null);
        }

        /// <summary>
        /// Attempt to create a document, will fail if the document already exists
        /// </summary>
        /// <param name="key">Key of the document to store</param>
        /// <param name="document">Serialized document to store</param>
        /// <param name="dataLocation">Location of the document to store</param>
        /// <param name="traits">Any data traits for this document</param>
        /// <returns>The result of the operation</returns>
        public DataResult CreateDocument(string dataLocation, string key, string document, IEnumerable<IDataTrait> traits)
        {
            if (!dataStore.ContainsKey(dataLocation))
            {
                dataStore[dataLocation] = new Dictionary<string, object>();
            }

            if (dataStore[dataLocation].ContainsKey(key))
            {
                return new DataResult(false, "Document already exists", null);
            }

            dataStore[dataLocation][key] = document;

            return new DataResult(true, string.Empty, null);
        }

        /// <summary>
        /// Attempt to save a document, will overwrite if the document already exists
        /// </summary>
        /// <param name="key">Key of the document to store</param>
        /// <param name="document">Serialized document to store</param>
        /// <param name="dataLocation">Location of the document to store</param>
        /// <param name="dataTraits">Any data traits for this document</param>
        /// <returns>The result of the operation</returns>
        public DataResult SaveDocument(string dataLocation, string key, string document, IEnumerable<IDataTrait> dataTraits)
        {
            if (!dataStore.ContainsKey(dataLocation))
            {
                dataStore[dataLocation] = new Dictionary<string, object>();
            }

            dataStore[dataLocation][key] = document;

            return new DataResult(true, string.Empty, null);
        }
    }
}
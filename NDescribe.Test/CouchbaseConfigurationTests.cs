﻿using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Xml.Linq;
using FluentAssertions;
using NDescribe.Couchbase;
using NUnit.Framework;

namespace NDescribe.Test
{
    [TestFixture]
    [SuppressMessage("ReSharper", "PublicMembersMustHaveComments")]
    public class CouchbaseConfigurationTests
    {
        [Test]
        public void TestLoad()
        {
            var text =
                @"<ndescribecouchbase><clusters><cluster name=""TestCluster"" ><servers><server uri=""http://127.0.0.1:8091/pools"" /></servers></cluster></clusters><buckets><bucket bucketName=""MyBucketName"" dataLocation =""DataLocation"" cluster =""TestCluster"" password =""myPassword"" /></buckets></ndescribecouchbase>";
            var configurationProvider = new CouchbaseFileConfigurationProvider(XDocument.Parse(text));
            var bucketConfigurations = configurationProvider.BucketConfigurations;
            var bucketConfigurationsArray = bucketConfigurations as BucketConfiguration[] ?? bucketConfigurations.ToArray();
            bucketConfigurationsArray.Length.Should().Be(1, "because we configured one bucket.");
            var bucketConfiguration = bucketConfigurationsArray[0];
            bucketConfiguration.Bucket.Should().Be("MyBucketName", "because that's what we configured.");
            bucketConfiguration.Cluster.Should().Be("TestCluster", "because that's what we configured.");
            bucketConfiguration.DataLocation.Should().Be("DataLocation", "because that's what we configured.");
            bucketConfiguration.Password.Should().Be("myPassword", "because that's what we configured.");

            var clusterConfigurations = configurationProvider.ClusterConfigurations;

            var clusterConfigurationsArray = clusterConfigurations as ClusterConfiguration[] ?? clusterConfigurations.ToArray();
            clusterConfigurationsArray.Length.Should().Be(1, "because that's what we configured.");
            var clusterConfiguration = clusterConfigurationsArray[0];
            clusterConfiguration.Name.Should().Be("TestCluster", "because that's what we configured.");
            clusterConfiguration.Servers.Count().Should().Be(1, "because that's what we configured.");
            var clusterConfigurationServer = clusterConfiguration.Servers.ElementAt(0);
            clusterConfigurationServer.AbsoluteUri.Should().Be("http://127.0.0.1:8091/pools", "because that's what we configured.");
        }
    }
}
﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using NDescribe.Core.DocumentGeneration.Traits;

namespace NDescribe.Test
{
    internal interface ITestInterface
    {
        long SomeCounter { get; set; }

        string Key { get; set; }
    }

    [SuppressMessage("ReSharper", "InternalMembersMustHaveComments")]
    internal class TestDocument : ITestInterface
    {
        public string Key { get; set; }

        public long SomeCounter { get; set; }

        public long SomeCounterField = 0;

        public string IndexedField { get; set; }

        public string LookupField { get; set; }

        public bool SomeBoolean { get; set; }

        public IList<string> Values { get; set; }

        public long DoStuff()
        {
            return 1;
        }
    }

    internal class TestTrait : ITrait
    {        
    }
}
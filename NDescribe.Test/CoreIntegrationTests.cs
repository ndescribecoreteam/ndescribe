﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using FluentAssertions;
using NDescribe.Core;
using NDescribe.Core.Configuration;
using NDescribe.Core.DataBridge;
using NDescribe.Core.DocumentGeneration.Patterns.CounterPattern;
using NDescribe.Core.DocumentGeneration.Patterns.LockingPattern;
using NDescribe.Core.DocumentGeneration.Patterns.LookupPattern;
using NDescribe.Core.DocumentGeneration.Patterns.TransactionPattern;
using NDescribe.Core.DocumentGeneration.Patterns.VersionPattern;
using NDescribe.Core.DocumentGeneration.Traits;
using NDescribe.Couchbase;
using NDescribe.InMemory;
using NUnit.Framework;

namespace NDescribe.Test
{
    [TestFixture]
    [SuppressMessage("ReSharper", "PublicMembersMustHaveComments")]
    [SuppressMessage("ReSharper", "StringLiteralsWordIsNotInDictionary")]
    [SuppressMessage("ReSharper", "EventExceptionNotDocumented")]
    [SuppressMessage("ReSharper", "ExceptionNotDocumentedOptional")]
    public class CoreIntegrationTests
    {
        private static readonly object[][] ConfigurationOptions;
        private static readonly string TestingBucketName;

        static CoreIntegrationTests()
        {
            TestingBucketName = string.Empty;

            bool runCouchbaseIntegrationTests;
            bool runMemoryIntegrationTests;

            var couchbaseIntegrationTestsAppSetting = ConfigurationManager.AppSettings["RunCouchbaseIntegrationTests"];
            var couchbaseConnectionStringsAppSetting = ConfigurationManager.AppSettings["CouchbaseConnectionStrings"];
            var memoryIntegrationTestsAppSetting = ConfigurationManager.AppSettings["RunMemoryIntegrationTests"];
            var couchbaseBucketNameAppSetting = ConfigurationManager.AppSettings["BucketName"];

            TestingBucketName = string.IsNullOrEmpty(couchbaseBucketNameAppSetting)
                ? "test"
                : couchbaseBucketNameAppSetting;

            bool.TryParse(couchbaseIntegrationTestsAppSetting, out runCouchbaseIntegrationTests);
            bool.TryParse(memoryIntegrationTestsAppSetting, out runMemoryIntegrationTests);

            List<object[]> configurationOptionsList = new List<object[]>();

            if (runCouchbaseIntegrationTests && !string.IsNullOrEmpty(couchbaseConnectionStringsAppSetting) &&
                !string.IsNullOrEmpty(TestingBucketName))
            {
                string[] uris = couchbaseConnectionStringsAppSetting.Split(',');
                var clusters = new Dictionary<string, IEnumerable<Uri>>
                {
                    {TestingBucketName, uris.Select(x => new Uri(x))}
                };
                var buckets = new Dictionary<string, string>
                {
                    {TestingBucketName, TestingBucketName}
                };

                configurationOptionsList.Add(new object[]
                {
                    new Func<IDataBridgeConfigurationProvider>(
                        () => new CouchbaseDirectConfigurationProvider(clusters, buckets))
                });
                Debug.WriteLine("Enabled Couchbase integration testing.");
            }

            if (runMemoryIntegrationTests)
            {
                configurationOptionsList.Add(new object[]
                {
                    new Func<IDataBridgeConfigurationProvider>(
                        () => new MemoryStoreConfigurationProvider())
                });
            }

            ConfigurationOptions = configurationOptionsList.ToArray();
        }

        [Test, TestCaseSource(nameof(ConfigurationOptions))]
        public void RetrieveAllCounterDocumentsWhenNoneExist(
            Func<IDataBridgeConfigurationProvider> dataBridgeConfigurationProviderFactory)
        {
            var options = new ConfigurationOptions(dataBridgeConfigurationProviderFactory());

            var context = new DataContext(TestingBucketName)
                .Define<TestDocument>(x => x.Key)
                .WithCounter(x => x.SomeCounter)
                .WithVersioning()
                .Defined();

            var session = new DataSessionFactory().CreateSession(new List<DataContext> { context }, options);

            var counterDocuments = session.GetAllCounterDocuments(new TestDocument {Key = "TestKey"});
            counterDocuments.Document.Should().BeEmpty("because we didn't add anything");
        }

        [Test, TestCaseSource(nameof(ConfigurationOptions))]
        public void RetrieveAllCounterDocuments(
            Func<IDataBridgeConfigurationProvider> dataBridgeConfigurationProviderFactory)
        {
            var options = new ConfigurationOptions(dataBridgeConfigurationProviderFactory());

            var context = new DataContext(TestingBucketName)
                .Define<TestDocument>(x => x.Key)
                .WithCounter(x => x.SomeCounter)
                .WithVersioning()
                .Defined();

            var session = new DataSessionFactory().CreateSession(new List<DataContext> {context}, options);

            var document = new TestDocument {Key = "TestKey"};

            var result = session.Save(document);

            Assert.True(result.Success);

            var readResult = session.Read(document);

            Assert.True(readResult.Success);

            result = session.Save(document);

            Assert.True(result.Success);

            document.SomeCounter = 0;
            result = session.Save(document);
            Assert.True(result.Success);

            var rangeResult = session.GetAllCounterDocuments(new TestDocument {Key = "TestKey"});
            Assert.True(rangeResult.Success);
            Assert.True(rangeResult.Document.Count == 2);
            rangeResult.Document.Values.ToList().ForEach(x => Assert.True(x.Success));
        }

        [Test, TestCaseSource(nameof(ConfigurationOptions))]
        public void RetrieveAllCounterDocumentsWithMetaCounter(
    Func<IDataBridgeConfigurationProvider> dataBridgeConfigurationProviderFactory)
        {
            var options = new ConfigurationOptions(dataBridgeConfigurationProviderFactory());

            var context = new DataContext(TestingBucketName)
                .Define<TestDocument>(x => x.Key)
                .WithMetaCounter("TestCounter")
                .WithVersioning()
                .Defined();

            var session = new DataSessionFactory().CreateSession(new List<DataContext> { context }, options);

            var document = new TestDocument { Key = "TestKey" };

            var operationContext = new OperationContextBuilder().WithMetaCounter("TestCounter", 0).Build();
            var result = session.Save(document, operationContext);

            Assert.True(result.Success);

            var readResult = session.Read(document, operationContext);

            Assert.True(readResult.Success);

            result = session.Save(document, operationContext);

            Assert.True(result.Success);

            document.SomeCounter = 0;
            result = session.Save(document);
            Assert.True(result.Success);

            var rangeResult = session.GetAllCounterDocuments(new TestDocument { Key = "TestKey" });
            Assert.True(rangeResult.Success);
            Assert.True(rangeResult.Document.Count == 2);
            rangeResult.Document.Values.ToList().ForEach(x => Assert.True(x.Success));
        }

        [Test, TestCaseSource(nameof(ConfigurationOptions))]
        public void RetrieveAllCounterDocumentsFromInterface(
            Func<IDataBridgeConfigurationProvider> dataBridgeConfigurationProviderFactory)
        {
            var options = new ConfigurationOptions(dataBridgeConfigurationProviderFactory());

            var context = new DataContext(TestingBucketName)
                .Define<TestDocument>(x => x.Key)
                .WithCounter(x => x.SomeCounter)
                .WithVersioning()
                .Defined();

            var session = new DataSessionFactory().CreateSession(new List<DataContext> {context}, options);

            var document = new TestDocument {Key = "TestKey"};

            var result = session.Save(document);

            Assert.True(result.Success);

            var readResult = session.Read(document);

            Assert.True(readResult.Success);

            result = session.Save(document);

            Assert.True(result.Success);

            document.SomeCounter = 0;
            result = session.Save(document);
            Assert.True(result.Success);

            var rangeResult = session.GetAllCounterDocuments(new TestDocument {Key = "TestKey"} as ITestInterface, true);
            Assert.True(rangeResult.Success);
            Assert.True(rangeResult.Document.Count == 2);
            rangeResult.Document.Values.ToList().ForEach(x => Assert.True(x.Success));
        }

        [Test, TestCaseSource(nameof(ConfigurationOptions))]
        public void RetrieveAllCounterDocumentsFromInterfaceWithInterfaceDefinition(
            Func<IDataBridgeConfigurationProvider> dataBridgeConfigurationProviderFactory)
        {
            var options = new ConfigurationOptions(dataBridgeConfigurationProviderFactory());

            var context = new DataContext(TestingBucketName)
                .Define<ITestInterface>(x => x.Key)
                .WithCounter(x => x.SomeCounter)
                .WithVersioning()
                .Defined();

            var session = new DataSessionFactory().CreateSession(new List<DataContext> {context}, options);

            var document = (ITestInterface) new TestDocument {Key = "TestKey"};

            var result = session.Save(document);

            Assert.True(result.Success);

            var readResult = session.Read(document);

            Assert.True(readResult.Success);

            result = session.Save(document);

            Assert.True(result.Success);

            document.SomeCounter = 0;
            result = session.Save(document);
            Assert.True(result.Success);

            var rangeResult = session.GetAllCounterDocuments(new TestDocument {Key = "TestKey"} as ITestInterface, true);
            Assert.True(rangeResult.Success);
            Assert.True(rangeResult.Document.Count == 2);
            rangeResult.Document.Values.ToList().ForEach(x => Assert.True(x.Success));
        }

        [Test, TestCaseSource(nameof(ConfigurationOptions))]
        public void RetrieveAllCounterDocumentsFromInterfaceWithInterfaceDefinitionAndNoDerivationSearch(
    Func<IDataBridgeConfigurationProvider> dataBridgeConfigurationProviderFactory)
        {
            var options = new ConfigurationOptions(dataBridgeConfigurationProviderFactory());

            var context = new DataContext(TestingBucketName)
                .Define<ITestInterface>(x => x.Key)
                .WithCounter(x => x.SomeCounter)
                .WithVersioning()
                .Defined();

            var session = new DataSessionFactory().CreateSession(new List<DataContext> { context }, options);

            var document = (ITestInterface)new TestDocument { Key = "TestKey" };

            var result = session.Save(document);

            Assert.True(result.Success);

            var readResult = session.Read(document);

            Assert.True(readResult.Success);

            result = session.Save(document);

            Assert.True(result.Success);

            document.SomeCounter = 0;
            result = session.Save(document);
            Assert.True(result.Success);

            var rangeResult = session.GetAllCounterDocuments(new TestDocument { Key = "TestKey" } as ITestInterface, false);
            Assert.True(rangeResult.Success);
            Assert.True(rangeResult.Document.Count == 2);
            rangeResult.Document.Values.ToList().ForEach(x => Assert.True(x.Success));
        }


        [Test, TestCaseSource(nameof(ConfigurationOptions))]
        public void RetrieveDocumentFromLookup(
            Func<IDataBridgeConfigurationProvider> dataBridgeConfigurationProviderFactory)
        {
            var options = new ConfigurationOptions(dataBridgeConfigurationProviderFactory());

            var context = new DataContext(TestingBucketName)
                .Define<TestDocument>(x => x.Key)
                .WithLookup(x => x.LookupField, lookupValue => $"Lookup::{lookupValue}")
                .Defined();

            var session = new DataSessionFactory().CreateSession(new List<DataContext> {context}, options);

            var document = new TestDocument {Key = "TestKey", LookupField = "TestLookup"};

            var result = session.Save(document);

            Assert.True(result.Success, result.Message);

            var readResult = session.ReadFromLookup(document);

            Assert.True(readResult.Success, $"{readResult.Message} - {readResult.Exception}");

            var deleteDocument = session.Delete(document);
            deleteDocument.Success.Should().BeTrue(deleteDocument.Message);

            var readResult2 = session.ReadFromLookup(document);
            readResult2.Success.Should().BeFalse(readResult2.Message);
        }

        [Test, TestCaseSource(nameof(ConfigurationOptions))]
        public void TestLocking(
            Func<IDataBridgeConfigurationProvider> dataBridgeConfigurationProviderFactory)
        {
            var options = new ConfigurationOptions(dataBridgeConfigurationProviderFactory());

            var context = new DataContext(TestingBucketName)
                .Define<TestDocument>(x => x.Key)
                .WithLocking()
                .Defined();

            var session = new DataSessionFactory().CreateSession(new List<DataContext> {context}, options);

            var document = new TestDocument {Key = "TestKey", LookupField = "TestLookup"};

            var result = session.Save(document);
            result.Success.Should().BeTrue(result.Message);

            var lockResult = session.AcquireLock(document);
            lockResult.Success.Should().BeTrue(lockResult.Message);

            var secondStoreResult = session.Save(document);
            secondStoreResult.Success.Should().BeFalse(secondStoreResult.Message);

            var unlockResult = session.ReleaseLock(document);
            unlockResult.Success.Should().BeTrue(unlockResult.Message);

            var thirdStoreResult = session.Save(document);
            thirdStoreResult.Success.Should().BeTrue(thirdStoreResult.Message);
        }

        [Test, TestCaseSource(nameof(ConfigurationOptions))]
        public void TestStoreWithoutLocking(
            Func<IDataBridgeConfigurationProvider> dataBridgeConfigurationProviderFactory)
        {
            var options = new ConfigurationOptions(dataBridgeConfigurationProviderFactory());

            var context = new DataContext(TestingBucketName)
                .Define<TestDocument>(x => x.Key)
                .WithLocking()
                .Defined();

            var session = new DataSessionFactory().CreateSession(new List<DataContext> {context}, options);

            var document = new TestDocument {Key = "TestKey", LookupField = "TestLookup"};

            var lockResult = session.AcquireLock(document);
            lockResult.Success.Should().BeTrue(lockResult.Message);

            var storeResult = session.SaveWithoutLock(document);
            storeResult.Success.Should().BeTrue(storeResult.Message);

            var unlockResult = session.ReleaseLock(document);
            unlockResult.Success.Should().BeTrue(unlockResult.Message);
        }

        [Test, TestCaseSource(nameof(ConfigurationOptions))]
        public void RetrieveAllCounterDocumentsUsingField(
            Func<IDataBridgeConfigurationProvider> dataBridgeConfigurationProviderFactory)
        {
            var options = new ConfigurationOptions(dataBridgeConfigurationProviderFactory());

            var context = new DataContext(TestingBucketName)
                .Define<TestDocument>(x => x.Key)
                .WithCounter(x => x.SomeCounterField)
                .WithVersioning()
                .Defined();

            var session = new DataSessionFactory().CreateSession(new List<DataContext> {context}, options);

            var document = new TestDocument {Key = "TestKey"};

            var result = session.Save(document);

            Assert.True(result.Success);

            var readResult = session.Read(document);

            Assert.True(readResult.Success);

            result = session.Save(document);

            Assert.True(result.Success);

            var rangeResult = session.GetAllCounterDocuments(new TestDocument {Key = "TestKey"});
            Assert.True(rangeResult.Success);
        }

        [Test, TestCaseSource(nameof(ConfigurationOptions))]
        public void TestInvalidUsageForCounter(
            Func<IDataBridgeConfigurationProvider> dataBridgeConfigurationProviderFactory)
        {
            new DataContext(TestingBucketName)
                .Define<TestDocument>(x => x.Key)
                .Invoking(builder => builder.WithCounter(x => x.DoStuff()))
                .ShouldThrow<ArgumentException>("because we specified a method instead of a value");
        }

        [Test, TestCaseSource(nameof(ConfigurationOptions))]
        public void TestTraits(
            Func<IDataBridgeConfigurationProvider> dataBridgeConfigurationProviderFactory)
        {
            new DataContext(TestingBucketName)
                .Define<TestDocument>(x => x.Key)
                .WithTrait<TestDocument, TestTrait>().Defined();
        }


        [Test, TestCaseSource(nameof(ConfigurationOptions))]
        public void RetrieveSpecificVersion(
            Func<IDataBridgeConfigurationProvider> dataBridgeConfigurationProviderFactory)
        {
            var options = new ConfigurationOptions(dataBridgeConfigurationProviderFactory());

            var context = new DataContext(TestingBucketName)
                .Define<TestDocument>(x => x.Key)
                .WithCounter(x => x.SomeCounter)
                .WithVersioning()
                .Defined();

            var session = new DataSessionFactory().CreateSession(new List<DataContext> {context}, options);

            var document = new TestDocument {Key = "TestKey", SomeBoolean = true};

            var result = session.Save(document);

            Assert.True(result.Success);

            document.SomeBoolean = false;

            result = session.Save(document);

            Assert.True(result.Success);

            var readResult = session.Read(document);

            Assert.True(readResult.Success, readResult.Message);
            Assert.False(readResult.Document.SomeBoolean);

            readResult = session.ReadWithVersion(document, 1);

            Assert.True(readResult.Success);
            Assert.True(readResult.Document.SomeBoolean);

            readResult = session.Read(document);

            Assert.True(readResult.Success);
            Assert.False(readResult.Document.SomeBoolean);

            var latestVersion = session.ReadLatestVersion(document);
            Assert.True(latestVersion.Success);
            Assert.AreEqual(latestVersion.Document, 2);
        }

        [Test, TestCaseSource(nameof(ConfigurationOptions))]
        public void TestLookup(Func<IDataBridgeConfigurationProvider> dataBridgeConfigurationProviderFactory)
        {
            var options = new ConfigurationOptions(dataBridgeConfigurationProviderFactory());

            var context = new DataContext(TestingBucketName)
                .Define<TestDocument>(x => x.Key)
                .WithLookup(x => x.LookupField, x => $"TestLookup::{x}")
                .Defined();

            var session = new DataSessionFactory().CreateSession(new List<DataContext> {context}, options);

            var document = new TestDocument {Key = "TestKey", LookupField = "lookup1"};

            var result = session.Save(document);
            Assert.True(result.Success);

            var deleteResult = session.Delete(document);
            deleteResult.Success.Should().BeTrue();
        }

        [Test, TestCaseSource(nameof(ConfigurationOptions))]
        public void TestTransaction(Func<IDataBridgeConfigurationProvider> dataBridgeConfigurationProviderFactory)
        {
            var options = new ConfigurationOptions(dataBridgeConfigurationProviderFactory());

            var context = new DataContext(TestingBucketName)
                .Define<TestDocument>(x => $"Document1:{x.Key}")
                .ForTransactions()                
                .Defined()
                .Define<SecondTestDocument>(x => $"Document2:{x.Key}")
                .ForTransactions()
                .Defined();

            var session = new DataSessionFactory().CreateSession(new List<DataContext> {context}, options);

            var document = new TestDocument {Key = "TestKey", LookupField = "testLookup1"};
            var document2 = new SecondTestDocument { Key = "TestKey", LookupField = "testLookup2" };

            DataResult result;

            var transactionConfiguration = new TransactionConfiguration("TestKey::TransactionCounter",
                x => $"TestKey::Transaction::{x}");

            using (transactionConfiguration.StartTransaction())
            {
                result = session.Save(document);
                result.Success.Should().BeTrue();

                result = session.Save(document2);
                result.Success.Should().BeTrue();

                var readResult1 = session.Read(document);
                readResult1.Success.Should().BeFalse();
                readResult1.Message.Should().Contain("any version");

                var readResultDocument2 = session.Read(document2);
                readResultDocument2.Success.Should().BeFalse();
                readResultDocument2.Message.Should().Contain("any version");
            }

            document.LookupField = null;
            document2.LookupField = null;

            var readResult2 = session.Read(document);
            readResult2.Success.Should().BeTrue(readResult2.Message);

            readResult2.Document.LookupField.Should().Be("testLookup1");

            var readResult3 = session.Read(document2);
            readResult3.Success.Should().BeTrue();

            readResult3.Document.LookupField.Should().Be("testLookup2");

            document.LookupField = "testLookup3";
            document2.LookupField = "testLookup4";

            using (transactionConfiguration.StartTransaction())
            {
                result = session.Save(document);
                result.Success.Should().BeTrue();

                result = session.Save(document2);
                result.Success.Should().BeTrue();

                readResult2 = session.Read(document);
                readResult2.Success.Should().BeTrue(readResult2.Message);

                readResult2.Document.LookupField.Should().Be("testLookup1");

                readResult3 = session.Read(document2);
                readResult3.Success.Should().BeTrue(readResult2.Message);

                readResult3.Document.LookupField.Should().Be("testLookup2");
            }

            document.LookupField = null;
            document2.LookupField = null;

            readResult2 = session.Read(document);
            readResult2.Success.Should().BeTrue();

            readResult2.Document.LookupField.Should().Be("testLookup3");

            readResult3 = session.Read(document2);
            readResult3.Success.Should().BeTrue();

            readResult3.Document.LookupField.Should().Be("testLookup4");

            result = session.Save(document);
            Assert.True(result.Success);
        }

        [Test, TestCaseSource(nameof(ConfigurationOptions))]
        public void TestTransactionCancellation(Func<IDataBridgeConfigurationProvider> dataBridgeConfigurationProviderFactory)
        {
            var options = new ConfigurationOptions(dataBridgeConfigurationProviderFactory());

            var context = new DataContext(TestingBucketName)
                .Define<TestDocument>(x => x.Key)
                .ForTransactions()
                .Defined();

            var session = new DataSessionFactory().CreateSession(new List<DataContext> { context }, options);

            var document = new TestDocument { Key = "TestKey", LookupField = "testLookup1" };
            var document2 = new TestDocument { Key = "TestKey2", LookupField = "testLookup2" };

            DataResult result;

            var transactionConfiguration = new TransactionConfiguration("TestKey::TransactionCounter",
                x => $"TestKey::Transaction::{x}");

            using (var scope = transactionConfiguration.StartTransaction())
            {
                scope.CancelTransaction();

                result = session.Save(document);
                result.Success.Should().BeTrue();

                result = session.Save(document2);
                result.Success.Should().BeTrue();

                var readResult1 = session.Read(document);
                readResult1.Success.Should().BeFalse();
                readResult1.Message.Should().Contain("any version");

                var readResultDocument2 = session.Read(document2);
                readResultDocument2.Success.Should().BeFalse();
                readResultDocument2.Message.Should().Contain("any version");
            }

            var readResult = session.Read(document);
            readResult.Success.Should().BeFalse("because the transaction was cancelled");
            readResult = session.Read(document2);
            readResult.Success.Should().BeFalse("because the transaction was cancelled");
        }

        [Test, TestCaseSource(nameof(ConfigurationOptions))]
        public void TestTransactionFailure(Func<IDataBridgeConfigurationProvider> dataBridgeConfigurationProviderFactory)
        {
            var options = new ConfigurationOptions(dataBridgeConfigurationProviderFactory());

            var context = new DataContext(TestingBucketName)
                .Define<TestDocument>(x => x.Key)
                .ForTransactions()
                .Defined();

            var session = new DataSessionFactory().CreateSession(new List<DataContext> { context }, options);

            var document = new TestDocument { Key = "TestKey", LookupField = "testLookup1" };
            var document2 = new TestDocument { Key = "TestKey2", LookupField = "testLookup2" };

            DataResult result;

            var transactionConfiguration = new TransactionConfiguration("TestKey::TransactionCounter",
                x => $"TestKey::Transaction::{x}");

            using (transactionConfiguration.StartTransaction())
            {
                result = session.Save(document);
                result.Success.Should().BeTrue();

                result = session.Save(document);
                result.Success.Should().BeFalse();

                result = session.Save(document2);
                result.Success.Should().BeTrue();

                var readResult1 = session.Read(document);
                readResult1.Success.Should().BeFalse();
                readResult1.Message.Should().Contain("any version");

                var readResultDocument2 = session.Read(document2);
                readResultDocument2.Success.Should().BeFalse();
                readResultDocument2.Message.Should().Contain("any version");
            }

            var readResult = session.Read(document);
            readResult.Success.Should().BeFalse("because the transaction was cancelled");
            readResult = session.Read(document2);
            readResult.Success.Should().BeFalse("because the transaction was cancelled");
        }

        [Test, TestCaseSource(nameof(ConfigurationOptions))]
        public void TestListTypes(Func<IDataBridgeConfigurationProvider> dataBridgeConfigurationProviderFactory)
        {
            var options = new ConfigurationOptions(dataBridgeConfigurationProviderFactory());

            var context = new DataContext(TestingBucketName)
                .Define<TestDocument>(x => x.Key)
                .ForTransactions()
                .Defined();

            var session = new DataSessionFactory().CreateSession(new List<DataContext> {context}, options);

            var document = new TestDocument
            {
                Key = "TestKey",
                LookupField = "testLookup1",
                Values = new List<string> {"a", "b", "c"}
            };

            var storeDocument = session.Save(document);
            storeDocument.Success.Should().BeTrue(storeDocument.Message);

            var retrieveDocument = session.Read(new TestDocument {Key = "TestKey"});
            retrieveDocument.Success.Should().BeTrue(retrieveDocument.Message);

            retrieveDocument.Document.Values.Should().ContainInOrder("a", "b", "c");
        }

        [Test, TestCaseSource(nameof(ConfigurationOptions))]
        public void TestInsertFunctionality(
            Func<IDataBridgeConfigurationProvider> dataBridgeConfigurationProviderFactory)
        {
            var options = new ConfigurationOptions(dataBridgeConfigurationProviderFactory());

            var context = new DataContext(TestingBucketName)
                .Define<TestDocument>(x => x.Key)
                .Defined();

            var session = new DataSessionFactory().CreateSession(new List<DataContext> {context}, options);

            var document = new TestDocument
            {
                Key = "TestKey",
                LookupField = "testLookup1",
                Values = new List<string> {"a", "b", "c"}
            };

            var insertDocument = session.Create(document);
            insertDocument.Success.Should().BeTrue(insertDocument.Message);

            var insertDocumentAgain = session.Create(document);
            insertDocumentAgain.Success.Should().BeFalse(insertDocumentAgain.Message);

            var upsertDocument = session.Save(document);
            upsertDocument.Success.Should().BeTrue(upsertDocument.Message);
        }

        [Test, TestCaseSource(nameof(ConfigurationOptions))]
        public void TestInvalidDocumentVersion(
        Func<IDataBridgeConfigurationProvider> dataBridgeConfigurationProviderFactory)
        {
            var options = new ConfigurationOptions(dataBridgeConfigurationProviderFactory());

            var context = new DataContext(TestingBucketName)
                .Define<TestDocument>(x => x.Key)
                .Defined();

            var sessionV1 = new DataSessionFactory().CreateSession(new List<DataContext> { context }, options);
            var sessionV2 = new DataSessionFactory().CreateSession(new List<DataContext> { context }, options, "2.0");

            var document = new TestDocument
            {
                Key = "TestKey",
                LookupField = "testLookup1",
                Values = new List<string> { "a", "b", "c" }
            };

            var insertDocument = sessionV1.Create(document);
            insertDocument.Success.Should().BeTrue(insertDocument.Message);

            var readResult = sessionV2.Read(document);
            readResult.Success.Should().BeFalse("because we are reading the wrong document version");

            readResult = sessionV1.Read(document);
            readResult.Success.Should().BeTrue("because this is the right document version");
        }

        [Test, TestCaseSource(nameof(ConfigurationOptions))]
        public void TestInsertFunctionalityOnInterface(
            Func<IDataBridgeConfigurationProvider> dataBridgeConfigurationProviderFactory)
        {
            var options = new ConfigurationOptions(dataBridgeConfigurationProviderFactory());

            var context = new DataContext(TestingBucketName)
                .Define<ITestInterface>(x => x.Key)
                .Defined();

            var session = new DataSessionFactory().CreateSession(new List<DataContext> {context}, options);

            var document = new TestDocument
            {
                Key = "TestKey",
                LookupField = "testLookup1",
                Values = new List<string> {"a", "b", "c"}
            } as ITestInterface;

            var insertDocument = session.Create(document);
            insertDocument.Success.Should().BeTrue(insertDocument.Message);

            var insertDocumentAgain = session.Create(document);
            insertDocumentAgain.Success.Should().BeFalse(insertDocumentAgain.Message);

            var upsertDocument = session.Save(document);
            upsertDocument.Success.Should().BeTrue(upsertDocument.Message);
        }

        [Test, TestCaseSource(nameof(ConfigurationOptions))]
        public void TestDelete(
            Func<IDataBridgeConfigurationProvider> dataBridgeConfigurationProviderFactory)
        {
            var options = new ConfigurationOptions(dataBridgeConfigurationProviderFactory());

            var context = new DataContext(TestingBucketName)
                .Define<TestDocument>(x => x.Key)
                .Defined();

            var session = new DataSessionFactory().CreateSession(new List<DataContext> {context}, options);

            var document = new TestDocument
            {
                Key = "TestKey",
                LookupField = "testLookup1",
                Values = new List<string> {"a", "b", "c"}
            };

            var insertDocument = session.Create(document);
            insertDocument.Success.Should().BeTrue(insertDocument.Message);

            var deleteDocument = session.Delete(document);
            deleteDocument.Success.Should().BeTrue(deleteDocument.Message);

            var insertDocument2 = session.Create(document);
            insertDocument2.Success.Should().BeTrue(insertDocument2.Message);

            var deleteDocument2 = session.Delete(document);
            deleteDocument2.Success.Should().BeTrue(deleteDocument2.Message, new Dictionary<string, object>());

            var deleteDocument3 = session.Delete(document);
            deleteDocument3.Success.Should().BeFalse(deleteDocument3.Message);
        }
    }
}
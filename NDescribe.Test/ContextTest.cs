﻿using System.Diagnostics.CodeAnalysis;
using FluentAssertions;
using NDescribe.Core;
using NUnit.Framework;

namespace NDescribe.Test
{
    [TestFixture]
    [SuppressMessage("ReSharper", "PublicMembersMustHaveComments")]
    public class ContextTest
    {
        [Test]
        public void WhenDefineCalled_ConfigurationBuilderReturned()
        {
            var configurationBuilder = new DataContext("").Define<TestDocument>(x => "Key");
            configurationBuilder.Should().NotBeNull("because the builder should generate one by default.");
        }
    }
}
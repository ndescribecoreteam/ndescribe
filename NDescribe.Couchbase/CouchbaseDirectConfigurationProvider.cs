﻿using System;
using System.Collections.Generic;
using System.Linq;
using Couchbase;
using Couchbase.Configuration.Client;
using Couchbase.Core;
using NDescribe.Core.DataBridge;

namespace NDescribe.Couchbase
{
    /// <summary>
    /// Configuration provider for a Couchbase Data Bridge from hard-coded configuration
    /// </summary>
    public class CouchbaseDirectConfigurationProvider : IDataBridgeConfigurationProvider
    {
        private readonly IDictionary<string, IEnumerable<Uri>> clusters;

        private readonly IDictionary<string, string> dataLocationClusterLookup;

        /// <summary>
        /// Create a new instance of the in-memory couchbase IDataBridge provider
        /// </summary>
        /// <param name="clusters">Cluster configuration by cluster-name and server addresses</param>
        /// <param name="bucketDefinition">Bucket definition by name and name of cluster (in this instance datalocation will be equivalent to name)</param>
        public CouchbaseDirectConfigurationProvider(IDictionary<string, IEnumerable<Uri>> clusters,
            IDictionary<string, string> bucketDefinition)
        {
            this.clusters = clusters;
            dataLocationClusterLookup = bucketDefinition;
        }

        /// <summary>
        /// Create the Couchbase IDataBridge based on configuration
        /// </summary>
        /// <returns>The created IDataBridge</returns>
        public IDataBridge Create()
        {
            Dictionary<string, ICluster> couchbaseClusters = clusters.AsParallel()
                .ToDictionary(x => x.Key,
                    x => (ICluster) new Cluster(new ClientConfiguration {Servers = x.Value.ToList()}));
            Dictionary<string, IBucket> bucketLookup = dataLocationClusterLookup.AsParallel()
                .ToDictionary(x => x.Key, x => couchbaseClusters[x.Value].OpenBucket(x.Key));
            return new CouchbaseDataBridge(bucketLookup);
        }
    }
}
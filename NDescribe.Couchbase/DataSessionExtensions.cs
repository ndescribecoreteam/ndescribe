﻿using System;
using System.Collections.Generic;
using Couchbase.Core;
using NDescribe.Core;
using NDescribe.Core.Session;

namespace NDescribe.Couchbase
{
    /// <summary>
    /// Extensions for IDataSession
    /// </summary>
    public static class DataSessionExtensions
    {
        /// <summary>
        /// Retrieve all buckets for a given IDataSession
        /// </summary>
        /// <param name="session">Session to retrieve buckets from</param>
        /// <returns>A dictionary mapping names of buckets to buckets</returns>
        /// <exception cref="ArgumentException">Thrown when the session is not queryable</exception>
        public static IDictionary<string, IBucket> GetBuckets(this IDataSession session)
        {
            var queryableSession = session as IQueryableSession;

            if (queryableSession == null)
            {
                throw new ArgumentException(
                    $"{nameof(IDataSession)} does not implement {nameof(IQueryableSession)} (this is unexpected).",
                    nameof(session));
            }

            return queryableSession.DataBridge.GetBuckets();
        }
    }
}
namespace NDescribe.Couchbase
{
    /// <summary>
    /// Configuration for a single Couchbase bucket
    /// </summary>
    public class BucketConfiguration
    {
        /// <summary>
        /// Data location name for this bucket
        /// </summary>
        public string DataLocation { get; }

        /// <summary>
        /// Name of the bucket
        /// </summary>
        public string Bucket { get; }

        /// <summary>
        /// Password (if set) for the bucket
        /// </summary>
        public string Password { get; }

        /// <summary>
        /// Cluster that this bucket belongs to
        /// </summary>
        public string Cluster { get; }

        /// <summary>
        /// Create a new bucket configuration
        /// </summary>
        /// <param name="dataLocation">Data location name for this bucket</param>
        /// <param name="bucket">Name of the bucket</param>
        /// <param name="password">Password (if set) for the bucket</param>
        /// <param name="cluster">Cluster that this bucket belongs to</param>
        public BucketConfiguration(string dataLocation, string bucket, string password, string cluster)
        {
            DataLocation = dataLocation;
            Bucket = bucket;
            Password = password;
            Cluster = cluster;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Configuration;
using NDescribe.Core;
using NDescribe.Core.Configuration;

namespace NDescribe.Couchbase
{
    /// <summary>
    /// Extension methods for creating a data session with default options
    /// </summary>
    public static class DataSessionFactoryExtensions
    {
        /// <summary>
        /// Create a session with the default options
        /// </summary>
        /// <param name="dataSessionFactory">Factory to use for creating a session</param>
        /// <param name="dataContexts">Data contexts to associate with this session</param>
        /// <param name="configurationFileLocation">Location of the file to use for configuring the Couchbase session</param>
        /// <returns>A data session created with the default options</returns>
        public static IDataSession CreateSession(this DataSessionFactory dataSessionFactory,
            IEnumerable<DataContext> dataContexts, string configurationFileLocation)
        {
            return dataSessionFactory.CreateSession(dataContexts,
                new ConfigurationOptions(new CouchbaseFileConfigurationProvider(configurationFileLocation)));
        }

        /// <summary>
        /// Create a session with the default options
        /// </summary>
        /// <param name="dataSessionFactory">Factory to use for creating a session</param>
        /// <param name="dataContexts">Data contexts to associate with this session</param>
        /// <param name="clusters">Clusters to configure</param>
        /// <param name="bucketDefinition">Bucket definitions linked to clusters</param>
        /// <returns>A data session created with the default options</returns>
        public static IDataSession CreateSession(this DataSessionFactory dataSessionFactory,
            IEnumerable<DataContext> dataContexts, IDictionary<string, IEnumerable<Uri>> clusters,
            IDictionary<string, string> bucketDefinition)
        {
            return dataSessionFactory.CreateSession(dataContexts,
                new ConfigurationOptions(new CouchbaseDirectConfigurationProvider(clusters, bucketDefinition)));
        }

        /// <summary>
        /// Create a session with the default options
        /// </summary>
        /// <param name="dataSessionFactory">Factory to use for creating a session</param>
        /// <param name="dataContexts">Data contexts to associate with this session</param>
        /// <returns>A data session created with the default options</returns>
        public static IDataSession CreateSession(this DataSessionFactory dataSessionFactory,
            IEnumerable<DataContext> dataContexts)
        {
            return dataSessionFactory.CreateSession(dataContexts,
                new ConfigurationOptions(new CouchbaseFileConfigurationProvider(ConfigurationManager.AppSettings["CouchbaseConfigurationFile"])));
        }
    }
}

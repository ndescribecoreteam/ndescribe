﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Couchbase;
using Couchbase.Configuration.Client;
using Couchbase.Core;
using NDescribe.Core.DataBridge;

namespace NDescribe.Couchbase
{
    /// <summary>
    /// Load configuration for a Couchbase provider from file
    /// </summary>
    public class CouchbaseFileConfigurationProvider : IDataBridgeConfigurationProvider
    {
        /// <summary>
        /// Cluster configurations retrieved from configuration file
        /// </summary>
        public IEnumerable<ClusterConfiguration> ClusterConfigurations { get; }

        /// <summary>
        /// Bucket configurations loaded from file
        /// </summary>
        public IEnumerable<BucketConfiguration> BucketConfigurations { get; }

        /// <summary>
        /// Create a file configuration provider specifying the file location
        /// </summary>
        /// <param name="fileLocation">Location of the file to load</param>
        public CouchbaseFileConfigurationProvider(string fileLocation)
            : this(XDocument.Load(fileLocation))
        {
            
        }

        /// <summary>
        /// Create a file configuration provider specifying a document
        /// </summary>
        /// <param name="document">The document to load</param>
        public CouchbaseFileConfigurationProvider(XDocument document)
        {
            var configurationNode = document.Descendants("ndescribecouchbase");
            var configurationNodeEnumeration = configurationNode as IList<XElement> ?? configurationNode.ToList();

            ClusterConfigurations = from x in configurationNodeEnumeration.Descendants("clusters").Descendants("cluster")
                select
                    new ClusterConfiguration(x.Attribute("name").Value, from server in x.Descendants("servers").Descendants("server")
                        select new Uri(server.Attribute("uri").Value));

            BucketConfigurations = from x in configurationNodeEnumeration.Descendants("buckets").Descendants("bucket")
                select new BucketConfiguration(x.Attribute("dataLocation").Value, x.Attribute("bucketName").Value, x.Attribute("password")?.Value, x.Attribute("cluster").Value);

        }

        /// <summary>
        /// Create a Couchbase data bridge
        /// </summary>
        /// <returns>The created data bridge</returns>
        public IDataBridge Create()
        {
            var couchbaseClusters = ClusterConfigurations.AsParallel()
                .ToDictionary(x => x.Name,
                    x => (ICluster)new Cluster(new ClientConfiguration { Servers = x.Servers.ToList() }));
            var bucketLookup = BucketConfigurations.AsParallel()
                .ToDictionary(x => x.DataLocation, x => couchbaseClusters[x.Cluster].OpenBucket(x.Bucket, x.Password));
            return new CouchbaseDataBridge(bucketLookup);
        }
    }
}
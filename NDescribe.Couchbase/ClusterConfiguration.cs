using System;
using System.Collections.Generic;

namespace NDescribe.Couchbase
{
    /// <summary>
    /// Represents the cluster configuration for a Couchbase instance
    /// </summary>
    public class ClusterConfiguration
    {
        /// <summary>
        /// Name of the cluster
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Servers contained in this cluster
        /// </summary>
        public IEnumerable<Uri> Servers { get; }

        /// <summary>
        /// Create a new cluster configuration
        /// </summary>
        /// <param name="name">Name of the cluster</param>
        /// <param name="servers">List of server URIs for this cluster</param>
        public ClusterConfiguration(string name, IEnumerable<Uri> servers)
        {
            Name = name;
            Servers = servers;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using Couchbase.Core;
using NDescribe.Core.DataBridge;

namespace NDescribe.Couchbase
{
    /// <summary>
    /// Extensions for IDataBridge
    /// </summary>
    public static class DataBridgeExtensions
    {
        /// <summary>
        /// Get all buckets for a given IDataBridge
        /// </summary>
        /// <param name="dataBridge">IDataBridge to retrieve buckets for, this must be a CouchbaseDataBridge</param>
        /// <returns>A dictionary containing names of buckets mapped to the buckets</returns>
        /// <exception cref="ArgumentException">Thrown when the IDataBridge is not a CouchbaseDataBridge</exception>
        public static IDictionary<string, IBucket> GetBuckets(this IDataBridge dataBridge)
        {
            var couchbaseDataBridge = dataBridge as CouchbaseDataBridge;

            if (couchbaseDataBridge == null)
            {
                throw new ArgumentException($"{nameof(GetBuckets)} called with a type that is not a {nameof(CouchbaseDataBridge)}", nameof(dataBridge));
            }

            return couchbaseDataBridge.BucketLookup;
        }
    }
}
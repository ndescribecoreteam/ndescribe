﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Couchbase;
using Couchbase.Core;
using Couchbase.IO;
using NDescribe.Core.DataBridge;
using NDescribe.Core.DocumentGeneration.Traits;
using NDescribe.Couchbase.Traits;

namespace NDescribe.Couchbase
{
    internal class CouchbaseDataBridge : IDataBridge
    {
        internal IDictionary<string, IBucket> BucketLookup { get; }

        public CouchbaseDataBridge(IDictionary<string, IBucket> bucketLookup)
        {
            this.BucketLookup = bucketLookup;
        }

        public DataRetrievalResult<bool> CreateLock(string dataLocation, string key, long expiry,
            IEnumerable<IDataTrait> dataTraits)
        {
            if (!BucketLookup.ContainsKey(dataLocation))
            {
                return new DataRetrievalResult<bool>(default(bool), false,
                    $"Could not find data location: {dataLocation}", null);
            }

            var dataTraitsList = dataTraits as IList<IDataTrait> ?? dataTraits.ToList();
            var options = GetOptions(dataTraitsList);

            var keyResult = KeyExists(dataLocation, key, dataTraitsList);

            if (!keyResult.Success)
            {
                return new DataRetrievalResult<bool>(default(bool), false, keyResult.Message, keyResult.Exception);
            }

            if (keyResult.Document)
            {
                return new DataRetrievalResult<bool>(false, true, string.Empty, null);
            }

            var insertResult = BucketLookup[dataLocation].Insert(key, string.Empty, TimeSpan.FromSeconds(expiry), options.ReplicateTo, options.PersistTo);

            if (!insertResult.Success)
            {
                return new DataRetrievalResult<bool>(default(bool), false,
                    $"Error inserting lock document: {insertResult.Message}", insertResult.Exception);
            }

            return new DataRetrievalResult<bool>(true, true, string.Empty, null);
        }

        public DataResult DeleteDocument(string dataLocation, string documentKey, IEnumerable<IDataTrait> dataTraits)
        {
            if (!BucketLookup.ContainsKey(dataLocation))
            {
                return new DataResult(false, $"Could not find data location: {dataLocation}",
                    null);
            }

            var options = GetOptions(dataTraits);

            var result = BucketLookup[dataLocation].Remove(documentKey, options.ReplicateTo, options.PersistTo);
            return new DataResult(result.Success, string.Empty, null);
        }

        public DataRetrievalResult<IDictionary<string, DataRetrievalResult<string>>> GetDocuments(string dataLocation, IEnumerable<string> keys, IEnumerable<IDataTrait> dataTraits)
        {
            if (!BucketLookup.ContainsKey(dataLocation))
            {
                return
                    new DataRetrievalResult<IDictionary<string, DataRetrievalResult<string>>>(
                        new Dictionary<string, DataRetrievalResult<string>>(), false, $"Could not find data location: {dataLocation}",
                        null);
            }

            var options = GetOptions(dataTraits);
            var result = BucketLookup[dataLocation].Get<string>(keys.ToList(),
                new ParallelOptions {MaxDegreeOfParallelism = options.MaxDegreesOfParallelism});

            var returnData = from keyValuePair in result.AsParallel()
                let operationResult = keyValuePair.Value
                select new
                {
                    keyValuePair.Key,
                    Value =
                        new DataRetrievalResult<string>(operationResult.Value, operationResult.Success,
                            operationResult.Message, operationResult.Exception)
                };

            var returnDictionary = returnData.ToDictionary(x => x.Key, x => x.Value);

            bool success = returnDictionary.Any(x => !x.Value.Success);
            string message = string.Empty;

            if (!success)
            {
                message = "Error in retrieving multiple documents, see internal error messages for more details";
            }

            return new DataRetrievalResult<IDictionary<string, DataRetrievalResult<string>>>(returnDictionary, success,
                message, null);
        }

        public DataRetrievalResult<long> GetCounterValue(string dataLocation, string key,
            IEnumerable<IDataTrait> dataTraits)
        {
            if (!BucketLookup.ContainsKey(dataLocation))
            {
                return new DataRetrievalResult<long>(default(long), false,
                    $"Could not find data location: {dataLocation}", null);
            }

            var result = BucketLookup[dataLocation].Increment(key, 0);

            return new DataRetrievalResult<long>(result.Success ? (long) result.Value : default(long),
                result.Success, string.IsNullOrEmpty(result.Message) ? string.Empty : result.Message,
                result.Exception);
        }

        public DataRetrievalResult<string> GetDocument(string dataLocation, string key,
            IEnumerable<IDataTrait> dataTraits)
        {
            if (!BucketLookup.ContainsKey(dataLocation))
            {
                return new DataRetrievalResult<string>(string.Empty, false,
                    $"Could not find data location: {dataLocation}", null);
            }

            var result = BucketLookup[dataLocation].Get<string>(key);

            if (result.Status == ResponseStatus.OperationTimeout)
            {
                result = BucketLookup[dataLocation].GetFromReplica<string>(key);
            }

            return new DataRetrievalResult<string>(result.Value ?? string.Empty, result.Success,
                string.IsNullOrEmpty(result.Message) ? string.Empty : result.Message,
                result.Exception);
        }

        public DataRetrievalResult<long> IncrementCounter(string dataLocation, string key,
            IEnumerable<IDataTrait> dataTraits, long incrementValue = 1)
        {
            if (!BucketLookup.ContainsKey(dataLocation))
            {
                return new DataRetrievalResult<long>(default(long), false,
                    $"Could not find data location: {dataLocation}", null);
            }

            var result = BucketLookup[dataLocation].Increment(key, (ulong) incrementValue);
            return new DataRetrievalResult<long>(result.Success ? (long) result.Value : default(long),
                result.Success, string.IsNullOrEmpty(result.Message) ? string.Empty : result.Message,
                result.Exception);
        }

        public DataRetrievalResult<bool> KeyExists(string dataLocation, string key, IEnumerable<IDataTrait> dataTraits)
        {
            if (!BucketLookup.ContainsKey(dataLocation))
            {
                return new DataRetrievalResult<bool>(default(bool), false,
                    $"Could not find data location: {dataLocation}", null);
            }

            var result = BucketLookup[dataLocation].Exists(key);
            return new DataRetrievalResult<bool>(result, true, string.Empty, null);
        }

        public DataResult SetCounterValue(string dataLocation, string key, long value,
            IEnumerable<IDataTrait> dataTraits)
        {
            if (!BucketLookup.ContainsKey(dataLocation))
            {
                return new DataResult(false, $"Could not find data location: {dataLocation}",
                    null);
            }

            var options = GetOptions(dataTraits);
            var result = BucketLookup[dataLocation].Upsert(key, (ulong) value, options.ReplicateTo, options.PersistTo);
            return new DataResult(result.Success,
                string.IsNullOrEmpty(result.Message) ? string.Empty : result.Message,
                result.Exception);
        }

        public DataResult CreateDocument(string dataLocation, string key, string document, IEnumerable<IDataTrait> traits)
        {
            if (!BucketLookup.ContainsKey(dataLocation))
            {
                return new DataResult(false, $"Could not find data location: {dataLocation}",
                    null);
            }

            var options = GetOptions(traits);
            var result = BucketLookup[dataLocation].Insert(key, document, new TimeSpan(0, 0, 0, options.Expiry), options.ReplicateTo, options.PersistTo);
            return new DataResult(result.Success,
                string.IsNullOrEmpty(result.Message) ? string.Empty : result.Message,
                result.Exception);
        }


        public DataResult SaveDocument(string dataLocation, string key, string document,
            IEnumerable<IDataTrait> dataTraits)
        {
            if (!BucketLookup.ContainsKey(dataLocation))
            {
                return new DataResult(false, $"Could not find data location: {dataLocation}",
                    null);
            }

            var options = GetOptions(dataTraits);
            var result = BucketLookup[dataLocation].Upsert(key, document, new TimeSpan(0, 0, 0, options.Expiry), options.ReplicateTo, options.PersistTo);
            return new DataResult(result.Success,
                string.IsNullOrEmpty(result.Message) ? string.Empty : result.Message,
                result.Exception);
        }

        private OptionsData GetOptions(IEnumerable<IDataTrait> traits)
        {
            var options = new OptionsData();

            var dataTraits = traits as IList<IDataTrait> ?? traits.ToList();
            var waitForPersistence = dataTraits.FirstOrDefault(x => x is WaitForPersistence);
            var waitForReplication = dataTraits.FirstOrDefault(x => x is WaitForReplication);
            var expiry = dataTraits.FirstOrDefault(x => x is FiniteLifetime);

            if (waitForPersistence != null)
            {
                options.PersistTo = ((WaitForPersistence) waitForPersistence).PersistTo;
            }

            if (waitForReplication != null)
            {
                options.ReplicateTo = ((WaitForReplication) waitForReplication).ReplicateTo;
            }

            if (expiry != null)
            {
                options.Expiry = ((FiniteLifetime) expiry).Lifetime;
            }

            return options;
        }

        private class OptionsData
        {
            public OptionsData()
            {
                PersistTo = PersistTo.Zero;
                ReplicateTo = ReplicateTo.Zero;
                Expiry = 0;
                MaxDegreesOfParallelism = 0;
            }

            public PersistTo PersistTo { get; set; }

            public ReplicateTo ReplicateTo { get; set; }

            public int Expiry { get; set; }

            public int MaxDegreesOfParallelism { get; set; }
        }
    }
}
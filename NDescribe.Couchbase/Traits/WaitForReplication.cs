﻿using Couchbase;
using NDescribe.Core.DocumentGeneration.Traits;

namespace NDescribe.Couchbase.Traits
{
    /// <summary>
    /// Allows forcing Couchbase databridge to wait for replication for document writes on this document and associated documents
    /// </summary>
    public class WaitForReplication : IDataTrait
    {
        /// <summary>
        /// Number of servers to wait to replicate to
        /// </summary>
        public ReplicateTo ReplicateTo { get; }

        /// <summary>
        /// Allows forcing Couchbase databridge to wait for replication for document writes on this document and associated documents
        /// </summary>
        /// <param name="replicateTo">Number of servers to replicate to</param>
        public WaitForReplication(ReplicateTo replicateTo)
        {
            ReplicateTo = replicateTo;
        }

        /// <summary>
        /// Allows forcing Couchbase databridge to wait for replication for document writes on this document and associated documents
        /// </summary>
        public WaitForReplication()
            : this(ReplicateTo.One)
        {
        }
    }
}
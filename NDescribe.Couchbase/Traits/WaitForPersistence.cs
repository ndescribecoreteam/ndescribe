﻿using Couchbase;
using NDescribe.Core.DocumentGeneration.Traits;

namespace NDescribe.Couchbase.Traits
{
    /// <summary>
    /// Allows forcing Couchbase databridge to wait for persistence for document writes on this document and associated documents
    /// </summary>
    public class WaitForPersistence : IDataTrait
    {
        /// <summary>
        /// Number of servers to wait for persistence for
        /// </summary>
        public PersistTo PersistTo { get; }

        /// <summary>
        /// Allows forcing Couchbase databridge to wait for persistence for document writes on this document and associated documents
        /// </summary>
        /// <param name="persistTo">Number of servers to wait for persistence on</param>
        public WaitForPersistence(PersistTo persistTo)
        {
            PersistTo = persistTo;
        }

        /// <summary>
        /// Allows forcing Couchbase databridge to wait for persistence for document writes on this document and associated documents
        /// </summary>
        public WaitForPersistence()
            : this(PersistTo.One)
        {
        }
    }
}

﻿using NDescribe.Core.DocumentGeneration.Traits;

namespace NDescribe.Couchbase.Traits
{
    /// <summary>
    /// Gives a document an expiry time
    /// </summary>
    public class FiniteLifetime : IDataTrait
    {
        /// <summary>
        /// Lifetime in ms for this document
        /// </summary>
        public int Lifetime { get; }

        /// <summary>
        /// Gives a document an expiry time
        /// </summary>
        /// <param name="lifetime">Lifetime in ms for this document</param>
        public FiniteLifetime(int lifetime)
        {
            Lifetime = lifetime;
        }
    }
}

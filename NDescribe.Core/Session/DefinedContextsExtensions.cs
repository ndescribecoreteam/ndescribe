﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NDescribe.Core.Session
{
    /// <summary>
    /// Extensions specifically for handling general behaviour for defined contexts
    /// </summary>
    internal static class DefinedContextsExtensions
    {
        /// <summary>
        /// Retrieve document details for a specific type (or an implemented interface).
        /// </summary>
        /// <param name="definedContexts">Defined contexts to search</param>
        /// <typeparam name="TDocument">Document type to search for</typeparam>
        /// <returns>Found document details</returns>
        /// <exception cref="NDescribeException">Thrown when the type is undefined</exception>
        public static DocumentDetails GetDocumentDetailsForType<TDocument>(
            this IDictionary<Type, DocumentDetails> definedContexts, bool throwOnError = true)
        {
            var type = typeof(TDocument);

            if (definedContexts.ContainsKey(type))
            {
                return definedContexts[type];
            }

            if (throwOnError)
            {
                throw new NDescribeException(
                    $"Could not find a defined context for this document type or any of its interfaces: {type.FullName}");
            }

            return null;
        }
    }
}
﻿using NDescribe.Core.DataBridge;
using NDescribe.Core.DocumentGeneration;
using NDescribe.Core.DocumentGeneration.Patterns;
using NDescribe.Core.Serialization;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NDescribe.Core.DocumentGeneration.Traits;

namespace NDescribe.Core.Session
{
    internal class ReadRangeProcessor
    {
        private static DataRetrievalResult<IDictionary<string, DataRetrievalResult<TDocument>>> ReadRangeForInterface
            <TDocument, TPatternHandler>(
            TDocument documentTemplate, OperationContext context, IDataBridge dataBridge,
            ISerializationProvider serializationProvider, IDictionary<Type, DocumentDetails> definedContexts, string expectedVersion)
            where TPatternHandler : IRangeHandler
        {
            // Fairly complex reflection in here to deal with the transitions between generic types
            var assignableTypes = definedContexts.Where(x => typeof (TDocument).IsAssignableFrom(x.Key)).Select(x => x.Key);

            // Do calls for retrieving ranges for any assignable types for this interface
            var dataRetrievalResults = assignableTypes.AsParallel().Select(x => typeof (ReadRangeProcessor).GetMethod(nameof(ReadRange))
                .MakeGenericMethod(x, typeof(TPatternHandler))
                .Invoke(null, new object[]
                {
                    documentTemplate, context, dataBridge, serializationProvider, definedContexts, false, expectedVersion
                })).ToList();

            
            var results = new Dictionary<string, DataRetrievalResult<TDocument>>();

            // Get all the property names we'll need in a minute
            const string documentPropertyName = nameof(DataRetrievalResult<TDocument>.Document);
            const string successPropertyName = nameof(DataRetrievalResult<TDocument>.Success);
            const string messagePropertyName = nameof(DataRetrievalResult<TDocument>.Message);
            const string exceptionPropertyName = nameof(DataRetrievalResult<TDocument>.Exception);

            foreach (var dataRetrievalResult in dataRetrievalResults)
            {
                // Read the KeyValuePair (hiding its genericity)
                var dataRetrievalResultType = dataRetrievalResult.GetType();

                var keyValuePairs = dataRetrievalResultType.GetProperty(documentPropertyName).GetValue(dataRetrievalResult) as IDictionary;

                if (keyValuePairs != null)
                {
                    foreach (var keyValuePair in keyValuePairs)
                    {
                        var pair = (DictionaryEntry) keyValuePair;

                        // Get all values for the new result (that will use the base interface)
                        var pairValueType = pair.Value.GetType();
                        var realValue = (TDocument) pairValueType.GetProperty(documentPropertyName).GetValue(pair.Value);
                        var singleSuccess = (bool)pairValueType.GetProperty(successPropertyName).GetValue(pair.Value);
                        var singleMessage = pairValueType.GetProperty(messagePropertyName).GetValue(pair.Value) as string;
                        var singleException = pairValueType.GetProperty(exceptionPropertyName).GetValue(pair.Value) as Exception;
                        var key = pair.Key as string;

                        if (!string.IsNullOrEmpty(key))
                        {
                            results.Add(key,
                                new DataRetrievalResult<TDocument>(realValue, singleSuccess, singleMessage,
                                    singleException));
                        }
                    }
                }
            }

            var success = results.All(x => x.Value.Success);

            return new DataRetrievalResult<IDictionary<string, DataRetrievalResult<TDocument>>>(results, success,
                success ? default(string) : "Error reading range, please check results for reason.", null);
        }

        public static DataRetrievalResult<IDictionary<string, DataRetrievalResult<TDocument>>> ReadRange<TDocument, TPatternHandler>(
            TDocument documentTemplate, OperationContext context, IDataBridge dataBridge,
            ISerializationProvider serializationProvider, IDictionary<Type, DocumentDetails> definedContexts, bool includeDerivedTypes, string expectedVersion)
            where TPatternHandler : IRangeHandler
        {
            bool throwOnError = true;
            DataRetrievalResult<IDictionary<string, DataRetrievalResult<TDocument>>> initialValue = null;

            if (includeDerivedTypes && typeof (TDocument).IsInterface)
            {
                initialValue = ReadRangeForInterface<TDocument, TPatternHandler>(documentTemplate, context, dataBridge, serializationProvider, definedContexts, expectedVersion);

                if (initialValue.Success)
                {
                    throwOnError = false;
                }
            }

            var contextInformation = definedContexts.GetDocumentDetailsForType<TDocument>(throwOnError);

            if (contextInformation == null)
            {
                if (initialValue != null)
                {
                    return initialValue;
                }

                return
                    new DataRetrievalResult<IDictionary<string, DataRetrievalResult<TDocument>>>(
                        new Dictionary<string, DataRetrievalResult<TDocument>>(), false, "Type not found", null);
            }

            var definedContext = contextInformation.DefinedContext;
            var internalDefinition = (IInternalDefinedContext<TDocument>) definedContext;

            var handler = (IRangeHandler)internalDefinition.AllHandlers.FirstOrDefault(x => x.GetType().GetGenericTypeDefinition() == typeof(TPatternHandler).GetGenericTypeDefinition());

            if (handler == null)
            {
                return
                    new DataRetrievalResult<IDictionary<string, DataRetrievalResult<TDocument>>>(
                        new Dictionary<string, DataRetrievalResult<TDocument>>(), false,
                        $"Could not find handler: {typeof (TPatternHandler).Name}", null);
            }

            try
            {
                string initialKey;
                if (handler is IKeyGenerationHandler<TDocument>)
                {
                    var keyHandler = handler as IKeyGenerationHandler<TDocument>;

                    initialKey = internalDefinition.GenerateKeyToHandler(documentTemplate, contextInformation, keyHandler, context, dataBridge);
                }
                else
                {
                    initialKey = internalDefinition.GenerateFullKey(documentTemplate, contextInformation, context, dataBridge, false);
                }

                var rangeResult = handler.HandleRangeRequest(initialKey, contextInformation, context, dataBridge);
                IEnumerable<string> keys = rangeResult.Item;

                if (handler is IKeyGenerationHandler<TDocument>)
                {
                    var keyHandler = handler as IKeyGenerationHandler<TDocument>;
                    keys = rangeResult.Item
                        .Select(
                            x => internalDefinition.GenerateKeyFromHandler(documentTemplate, contextInformation,
                                keyHandler, x, context, dataBridge)).ToList();
                }

                keys = keys.ToList();

                var preReadProcessingResults =
                    keys
                        .Select(
                            singleKey =>
                                new {
                                    Key = singleKey,
                                    Value = ReadProcessor.ProcessPreRead(singleKey, contextInformation, context, dataBridge,
                                        internalDefinition)
                                })
                        .ToDictionary(x => x.Key, x => x.Value);

                var readResults = dataBridge.GetDocuments(contextInformation.DataLocation,
                    preReadProcessingResults.Where(x => x.Value.Success).Select(x => x.Value.Item.Key),
                    context.ContextualTraits.OfType<IDataTrait>());

                var postReadProcessingResults =
                    readResults.Document
                        .Where(x => x.Value.Success)
                        .Select(
                            singleReadResult =>
                                new
                                {
                                    singleReadResult.Key,
                                    Value = ReadProcessor.ProcessPostRead(contextInformation,
                                        serializationProvider, context, dataBridge, singleReadResult.Value.Document,
                                        ((IInternalDefinedContext<TDocument>) contextInformation.DefinedContext)
                                            .PostDeserializationHandlers, expectedVersion)
                                }).ToList();

                var fullResults = preReadProcessingResults.Where(x => !x.Value.Success)
                    .Select(x => new
                    {
                        x.Key,
                        Value =
                            new DataRetrievalResult<TDocument>(default(TDocument), false, x.Value.Message, x.Value.Exception)
                    })
                    .Union(readResults.Document.Where(x => !x.Value.Success)
                        .Select(x => new
                        {
                            x.Key,
                            Value =
                                new DataRetrievalResult<TDocument>(default(TDocument), false, x.Value.Message,
                                    x.Value.Exception)
                        }))
                    .Union(postReadProcessingResults.Select(x => new
                    {
                        x.Key,
                        x.Value
                    }));

                var dictionary = fullResults.ToDictionary(x => x.Key, x => x.Value);

                if (initialValue != null)
                {
                    dictionary = dictionary.Union(initialValue.Document).GroupBy(x => x.Key).Select(x => x.First()).ToDictionary(x => x.Key, x => x.Value);
                }

                var success = dictionary.All(x => x.Value.Success);
                return new DataRetrievalResult<IDictionary<string, DataRetrievalResult<TDocument>>>(dictionary, success,
                    success ? default(string) : "Error retrieving range, see individual results for errors", null);
            }
            catch (Exception e)
            {
                return
                    new DataRetrievalResult<IDictionary<string, DataRetrievalResult<TDocument>>>(
                        new Dictionary<string, DataRetrievalResult<TDocument>>(), false,
                        $"Error reading range: {e.Message}", e);
            }
        }
    }
}
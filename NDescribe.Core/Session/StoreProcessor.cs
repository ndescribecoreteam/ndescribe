﻿using NDescribe.Core.DataBridge;
using NDescribe.Core.DocumentGeneration;
using NDescribe.Core.DocumentGeneration.Patterns;
using NDescribe.Core.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using NDescribe.Core.DocumentGeneration.Traits;

namespace NDescribe.Core.Session
{
    internal static class StoreProcessor
    {
        public static DataResult SaveDocument<TDocument>(TDocument document,
            OperationContext context, IDataBridge dataBridge,
            ISerializationProvider serializationProvider, IDictionary<Type, DocumentDetails> definedContexts, string version)
        {
            return StoreDocument(document, context, dataBridge, serializationProvider, definedContexts, dataBridge.SaveDocument, version);
        }

        public static DataResult InsertDocument<TDocument>(TDocument document,
            OperationContext context, IDataBridge dataBridge,
            ISerializationProvider serializationProvider, IDictionary<Type, DocumentDetails> definedContexts, string version)
        {
            return StoreDocument(document, context, dataBridge, serializationProvider, definedContexts,
                dataBridge.CreateDocument, version);
        }


        private static DataResult StoreDocument<TDocument>(TDocument document,
            OperationContext context, IDataBridge dataBridge,
            ISerializationProvider serializationProvider, IDictionary<Type, DocumentDetails> definedContexts, Func<string, string, string, IEnumerable<IDataTrait>, DataResult> storageMethod, string version)
        {
            var contextInformation = definedContexts.GetDocumentDetailsForType<TDocument>();
            var definedContext = contextInformation.DefinedContext;
            var dataLocation = contextInformation.DataLocation;
            var internalDefinition = (IInternalDefinedContext<TDocument>) definedContext;

            IDocumentWrapper<TDocument> wrapper = new DocumentWrapper<TDocument>(version, document, context.Metadata);

            var key = internalDefinition.GenerateInitialKey(document);

            var keyGenerationResult = internalDefinition.KeyGenerationHandlers.Aggregate(
                new PatternResult<string>(key, true, string.Empty, null, context),
                (result, handler) => !result.Success
                    ? result
                    : handler.HandlePreStoreKeyGeneration(result.Item,  document, contextInformation, context, dataBridge));

            if (!keyGenerationResult.Success)
            {
                return new DataResult(false, keyGenerationResult.Message, keyGenerationResult.Exception);
            }

            key = keyGenerationResult.Item;

            var preSerializationResult =
                internalDefinition.PreSerializationHandlers.Aggregate(
                    new PatternResult<IDocumentWrapper<TDocument>>(wrapper, true, string.Empty,
                        null, context),
                    (result, handler) =>
                        !result.Success
                            ? result
                            : handler.HandlePreSerialization(result.Item, contextInformation, key, context, dataBridge));

            if (!preSerializationResult.Success)
            {
                return new DataResult(false, preSerializationResult.Message, preSerializationResult.Exception);
            }

            wrapper = preSerializationResult.Item;

            string serializedDocument = serializationProvider.Serialize(wrapper);
            StorageOptions storageOptions = new StorageOptions(key, serializedDocument);

            var preStorageResult =
                internalDefinition.PreStorageHandlers.Aggregate(
                    new PatternResult<StorageOptions>(storageOptions, true, string.Empty, null,
                        context),
                    (result, handler) =>
                        !result.Success
                            ? result
                            : handler.HandlePreStorage(result.Item, contextInformation, context, dataBridge));

            if (!preStorageResult.Success)
            {
                return new DataResult(false, preStorageResult.Message, preStorageResult.Exception);
            }

            storageOptions = preStorageResult.Item;

            var traits = internalDefinition.DataTraits.Union(context.ContextualTraits.OfType<IDataTrait>());

            var storeResult = storageMethod(dataLocation, storageOptions.Key,
                storageOptions.SerializedDocument, traits);

            if (!storeResult.Success)
            {
                return new DataResult(false, storeResult.Message, storeResult.Exception);
            }

            var postStorageResult =
                internalDefinition.PostStorageHandlers.Aggregate(
                    new PatternResult<DataResult>(storeResult, true, string.Empty, null,
                        context),
                    (result, handler) =>
                        !result.Success
                            ? result
                            : handler.HandlePostStore(key, wrapper, contextInformation, context, dataBridge, result.Item));

            if (!postStorageResult.Success)
            {
                return new DataResult(false, postStorageResult.Message, postStorageResult.Exception);
            }

            return postStorageResult.Item;
        }
    }
}
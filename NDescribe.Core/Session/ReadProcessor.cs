﻿using NDescribe.Core.DataBridge;
using NDescribe.Core.DocumentGeneration;
using NDescribe.Core.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using NDescribe.Core.DocumentGeneration.Patterns;
using NDescribe.Core.DocumentGeneration.Traits;

namespace NDescribe.Core.Session
{
    internal class ReadProcessor
    {
        public static DataRetrievalResult<TDocument> ReadDocument<TDocument>(TDocument documentTemplate, IDataBridge dataBridge,
            ISerializationProvider serializationProvider, IDictionary<Type, DocumentDetails> definedContexts, string expectedVersion)
        {
            return ReadDocument(documentTemplate, new OperationContext(), dataBridge, serializationProvider,
                definedContexts, expectedVersion);
        }

        public static DataRetrievalResult<TDocument> ReadDocument<TDocument>(TDocument documentTemplate,
            OperationContext context, IDataBridge dataBridge,
            ISerializationProvider serializationProvider, IDictionary<Type, DocumentDetails> definedContexts, string expectedVersion)
        {
            try
            {
                var contextInformation = definedContexts.GetDocumentDetailsForType<TDocument>();
                var definedContext = contextInformation.DefinedContext;
                var internalDefinition = (IInternalDefinedContext<TDocument>)definedContext;

                var key = internalDefinition.GenerateInitialKey(documentTemplate);

                var keyGenerationResult = internalDefinition.KeyGenerationHandlers.Aggregate(
                    new PatternResult<string>(key, true, string.Empty, null, context),
                    (result, handler) => !result.Success
                        ? result
                        : handler.HandlePreReadKeyGeneration(result.Item, documentTemplate, contextInformation, context, dataBridge));

                if (!keyGenerationResult.Success)
                {
                    return new DataRetrievalResult<TDocument>(default(TDocument), false, keyGenerationResult.Message, keyGenerationResult.Exception);
                }

                key = keyGenerationResult.Item;
                return ReadFromKey<TDocument>(key, contextInformation, serializationProvider, context, dataBridge, expectedVersion);
            }
            catch (Exception ex)
            {
                return new DataRetrievalResult<TDocument>(default(TDocument), false,
                    "Error in Read (see exception for more detail)", ex);
            }
        }

        public static DataRetrievalResult<TDocument> ReadFromKey<TDocument>(string key, DocumentDetails documentDetails,
            ISerializationProvider serializationProvider, OperationContext context, IDataBridge dataBridge, string expectedVersion)
        {
            var internalDefinition = (IInternalDefinedContext<TDocument>) documentDetails.DefinedContext;
            var dataLocation = documentDetails.DataLocation;

            PatternResult<ReadOptions> preReadResult = ProcessPreRead(key, documentDetails, context, dataBridge, internalDefinition);

            if (!preReadResult.Success)
            {
                return new DataRetrievalResult<TDocument>(default(TDocument), false, preReadResult.Message, preReadResult.Exception);
            }

            var traits = internalDefinition.DataTraits.Union(context.ContextualTraits.OfType<IDataTrait>());

            var getResult = dataBridge.GetDocument(dataLocation, preReadResult.Item.Key, traits);

            if (!getResult.Success)
            {
                return new DataRetrievalResult<TDocument>(default(TDocument), false, getResult.Message,
                    getResult.Exception);
            }

            return ProcessPostRead(documentDetails, serializationProvider, context, dataBridge, getResult.Document, internalDefinition.PostDeserializationHandlers, expectedVersion);
        }

        public static DataRetrievalResult<TDocument> ProcessPostRead<TDocument>(DocumentDetails documentDetails,
            ISerializationProvider serializationProvider, OperationContext context, IDataBridge dataBridge,
            string documentText, IEnumerable<IPostDeserializationHandler<TDocument>> postDeserializationHandlers, string expectedVersion)
        {
            var document =
                serializationProvider.Deserialize<IDocumentWrapper<TDocument>>(documentText);

            foreach (var handler in postDeserializationHandlers)
            {
                var result = handler.HandlePostDeserialization(document, documentDetails, context,
                    dataBridge);

                if (!result.Success)
                {
                    return new DataRetrievalResult<TDocument>(default(TDocument), false, result.Message,
                        result.Exception);
                }

                context = result.Context;
                document = result.Item;
            }

            if (document.Version != expectedVersion)
            {
                return new DataRetrievalResult<TDocument>(default(TDocument), false,
                    $"Expected version was {expectedVersion} but document was version {document.Version}", null);
            }

            return new DataRetrievalResult<TDocument>(document.Document, true, string.Empty,
                null);
        }

        public static PatternResult<ReadOptions> ProcessPreRead<TDocument>(string key, DocumentDetails documentDetails, OperationContext context,
            IDataBridge dataBridge, IInternalDefinedContext<TDocument> internalDefinition)
        {
            var readOptions = new ReadOptions(key);
            return internalDefinition.PreReadHandlers.Aggregate(
                new PatternResult<ReadOptions>(readOptions, true, string.Empty, null,
                    context),
                (currentResult, handler) =>
                    !currentResult.Success
                        ? currentResult
                        : handler.HandlePreRead(readOptions, context, documentDetails, dataBridge));
        }
    }
}
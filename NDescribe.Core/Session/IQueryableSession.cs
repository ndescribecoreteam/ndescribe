﻿using System;
using NDescribe.Core.DataBridge;
using NDescribe.Core.DocumentGeneration.Patterns;
using NDescribe.Core.Serialization;
using System.Collections.Generic;

namespace NDescribe.Core.Session
{
    /// <summary>
    /// Special type of session interface for making queries about the session
    /// </summary>
    public interface IQueryableSession
    {
        /// <summary>
        /// Data bridge for performing operations on the document
        /// </summary>
        IDataBridge DataBridge { get; }

        /// <summary>
        /// Serialization provider to use for serializing data
        /// </summary>
        ISerializationProvider SerializationProvider { get; }

        /// <summary>
        /// Get the document details for the given type
        /// </summary>
        /// <typeparam name="TDocument">Type to query</typeparam>
        /// <returns>The document details for this type</returns>
        DocumentDetails GetDocumentDetailsForType<TDocument>();

        /// <summary>
        /// Get a handler for the given document type
        /// </summary>
        /// <typeparam name="TDocument">Document type to query</typeparam>
        /// <typeparam name="TPatternHandler">Pattern handler type to retrieve</typeparam>
        /// <returns>The constructed pattern handler</returns>
        TPatternHandler GetPatternHandlerForDocument<TDocument, TPatternHandler>() where TPatternHandler : class;

        /// <summary>
        /// Retrieve a range of values
        /// </summary>
        /// <typeparam name="TDocument">Document type to retrieve values for</typeparam>
        /// <typeparam name="TPatternHandler">Pattern handler to retrieve range values for</typeparam>
        /// <returns>A collection of documents</returns>
        DataRetrievalResult<IDictionary<string, DataRetrievalResult<TDocument>>> ReadRange<TDocument, TPatternHandler>(TDocument documentTemplate)
            where TPatternHandler : IRangeHandler;

        /// <summary>
        /// Retrieve a range of values
        /// </summary>
        /// <typeparam name="TDocument">Document type to retrieve values for</typeparam>
        /// <param name="documentTemplate">Document template to use when reading a range</param>
        /// <param name="context">Document context to use for reading the document range</param>
        /// <param name="includeDerivedTypes">Whether to include derived types in calls or not</param>
        /// <typeparam name="TPatternHandler">Pattern handler to retrieve range values for</typeparam>
        /// <returns>A collection of documents</returns>
        DataRetrievalResult<IDictionary<string, DataRetrievalResult<TDocument>>> ReadRange<TDocument, TPatternHandler>(TDocument documentTemplate,
            OperationContext context, bool includeDerivedTypes) where TPatternHandler : IRangeHandler;

        /// <summary>
        /// Retrieve a range of values
        /// </summary>
        /// <typeparam name="TDocument">Document type to retrieve values for</typeparam>
        /// <param name="documentTemplate">Document template to use when reading a range</param>
        /// <param name="includeDerivedTypes">Whether to include derived types in calls or not</param>
        /// <typeparam name="TPatternHandler">Pattern handler to retrieve range values for</typeparam>
        /// <returns>A collection of documents</returns>
        DataRetrievalResult<IDictionary<string, DataRetrievalResult<TDocument>>> ReadRange<TDocument, TPatternHandler>(TDocument documentTemplate,
            bool includeDerivedTypes) where TPatternHandler : IRangeHandler;


        /// <summary>
        /// Event for alerting watchers of failed data operations
        /// </summary>
        event EventHandler<DataWriteOperationFailedEventArgs> DataWriteOperationFailedEvent;
    }
}
﻿using System;

namespace NDescribe.Core.Session
{
    /// <summary>
    /// Contains details about a single document definition
    /// </summary>
    public class DocumentDetails
    {
        /// <summary>
        /// Model type that this document refers to
        /// </summary>
        public Type ModelType { get; private set; }

        /// <summary>
        /// Location to store this document
        /// </summary>
        public string DataLocation { get; private set; }

        /// <summary>
        /// Defined context for this document
        /// </summary>
        public IDefinedContext DefinedContext { get; private set; }

        /// <summary>
        /// Create a new document details object
        /// </summary>
        /// <param name="modelType">Model type that this document refers to</param>
        /// <param name="dataLocation">Location to store this document</param>
        /// <param name="definedContext">Defined context for this document</param>
        public DocumentDetails(Type modelType, string dataLocation, IDefinedContext definedContext)
        {
            ModelType = modelType;
            DataLocation = dataLocation;
            DefinedContext = definedContext;
        }
    }
}
﻿using System;
using NDescribe.Core.DataBridge;

namespace NDescribe.Core.Session
{
    /// <summary>
    /// Represents a failed data operation
    /// </summary>
    public class DataWriteOperationFailedEventArgs : EventArgs
    {
        /// <summary>
        /// Create a new set of event args
        /// </summary>
        /// <param name="dataResult">DataResult that caused the failure</param>
        public DataWriteOperationFailedEventArgs(DataResult dataResult)
        {
            DataResult = dataResult;
        }

        /// <summary>
        /// Result that caused the failure
        /// </summary>
        public DataResult DataResult { get; }
    }
}
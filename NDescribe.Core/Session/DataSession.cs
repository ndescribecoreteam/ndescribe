﻿using System;
using System.Collections.Generic;
using NDescribe.Core.DataBridge;
using NDescribe.Core.Serialization;
using NDescribe.Core.DocumentGeneration.Patterns;
using NDescribe.Core.Configuration;
using System.Linq;
using NDescribe.Core.DocumentGeneration;

namespace NDescribe.Core.Session
{
    internal class DataSession : IDataSession, IQueryableSession
    {
        public string Version { get; }

        public IDataBridge DataBridge { get; }

        public ISerializationProvider SerializationProvider { get; }

        public IDictionary<Type, DocumentDetails> DefinedContexts { get; }

        public DataSession(ConfigurationOptions configurationOptions, IEnumerable<DataContext> dataContexts, string version)
        {
            DataBridge = configurationOptions.DataBridgeConfigurationProvider.Create();
            SerializationProvider = configurationOptions.SerializationProvider;
            Version = version;

            var definedContexts =
                dataContexts.SelectMany(
                    x =>
                        from definedContext in x.DefinedContexts
                        select new {x.DataLocation, DefinedContext = definedContext});
            DefinedContexts = definedContexts.ToDictionary(x => x.DefinedContext.DocumentType,
                x => new DocumentDetails(x.DefinedContext.DocumentType, x.DataLocation, x.DefinedContext));
            ProcessPostDefinedHandlers();
        }

        public DataRetrievalResult<TDocument> Read<TDocument>(string key)
        {
            var definedContext = DefinedContexts[typeof (TDocument)];
            return ReadProcessor.ReadFromKey<TDocument>(key, definedContext, SerializationProvider, new OperationContext(), DataBridge, Version);
        }

        public DataResult Delete<TDocument>(TDocument documentTemplate)
        {
            return Delete(documentTemplate, new OperationContext());
        }

        private DataResult RaiseDataWriteError(DataResult result)
        {
            if (!result.Success && DataWriteOperationFailedEvent != null)
            {
                DataWriteOperationFailedEvent(this, new DataWriteOperationFailedEventArgs(result));
            }

            return result;
        }

        public DataResult Delete<TDocument>(TDocument documentTemplate, OperationContext context)
        {
            return RaiseDataWriteError(DeleteProcessor.DeleteDocument(documentTemplate, DataBridge, SerializationProvider, DefinedContexts, context));
        }

        public DataRetrievalResult<TDocument> Read<TDocument>(TDocument documentTemplate)
        {
            return ReadProcessor.ReadDocument(documentTemplate, DataBridge, SerializationProvider,
                DefinedContexts, Version);
        }

        public DataResult Create<TDocument>(TDocument document)
        {
            return Create(document, new OperationContext());
        }

        public DataResult Create<TDocument>(TDocument document, OperationContext context)
        {
            return RaiseDataWriteError(StoreProcessor.InsertDocument(document, context, DataBridge, SerializationProvider,
                DefinedContexts, Version));
        }

        public DataResult Save<TDocument>(TDocument document)
        {
            return Save(document, new OperationContext());
        }

        public DataResult Save<TDocument>(TDocument document, OperationContext context)
        {
            return RaiseDataWriteError(StoreProcessor.SaveDocument(document, context, DataBridge, SerializationProvider,
                DefinedContexts, Version));
        }

        public DataRetrievalResult<IDictionary<string, DataRetrievalResult<TDocument>>> ReadRange
            <TDocument, TPatternHandler>(
            TDocument documentTemplate) where TPatternHandler : IRangeHandler
        {
            return ReadRange<TDocument, TPatternHandler>(documentTemplate, new OperationContext(), false);
        }

        public DataRetrievalResult<IDictionary<string, DataRetrievalResult<TDocument>>> ReadRange<TDocument, TPatternHandler>(
            TDocument documentTemplate, bool includeDerivedTypes) where TPatternHandler : IRangeHandler
        {
            return ReadRange<TDocument, TPatternHandler>(documentTemplate, new OperationContext(), includeDerivedTypes);
        }

        public DataRetrievalResult<TDocument> Read<TDocument>(TDocument documentTemplate,
            OperationContext context)
        {
            return ReadProcessor.ReadDocument(documentTemplate, context, DataBridge, SerializationProvider,
                DefinedContexts, Version);
        }

        public DataRetrievalResult<IDictionary<string, DataRetrievalResult<TDocument>>> ReadRange<TDocument, TPatternHandler>(
            TDocument documentTemplate, OperationContext context, bool includeDerivedTypes)
            where TPatternHandler : IRangeHandler
        {
            return ReadRangeProcessor.ReadRange<TDocument, TPatternHandler>(documentTemplate, context,
                DataBridge, SerializationProvider, DefinedContexts, includeDerivedTypes, Version);
        }

        public event EventHandler<DataWriteOperationFailedEventArgs> DataWriteOperationFailedEvent;

        public DocumentDetails GetDocumentDetailsForType<TDocument>()
        {
            return DefinedContexts[typeof (TDocument)];
        }

        public TPatternHandler GetPatternHandlerForDocument<TDocument, TPatternHandler>() where TPatternHandler : class
        {
            var details = GetDocumentDetailsForType<TDocument>();
            var internalDefinedType = (IInternalDefinedContext<TDocument>) details.DefinedContext;

            var postDeserializationHandler =
                internalDefinedType.PostDeserializationHandlers.FirstOrDefault(x => x is TPatternHandler);
            if (postDeserializationHandler != null)
            {
                return (TPatternHandler) postDeserializationHandler;
            }

            var postStorageHandler = internalDefinedType.PostStorageHandlers.FirstOrDefault(x => x is TPatternHandler);
            if (postStorageHandler != null)
            {
                return (TPatternHandler) postStorageHandler;
            }

            var preReadHandler = internalDefinedType.PreReadHandlers.FirstOrDefault(x => x is TPatternHandler);
            if (preReadHandler != null)
            {
                return (TPatternHandler) preReadHandler;
            }

            var preSerializationHandler =
                internalDefinedType.PreSerializationHandlers.FirstOrDefault(x => x is TPatternHandler);
            if (preSerializationHandler != null)
            {
                return (TPatternHandler) preSerializationHandler;
            }

            var preStorageHandler = internalDefinedType.PreStorageHandlers.FirstOrDefault(x => x is TPatternHandler);
            if (preStorageHandler != null)
            {
                return (TPatternHandler) preStorageHandler;
            }

            var keyGenerationHandler =
                internalDefinedType.KeyGenerationHandlers.FirstOrDefault(x => x is TPatternHandler);

            return (TPatternHandler) keyGenerationHandler;
        }

        void ProcessPostDefinedHandlers()
        {
            foreach (
                var result in
                    DefinedContexts.Values.Select(
                        documentDetails => documentDetails.DefinedContext.RunPostDefinedHandlers(this, documentDetails))
                        .Where(result => !result.Success))
            {
                throw new NDescribeException(
                    $"Error running post definition handlers: {result.Message}");
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using NDescribe.Core.DataBridge;
using NDescribe.Core.DocumentGeneration;
using NDescribe.Core.DocumentGeneration.Patterns;
using NDescribe.Core.DocumentGeneration.Traits;
using NDescribe.Core.Serialization;

namespace NDescribe.Core.Session
{
    internal class DeleteProcessor
    {
        public static DataResult DeleteDocument<TDocument>(TDocument documentTemplate, IDataBridge dataBridge,
            ISerializationProvider serializationProvider, IDictionary<Type, DocumentDetails> definedContexts, OperationContext context)
        {
            var contextInformation = definedContexts.GetDocumentDetailsForType<TDocument>();
            var definedContext = contextInformation.DefinedContext;
            var internalDefinition = (IInternalDefinedContext<TDocument>)definedContext;

            var key = internalDefinition.GenerateInitialKey(documentTemplate);

            var keyGenerationResult = internalDefinition.KeyGenerationHandlers.Aggregate(
                new PatternResult<string>(key, true, string.Empty, null, context),
                (result, handler) => !result.Success
                    ? result
                    : handler.HandlePreReadKeyGeneration(result.Item, documentTemplate, contextInformation, context, dataBridge));

            if (!keyGenerationResult.Success)
            {
                return new DataResult(false, $"Failed to generate key from template: {keyGenerationResult.Message}", keyGenerationResult.Exception);
            }

            return DeleteDocumentFromKey(keyGenerationResult.Item, documentTemplate, dataBridge, serializationProvider, definedContexts, context);
        }

        public static DataResult DeleteDocumentFromKey<TDocument>(string key, TDocument documentTemplate, IDataBridge dataBridge,
            ISerializationProvider serializationProvider, IDictionary<Type, DocumentDetails> definedContexts,
            OperationContext context)
        {
            var contextInformation = definedContexts.GetDocumentDetailsForType<TDocument>();
            var definedContext = contextInformation.DefinedContext;
            var internalDefinition = (IInternalDefinedContext<TDocument>)definedContext;

            var traits = internalDefinition.DataTraits.Union(context.ContextualTraits.OfType<IDataTrait>());

            var deleteResult = dataBridge.DeleteDocument(contextInformation.DataLocation, key, traits);

            if (!deleteResult.Success)
            {
                return deleteResult;
            }

            var patternResult = internalDefinition.PostDeleteHandlers.Aggregate(
                new PatternResult<DataResult>(deleteResult, true, string.Empty, null,
                    context),
                (result, handler) =>
                    result.Success
                        ? handler.HandlePostDelete(key, documentTemplate, contextInformation, context,
                            dataBridge)
                        : result
                );

            if (!patternResult.Success)
            {
                return new DataResult(false, $"Error running pre-delete patterns: {patternResult.Message}",
                    patternResult.Exception);
            }

            if (!patternResult.Item.Success)
            {
                return patternResult.Item;
            }

            return patternResult.Item;
        }
    }
}

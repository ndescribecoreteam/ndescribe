﻿namespace NDescribe.Core.Serialization
{
    /// <summary>
    /// Used to serialize and deserialize a document
    /// </summary>
    public interface ISerializationProvider
    {
        /// <summary>
        /// Serialize a document
        /// </summary>
        /// <typeparam name="T">Type of document to serialize</typeparam>
        /// <param name="document">Document to serialize</param>
        /// <returns>The serialized document</returns>
        string Serialize<T>(T document);

        /// <summary>
        /// Deserialize a document
        /// </summary>
        /// <typeparam name="T">The type of document to deserialize</typeparam>
        /// <param name="document">Document to deserialize</param>
        /// <returns>The deserialized document</returns>
        T Deserialize<T>(string document);
    }
}
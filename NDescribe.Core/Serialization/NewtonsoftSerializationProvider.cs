﻿using Newtonsoft.Json;

namespace NDescribe.Core.Serialization
{
    internal class NewtonsoftSerializationProvider : ISerializationProvider
    {
        private readonly JsonSerializerSettings serializerSettings;

        public NewtonsoftSerializationProvider()
        {
            serializerSettings = new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.All,
                TypeNameAssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Full
            };
        }

        public T Deserialize<T>(string document)
        {
            return JsonConvert.DeserializeObject<T>(document, serializerSettings);
        }

        public string Serialize<T>(T document)
        {
            return JsonConvert.SerializeObject(document, serializerSettings);
        }
    }
}
﻿using System;
using System.Runtime.Serialization;

namespace NDescribe.Core
{
    /// <summary>
    /// Represents an exceptional error 
    /// </summary>
    public class NDescribeException : ApplicationException
    {
        /// <summary>
        /// Represents an exceptional error in NDescribe code
        /// </summary>
        public NDescribeException()
        {
        }

        /// <summary>
        /// Represents an exceptional error in NDescribe code
        /// </summary>
        /// <param name="message">Exception message</param>
        public NDescribeException(string message)
            : base(message)
        {            
        }

        /// <summary>
        /// Represents an exceptional error in NDescribe code
        /// </summary>
        /// <param name="message">Exception message</param>
        /// <param name="innerException">Contained child exception</param>
        public NDescribeException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        /// Represents an exceptional error in NDescribe code
        /// </summary>
        /// <param name="info">Serialization info</param>
        /// <param name="context">Streaming context</param>
        public NDescribeException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
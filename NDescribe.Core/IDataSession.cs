﻿using NDescribe.Core.DataBridge;

namespace NDescribe.Core
{
    /// <summary>
    /// Manage document storage
    /// </summary>
    public interface IDataSession
    {
        /// <summary>
        /// Store a document
        /// </summary>
        /// <typeparam name="TDocument">Type of document to store</typeparam>
        /// <param name="document">Document to store</param>
        /// <returns>Status of the operation</returns>
        DataResult Create<TDocument>(TDocument document);

        /// <summary>
        /// Store a document
        /// </summary>
        /// <typeparam name="TDocument">Type of document to store</typeparam>
        /// <param name="document">Document to store</param>
        /// <param name="context">Context to use for this document</param>
        /// <returns>Status of the operation</returns>
        DataResult Create<TDocument>(TDocument document, OperationContext context);

        /// <summary>
        /// Store a document
        /// </summary>
        /// <typeparam name="TDocument">Type of document to store</typeparam>
        /// <param name="document">Document to store</param>
        /// <returns>Status of the operation</returns>
        DataResult Save<TDocument>(TDocument document);

        /// <summary>
        /// Store a document
        /// </summary>
        /// <typeparam name="TDocument">Type of document to store</typeparam>
        /// <param name="document">Document to store</param>
        /// <param name="context">Context to use for this document</param>
        /// <returns>Status of the operation</returns>
        DataResult Save<TDocument>(TDocument document, OperationContext context);

        /// <summary>
        /// Read a document
        /// </summary>
        /// <typeparam name="TDocument">Type of document to read</typeparam>
        /// <param name="documentTemplate">A document which is setup with enough data to allow construction of its key</param>
        /// <returns>The retrieval status</returns>
        DataRetrievalResult<TDocument> Read<TDocument>(TDocument documentTemplate);

        /// <summary>
        /// Read a document
        /// </summary>
        /// <typeparam name="TDocument">Type of document to read</typeparam>
        /// <param name="documentTemplate">A document which is setup with enough data to allow construction of its key</param>
        /// <param name="context">Context to use for reading the document</param>
        /// <returns>The retrieval status</returns>
        DataRetrievalResult<TDocument> Read<TDocument>(TDocument documentTemplate,
            OperationContext context);

        /// <summary>
        /// Read a document
        /// </summary>
        /// <typeparam name="TDocument">Type of document to read</typeparam>
        /// <param name="key">The key of the document</param>
        /// <returns>The retrieval status</returns>
        DataRetrievalResult<TDocument> Read<TDocument>(string key);

        /// <summary>
        /// Delete a document
        /// </summary>
        /// <typeparam name="TDocument">Type of document to delete</typeparam>
        /// <param name="documentTemplate">Document template for the document to delete</param>
        /// <returns>The result of the delete operation</returns>
        DataResult Delete<TDocument>(TDocument documentTemplate);

        /// <summary>
        /// Delete a document
        /// </summary>
        /// <typeparam name="TDocument">Type of document to delete</typeparam>
        /// <param name="documentTemplate">Document template for the document to delete</param>
        /// <param name="context">Current context fo the delete operation</param>
        /// <returns>The result of the delete operation</returns>
        DataResult Delete<TDocument>(TDocument documentTemplate, OperationContext context);
    }
}
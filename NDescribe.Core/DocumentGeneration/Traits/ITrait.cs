﻿namespace NDescribe.Core.DocumentGeneration.Traits
{
    /// <summary>
    /// Represents an aspect of an object that is not directly supported by the core library
    /// </summary>
    public interface ITrait
    { 
    }
}
﻿namespace NDescribe.Core.DocumentGeneration.Traits
{
    /// <summary>
    /// A trait that might be databridge implementation specific
    /// </summary>
    public interface IDataTrait : ITrait
    {         
    }
}
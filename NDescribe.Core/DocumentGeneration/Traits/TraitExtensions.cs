﻿namespace NDescribe.Core.DocumentGeneration.Traits
{
    /// <summary>
    /// Extensions for handling traits
    /// </summary>
    public static class TraitExtensions
    {
        /// <summary>
        /// Add a trait to a type
        /// </summary>
        /// <typeparam name="TDocument">Type of document to add the trait to</typeparam>
        /// <typeparam name="TTrait">Trait type to add</typeparam>
        /// <param name="documentConfigurationBuilder">Document configuration builder to add the trait to</param>
        /// <returns>The document configuration builder for further operations</returns>
        public static IDocumentConfigurationBuilder<TDocument> WithTrait<TDocument, TTrait>(this IDocumentConfigurationBuilder<TDocument> documentConfigurationBuilder)
            where TTrait : ITrait, new()
        {
            TTrait trait = new TTrait();
            return WithTrait(documentConfigurationBuilder, trait);
        }

        /// <summary>
        /// Add a trait to a document
        /// </summary>
        /// <typeparam name="TDocument">Document type to add the trait to</typeparam>
        /// <typeparam name="TTrait">Trait type to add</typeparam>
        /// <param name="documentConfigurationBuilder">Configuration builder to add the trait to</param>
        /// <param name="trait">Trait to add</param>
        /// <returns>The configuration builder for further operations</returns>
        public static IDocumentConfigurationBuilder<TDocument> WithTrait<TDocument, TTrait>(
            this IDocumentConfigurationBuilder<TDocument> documentConfigurationBuilder, TTrait trait)
            where TTrait : ITrait
        {
            documentConfigurationBuilder.AddTrait(trait);
            return documentConfigurationBuilder;
        } 
    }
}
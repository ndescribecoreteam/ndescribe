﻿using System.Collections.Generic;

namespace NDescribe.Core.DocumentGeneration
{
    /// <summary>
    /// Document wrapper
    /// </summary>
    /// <typeparam name="TDocument">The type from the domain model</typeparam>
    public interface IDocumentWrapper<out TDocument>
    {
        /// <summary>
        /// Version of this document
        /// </summary>
        string Version { get; }

        /// <summary>
        /// Document to wrap
        /// </summary>
        TDocument Document { get; }

        /// <summary>
        /// Metadata about the document
        /// </summary>
        IDictionary<string, object> Metadata { get; } 
    }
}
﻿using System.Collections.Generic;
using NDescribe.Core.DataBridge;
using NDescribe.Core.DocumentGeneration.Patterns;
using NDescribe.Core.DocumentGeneration.Traits;
using NDescribe.Core.Session;

namespace NDescribe.Core.DocumentGeneration
{
    /// <summary>
    /// Internal context interface for built configuration
    /// </summary>
    public interface IInternalDefinedContext<TDocument>
    {
        /// <summary>
        /// Generate an initial unprocessed key for this document
        /// </summary>
        /// <param name="document">Document</param>
        /// <returns>The basic initial key</returns>
        string GenerateInitialKey(TDocument document);

        /// <summary>
        /// Generate a key for a specific handler
        /// </summary>
        /// <typeparam name="THandler">Type of the handler to generate the key for</typeparam>
        /// <param name="document">Document to generate the key from</param>
        /// <param name="documentDetails">Document details for the current document, and its definition</param>
        /// <param name="handler">Handler to generate the key for</param>
        /// <param name="context">Current context for this operation</param>
        /// <param name="dataBridge">The data bridge to use for accessing data</param>
        /// <returns>The generated key</returns>
        string GenerateKeyToHandler<THandler>(TDocument document, DocumentDetails documentDetails, THandler handler, OperationContext context, IDataBridge dataBridge) where THandler : IKeyGenerationHandler<TDocument>;

        /// <summary>
        /// Generate a key for a specific handler
        /// </summary>
        /// <typeparam name="THandler">Type of the handler to generate the key for</typeparam>
        /// <param name="document">Document to generate the key from</param>
        /// <param name="documentDetails">Document details for the current document, and its definition</param>
        /// <param name="handler">Handler to generate the key for</param>
        /// <param name="currentKey">The currently generated key</param>
        /// <param name="context">Current context for this operation</param>
        /// <param name="dataBridge">The data bridge to use for accessing data</param>
        /// <returns>The generated key</returns>
        string GenerateKeyFromHandler<THandler>(TDocument document, DocumentDetails documentDetails, THandler handler, string currentKey, OperationContext context, IDataBridge dataBridge) where THandler : IKeyGenerationHandler<TDocument>;

        /// <summary>
        /// Generate the full key for a document
        /// </summary>
        /// <param name="document">Document to generate the key for</param>
        /// <param name="documentDetails">Document details for the current document, and its definition</param>
        /// <param name="context">Current context for this operation</param>
        /// <param name="dataBridge">The data bridge to use for accessing data</param>
        /// <param name="dataAccessCall">Whether this is a data access call or just a query</param>
        /// <returns>The generated key</returns>
        string GenerateFullKey(TDocument document, DocumentDetails documentDetails, OperationContext context, IDataBridge dataBridge, bool dataAccessCall);

        /// <summary>
        /// All post-read handlers defined for this context
        /// </summary>
        List<IPostDeserializationHandler<TDocument>> PostDeserializationHandlers { get; }

        /// <summary>
        /// All post-storage handlers defined for this context
        /// </summary>
        List<IPostStorageHandler<TDocument>> PostStorageHandlers { get; }

        /// <summary>
        /// All pre-serialization handlers defined for this context
        /// </summary>
        List<IPreSerializationHandler<TDocument>> PreSerializationHandlers { get; }

        /// <summary>
        /// All pre-storage handlers defined for this context
        /// </summary>
        List<IPreStorageHandler> PreStorageHandlers { get; }

        /// <summary>
        /// All pre-read handlers defined for this context
        /// </summary>
        List<IPreReadHandler> PreReadHandlers { get; }

        /// <summary>
        /// All key generation handlers for this context
        /// </summary>
        List<IKeyGenerationHandler<TDocument>> KeyGenerationHandlers { get; }

        /// <summary>
        /// All pre-delete handlers defined fort his context
        /// </summary>
        List<IPostDeleteHandler<TDocument>> PostDeleteHandlers { get; }

            /// <summary>
        /// Retrieve all handlers
        /// </summary>
        IEnumerable<IPatternHandler> AllHandlers { get; }

        /// <summary>
        /// Traits for handling data operations
        /// </summary>
        List<IDataTrait> DataTraits { get; }
    }
}
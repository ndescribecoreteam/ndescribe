﻿using NDescribe.Core.DataBridge;
using System;
using NDescribe.Core.Session;

namespace NDescribe.Core.DocumentGeneration.Patterns.LockingPattern
{
    /// <summary>
    /// Extensions to handle the locking pattern
    /// </summary>
    public static class LockingPatternExtensions
    {
        /// <summary>
        /// The default number of seconds that we will wait for
        /// </summary>
        private const long DefaultExpirySeconds = 60;

        /// <summary>
        /// The default locking key format
        /// </summary>
        private const string DefaultLockingKeyFormat = "{0}::Lock";

        /// <summary>
        /// Make a document a locking document
        /// </summary>
        /// <param name="configurationBuilder">Configuration builder to add the document handlers to</param>
        /// <returns>The configured document configuration builder</returns>
        public static IDocumentConfigurationBuilder<TDocument> WithLocking<TDocument>(
            this IDocumentConfigurationBuilder<TDocument> configurationBuilder)
        {
            return WithLocking(configurationBuilder, x => string.Format(DefaultLockingKeyFormat, x));
        }

        /// <summary>
        /// Make a document a locking document
        /// </summary>
        /// <param name="configurationBuilder">Configuration builder to add the document handlers to</param>
        /// <param name="lockKeyModifier">The key format to use for this document</param>
        /// <returns>The configured document configuration builder</returns>
        public static IDocumentConfigurationBuilder<TDocument> WithLocking<TDocument>(
            this IDocumentConfigurationBuilder<TDocument> configurationBuilder, Func<string, string> lockKeyModifier)
        {
            return WithLocking(configurationBuilder, lockKeyModifier, DefaultExpirySeconds);
        }

        /// <summary>
        /// Make a document a locking document
        /// </summary>
        /// <param name="configurationBuilder">Configuration builder to add the document handlers to</param>
        /// <param name="lockKeyModifier">The key format to use for this document</param>
        /// <param name="lockExpirySeconds">Number of seconds to hold onto the lock document</param>
        /// <returns>The configured document configuration builder</returns>
        public static IDocumentConfigurationBuilder<TDocument> WithLocking<TDocument>(
            this IDocumentConfigurationBuilder<TDocument> configurationBuilder, Func<string, string> lockKeyModifier,
            long lockExpirySeconds)
        {
            return WithLocking(configurationBuilder, lockKeyModifier, lockExpirySeconds, true);
        }

        /// <summary>
        /// Make a document a locking document
        /// </summary>
        /// <param name="configurationBuilder">Configuration builder to add the document handlers to</param>
        /// <param name="lockKeyModifier">The key format to use for this document</param>
        /// <param name="lockExpirySeconds">Number of seconds to hold onto the lock document</param>
        /// <param name="alwaysLock">Always lock the document or only lock it when requested</param>
        /// <returns>The configured document configuration builder</returns>
        public static IDocumentConfigurationBuilder<TDocument> WithLocking<TDocument>(
            this IDocumentConfigurationBuilder<TDocument> configurationBuilder, Func<string, string> lockKeyModifier,
            long lockExpirySeconds, bool alwaysLock)
        {
            var handler = new LockingPatternHandler<TDocument>(lockKeyModifier, lockExpirySeconds);
            configurationBuilder.AddHandler(handler);
            return configurationBuilder;
        }

        /// <summary>
        /// Attempt to store a locking document without a lock
        /// </summary>
        /// <param name="dataSession">Data session currently being used</param>
        /// <param name="document">Document to store</param>
        /// <returns>The result of the operation</returns>
        public static DataResult SaveWithoutLock<TDocument>(this IDataSession dataSession, TDocument document)
        {
            var documentContext = new OperationContext
            {
                ContextualObjects = {[LockingPatternHandler<TDocument>.DoNotLockKey] = true}
            };
            return dataSession.Save(document, documentContext);
        }

        /// <summary>
        /// Acquire a lock on a document
        /// </summary>
        /// <typeparam name="TDocument">Type of document to lock</typeparam>
        /// <param name="dataSession">Data session to use</param>
        /// <param name="document">Document to lock</param>
        /// <param name="lockExpiry">Time for the lock to expire in seconds</param>
        /// <returns>Status of the locking operation</returns>
        public static DataRetrievalResult<bool> AcquireLock<TDocument>(this IDataSession dataSession, TDocument document,
            long lockExpiry)
        {
            return AcquireLock(dataSession, document, lockExpiry, new OperationContext());
        }

        /// <summary>
        /// Acquire a lock on a document
        /// </summary>
        /// <typeparam name="TDocument">Type of document to lock</typeparam>
        /// <param name="dataSession">Data session to use</param>
        /// <param name="document">Document to lock</param>
        /// <param name="lockExpiry">Time for the lock to expire in seconds</param>
        /// <param name="context">Current document context</param>
        /// <returns>Status of the locking operation</returns>
        public static DataRetrievalResult<bool> AcquireLock<TDocument>(this IDataSession dataSession, TDocument document,
            long lockExpiry, OperationContext context)
        {
            var queryableSession = (IQueryableSession) dataSession;
            return LockingPatternHandler<TDocument>.AcquireLock(queryableSession, document,
                x => string.Format(DefaultLockingKeyFormat, x), lockExpiry, context);
        }

        /// <summary>
        /// Acquire a lock on a document
        /// </summary>
        /// <typeparam name="TDocument">Type of document to lock</typeparam>
        /// <param name="dataSession">Data session to use</param>
        /// <param name="document">Document to lock</param>
        /// <returns>Status of the locking operation</returns>
        public static DataRetrievalResult<bool> AcquireLock<TDocument>(this IDataSession dataSession, TDocument document)
        {
            return AcquireLock(dataSession, document, new OperationContext());
        }

        /// <summary>
        /// Acquire a lock on a document
        /// </summary>
        /// <typeparam name="TDocument">Type of document to lock</typeparam>
        /// <param name="dataSession">Data session to use</param>
        /// <param name="document">Document to lock</param>
        /// <param name="context">Current document context</param>
        /// <returns>Status of the locking operation</returns>
        public static DataRetrievalResult<bool> AcquireLock<TDocument>(this IDataSession dataSession, TDocument document,
            OperationContext context)
        {
            return AcquireLock(dataSession, document, DefaultExpirySeconds, context);
        }

        /// <summary>
        /// Release an acquired lock
        /// </summary>
        /// <typeparam name="TDocument">Document type to unlock</typeparam>
        /// <param name="dataSession">Session that this lock applies to</param>
        /// <param name="document">Document to unlock</param>
        /// <returns></returns>
        public static DataResult ReleaseLock<TDocument>(this IDataSession dataSession, TDocument document)
        {
            var queryableSession = (IQueryableSession) dataSession;
            var handler = queryableSession.GetPatternHandlerForDocument<TDocument, LockingPatternHandler<TDocument>>();

            if (handler != null)
            {
                return LockingPatternHandler<TDocument>.ReleaseLock(queryableSession, document, handler.LockKeyModifier);
            }

            return LockingPatternHandler<TDocument>.ReleaseLock(queryableSession, document,
                x => string.Format(DefaultLockingKeyFormat, x));
        }
    }
}
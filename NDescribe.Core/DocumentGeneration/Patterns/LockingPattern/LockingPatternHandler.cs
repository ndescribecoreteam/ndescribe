﻿using System;
using System.Linq;
using NDescribe.Core.DataBridge;
using NDescribe.Core.DocumentGeneration.Traits;
using NDescribe.Core.Session;

namespace NDescribe.Core.DocumentGeneration.Patterns.LockingPattern
{
    internal class LockingPatternHandler<TDocument> : IPreStorageHandler, IUniquePattern
    {
        public const string DoNotLockKey = "LOCKING_PATTERN_DO_NOT_LOCK";

        private readonly long lockExpirySeconds;

        private readonly Func<string, string> lockKeyModifier;

        public Func<string, string> LockKeyModifier => lockKeyModifier;

        public LockingPatternHandler(Func<string, string> lockKeyModifier, long lockExpirySeconds)
        {
            this.lockKeyModifier = lockKeyModifier;
            this.lockExpirySeconds = lockExpirySeconds;
        }

        public PatternResult<StorageOptions> HandlePreStorage(StorageOptions storageOptions, DocumentDetails documentDetails, OperationContext context, IDataBridge dataBridge)
        {
            if (context.ContextualObjects.ContainsKey(DoNotLockKey))
            {
                return new PatternResult<StorageOptions>(storageOptions, true, string.Empty, null, context);
            }

            var lockKey = lockKeyModifier(storageOptions.Key);
            var dataLocation = documentDetails.DataLocation;
            var dataTraits = ((IInternalDefinedContext<TDocument>) documentDetails.DefinedContext).DataTraits.Union(context.ContextualTraits.OfType<IDataTrait>());

            var acquireLockResult = dataBridge.CreateLock(dataLocation, lockKey, lockExpirySeconds, dataTraits);

            if (!acquireLockResult.Success)
            {
                return new PatternResult<StorageOptions>(storageOptions, false, acquireLockResult.Message,
                    acquireLockResult.Exception, context);
            }

            if (!acquireLockResult.Document)
            {
                return new PatternResult<StorageOptions>(storageOptions, false,
                    "Failed to lock document, lock already acquired", null, context);
            }

            return new PatternResult<StorageOptions>(storageOptions, true, string.Empty, null,
                context);
        }

        /// <summary>
        /// Release a previously acquired lock
        /// </summary>
        /// <param name="session">Session to use</param>
        /// <param name="document">Document to unlock</param>
        /// <param name="lockKeyModifier">The modifier for the key when generating a locking document</param>
        /// <returns>The result of the unlock operation</returns>
        public static DataResult ReleaseLock(IQueryableSession session, TDocument document,
            Func<string, string> lockKeyModifier)
        {
            var documentDetails = session.GetDocumentDetailsForType<TDocument>();
            var internalDefinition = (IInternalDefinedContext<TDocument>) documentDetails.DefinedContext;
            var key = internalDefinition.GenerateFullKey(document, documentDetails, new OperationContext(), session.DataBridge, false);

            var lockKey = lockKeyModifier(key);
            return session.DataBridge.DeleteDocument(documentDetails.DataLocation, lockKey, internalDefinition.DataTraits);
        }

        /// <summary>
        /// Acquire a lock
        /// </summary>
        /// <param name="session">Session that this lock applies to</param>
        /// <param name="document">Document to lock</param>
        /// <param name="lockKeyModifier">Lock key modifier</param>
        /// <param name="lockExpiry">Lock expiry time in seconds</param>
        /// <param name="context">Current document context</param>
        /// <returns>Success of the locking operation</returns>
        public static DataRetrievalResult<bool> AcquireLock(IQueryableSession session, TDocument document,
            Func<string, string> lockKeyModifier, long lockExpiry, OperationContext context)
        {
            var documentDetails = session.GetDocumentDetailsForType<TDocument>();
            var internalDefinition = (IInternalDefinedContext<TDocument>)documentDetails.DefinedContext;
            var key = internalDefinition.GenerateFullKey(document, documentDetails, context, session.DataBridge, false);
            var lockKey = lockKeyModifier(key);

            return session.DataBridge.CreateLock(documentDetails.DataLocation, lockKey, lockExpiry, internalDefinition.DataTraits);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using NDescribe.Core.DataBridge;
using NDescribe.Core.DocumentGeneration.Traits;
using NDescribe.Core.Session;

namespace NDescribe.Core.DocumentGeneration.Patterns.CounterPattern
{
    internal class CounterPatternHandler<TDocument> : IKeyGenerationHandler<TDocument>,  IRangeHandler
    {
        private readonly MemberInfo counterValue;

        private readonly string metadataName;

        private readonly Func<string, string> counterKeyGenerator;

        private readonly Func<string, long, string> keyModifier;

        public CounterPatternHandler(MemberInfo counterValue, Func<string, string> counterKeyGenerator,
            Func<string, long, string> keyModifier)
        {
            metadataName = null;
            this.counterValue = counterValue;
            this.counterKeyGenerator = counterKeyGenerator;
            this.keyModifier = keyModifier;
        }

        public CounterPatternHandler(string metadataName, Func<string, string> counterKeyGenerator,
            Func<string, long, string> keyModifier)
        {
            this.metadataName = metadataName;
            counterValue = null;
            this.counterKeyGenerator = counterKeyGenerator;
            this.keyModifier = keyModifier;
        }

        public PatternResult<string> HandlePreReadKeyGeneration(string currentKey, TDocument templateDocument, DocumentDetails documentDetails,
            OperationContext context,
            IDataBridge dataBridge)
        {
            var dataLocation = documentDetails.DataLocation;
            var dataTraits = ((IInternalDefinedContext<TDocument>) documentDetails.DefinedContext).DataTraits.Union(context.ContextualTraits.OfType<IDataTrait>());

            

            return GenerateKey(currentKey, templateDocument, context,
                () => dataBridge.GetCounterValue(dataLocation, counterKeyGenerator(currentKey), dataTraits),
                "could not retrieve counter latest value");
        }

        internal static string GetMetaDataName(string metadataName)
        {
            if (metadataName == null)
            {
                return null;
            }

            return $"COUNTER_{metadataName}";
        }

        private PatternResult<string> GenerateKey(string currentKey, TDocument templateDocument, OperationContext context,
            Func<DataRetrievalResult<long>> counterOperation, string counterErrorMessage)
        {
            long value = 0;

            var generatedMetadataName = GetMetaDataName(metadataName);

            if (generatedMetadataName == null && counterValue == null)
            {
                throw new ArgumentException("Metadata name and counter value both null in counter handler, please review session setup.");
            }

            if (generatedMetadataName != null)
            {
                if (context.Metadata.ContainsKey(generatedMetadataName))
                {
                    value = (long) context.Metadata[generatedMetadataName];
                }
            }
            else
            {
                switch (counterValue.MemberType)
                {
                    case MemberTypes.Property:
                        value = (long) ((PropertyInfo) counterValue).GetValue(templateDocument);
                        break;
                    case MemberTypes.Field:
                        value = (long) ((FieldInfo) counterValue).GetValue(templateDocument);
                        break;
                }
            }

            if (value == 0)
            {
                var counterResult = counterOperation();
                if (!counterResult.Success)
                {
                    return new PatternResult<string>(currentKey, false,
                        $"CounterPatternHandler - {counterErrorMessage}: {counterResult.Message}",
                        counterResult.Exception, context);
                }

                value = counterResult.Document;

                if (counterValue != null)
                {
                    switch (counterValue.MemberType)
                    {
                        case MemberTypes.Property:
                            ((PropertyInfo) counterValue).SetValue(templateDocument, value);
                            break;
                        case MemberTypes.Field:
                            ((FieldInfo) counterValue).SetValue(templateDocument, value);
                            break;
                    }
                }
                else
                {
                    context.Metadata[generatedMetadataName] = value;
                }
            }

            string documentKey = keyModifier(currentKey, value);

            return new PatternResult<string>(documentKey, true, string.Empty, null, context);
        }

        public PatternResult<string> HandlePreStoreKeyGeneration(string currentKey, TDocument document, DocumentDetails documentDetails,
            OperationContext context,
            IDataBridge dataBridge)
        {
            var dataLocation = documentDetails.DataLocation;
            var dataTraits = ((IInternalDefinedContext<TDocument>) documentDetails.DefinedContext).DataTraits.Union(context.ContextualTraits.OfType<IDataTrait>());

            return GenerateKey(currentKey, document, context,
                () => dataBridge.IncrementCounter(dataLocation, counterKeyGenerator(currentKey), dataTraits),
                "could not increment counter");
        }

        public bool IncludeInQueries()
        {
            return true;
        }

        public PatternResult<IEnumerable<string>> HandleRangeRequest(string key, DocumentDetails documentDetails, OperationContext context, IDataBridge dataBridge)
        {
            var returnData = new List<string>();

            var dataLocation = documentDetails.DataLocation;
            var dataTraits = ((IInternalDefinedContext<TDocument>) documentDetails.DefinedContext).DataTraits.Union(context.ContextualTraits.OfType<IDataTrait>());

            var counterResult = dataBridge.GetCounterValue(dataLocation, counterKeyGenerator(key), dataTraits);
            if (!counterResult.Success)
            {
                return new PatternResult<IEnumerable<string>>(returnData, false,
                    $"CounterPatternHandler - could not retrieve counter latest value: {counterResult.Message}",
                    counterResult.Exception, context);
            }

            var currentCounterValue = counterResult.Document;
            for (long i = 1; i <= currentCounterValue; i++)
            {
                returnData.Add(keyModifier(key, i));
            }

            return new PatternResult<IEnumerable<string>>(returnData, true, string.Empty, null, context);
        }
    }
}
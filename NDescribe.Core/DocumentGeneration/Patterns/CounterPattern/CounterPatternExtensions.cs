﻿using System;
using System.Linq.Expressions;
using NDescribe.Core.DocumentGeneration.Patterns.ExpressionHelpers;
using NDescribe.Core.DataBridge;
using System.Collections.Generic;
using NDescribe.Core.Session;

namespace NDescribe.Core.DocumentGeneration.Patterns.CounterPattern
{
    /// <summary>
    /// Extension methods for the counter pattern
    /// </summary>
    public static class CounterPatternExtensions
    {
        private const string DefaultKeyFormat = "{0}::{1}";

        private const string DefaultCounterKeyFormat = "{0}::Counter";

        /// <summary>
        /// Add a counter to the document
        /// </summary>
        /// <typeparam name="TDocument">Type of document to add to</typeparam>
        /// <param name="configurationBuilder">The configuration builder currently in use</param>
        /// <param name="counterValue">Value to use as the counter</param>
        /// <param name="counterKeyGenerator">Key generator for the counter</param>
        /// <param name="keyModifier">Key modifier for the main key to add the counter value</param>
        /// <returns>The updated configuration builder</returns>
        public static IDocumentConfigurationBuilder<TDocument> WithCounter<TDocument>(
            this IDocumentConfigurationBuilder<TDocument> configurationBuilder,
            Expression<Func<TDocument, long>> counterValue, Func<string, string> counterKeyGenerator,
            Func<string, long, string> keyModifier)
        {
            var memberInfo = counterValue.GetMemberInfo();
            CounterPatternHandler<TDocument> handler = new CounterPatternHandler<TDocument>(memberInfo,
                counterKeyGenerator, keyModifier);
            configurationBuilder.AddHandler(handler);
            return configurationBuilder;
        }

        /// <summary>
        /// Add a counter to the document via metadata
        /// </summary>
        /// <typeparam name="TDocument">Type of document to add to</typeparam>
        /// <param name="configurationBuilder">The configuration builder currently in use</param>
        /// <param name="metadataName">Name to give the counter in metadata</param>
        /// <param name="counterKeyGenerator">Key generator for the counter</param>
        /// <param name="keyModifier">Key modifier for the main key to add the counter value</param>
        /// <returns>The updated configuration builder</returns>
        public static IDocumentConfigurationBuilder<TDocument> WithMetaCounter<TDocument>(
            this IDocumentConfigurationBuilder<TDocument> configurationBuilder,
            string metadataName, Func<string, string> counterKeyGenerator,
            Func<string, long, string> keyModifier)
        {
            CounterPatternHandler<TDocument> handler = new CounterPatternHandler<TDocument>(metadataName,
                counterKeyGenerator, keyModifier);
            configurationBuilder.AddHandler(handler);
            return configurationBuilder;
        }

        /// <summary>
        /// Add a counter to the document (with a default key modifier)
        /// </summary>
        /// <typeparam name="TDocument">Type of document to add to</typeparam>
        /// <param name="configurationBuilder">The configuration builder currently in use</param>
        /// <param name="counterValue">Value to use as the counter</param>
        /// <returns>The updated configuration builder</returns>
        public static IDocumentConfigurationBuilder<TDocument> WithCounter<TDocument>(
            this IDocumentConfigurationBuilder<TDocument> configurationBuilder,
            Expression<Func<TDocument, long>> counterValue)
        {
            return WithCounter(configurationBuilder, counterValue, x => string.Format(DefaultCounterKeyFormat, x),
                (x, y) => string.Format(DefaultKeyFormat, x, y));
        }

        /// <summary>
        /// Add a counter to the document (with a default key modifier)
        /// </summary>
        /// <typeparam name="TDocument">Type of document to add to</typeparam>
        /// <param name="configurationBuilder">The configuration builder currently in use</param>
        /// <param name="metadataName">Name to give the counter in metadata</param>
        /// <returns>The updated configuration builder</returns>
        public static IDocumentConfigurationBuilder<TDocument> WithMetaCounter<TDocument>(
            this IDocumentConfigurationBuilder<TDocument> configurationBuilder,
            string metadataName)
        {
            return WithMetaCounter(configurationBuilder, metadataName, x => string.Format(DefaultCounterKeyFormat, x),
                (x, y) => string.Format(DefaultKeyFormat, x, y));
        }

        /// <summary>
        /// Retrieve all documents in this counter range
        /// </summary>
        /// <typeparam name="TDocument">Document type to read</typeparam>
        /// <param name="dataSession">Data session to use</param>
        /// <param name="documentTemplate">The template document to setup any required data</param>
        /// <param name="includeDerivedTypes">Whether to include derived types when searching</param>
        /// <returns></returns>
        public static DataRetrievalResult<IDictionary<string, DataRetrievalResult<TDocument>>> GetAllCounterDocuments<TDocument>(
            this IDataSession dataSession, TDocument documentTemplate, bool includeDerivedTypes = false)
        {
            var queryableSession = (IQueryableSession) dataSession;
            return queryableSession.ReadRange<TDocument, CounterPatternHandler<TDocument>>(documentTemplate, includeDerivedTypes);
        }

        /// <summary>
        /// Add a meta counter to the operation context via a builder for usual operations
        /// </summary>
        /// <param name="operationContextBuilder">Builder to add teh metadata to</param>
        /// <param name="metadataName">Name of the metadata counter to add</param>
        /// <param name="counterValue">Value of the metadata counter to add</param>
        /// <returns>The operation context builder to continue building with</returns>
        public static OperationContextBuilder WithMetaCounter(this OperationContextBuilder operationContextBuilder, string metadataName, long counterValue)
        {
            return operationContextBuilder.WithMetaData(CounterPatternHandler<object>.GetMetaDataName(metadataName), counterValue);
        }
    }
}
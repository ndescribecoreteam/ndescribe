﻿using NDescribe.Core.DataBridge;
using NDescribe.Core.Session;

namespace NDescribe.Core.DocumentGeneration.Patterns
{
    /// <summary>
    /// Perform post-storage processing
    /// </summary>
    /// <typeparam name="TDocument">Type to process for</typeparam>
    public interface IPostStorageHandler<in TDocument> : IPatternHandler
    {
        /// <summary>
        /// Allows any post-storage processing
        /// </summary>
        /// <param name="documentKey">Key assigned to this document</param>
        /// <param name="document">Document to process</param>
        /// <param name="documentDetails">Details about this document type in the current session</param>
        /// <param name="context">Current context of the document being processed</param>
        /// <param name="dataBridge">Databridge to process data with if required</param>
        /// <param name="dataResult">Data result of the storage operation</param>
        /// 
        /// 
        /// <returns>A data result based on success or failure of an operation</returns>
        PatternResult<DataResult> HandlePostStore(string documentKey, IDocumentWrapper<TDocument> document, DocumentDetails documentDetails,
            OperationContext context, IDataBridge dataBridge, DataResult dataResult);
    }
}
﻿using NDescribe.Core.DataBridge;
using NDescribe.Core.Session;

namespace NDescribe.Core.DocumentGeneration.Patterns
{
    /// <summary>
    /// Handler for any actions required before deletion
    /// </summary>
    /// <typeparam name="TDocument">Document type to handle</typeparam>
    public interface IPostDeleteHandler<in TDocument>
    {
        /// <summary>
        /// Allows any processing before we delete a document
        /// </summary>
        /// <param name="documentKey">Key of the document to be deleted</param>
        /// <param name="documentTemplate">Template of the document to be deleted</param>
        /// <param name="documentDetails">Details of this document type including definition details</param>
        /// <param name="context">Context of the current document</param>
        /// <param name="dataBridge">Data bridge for storing data</param>
        /// <returns>A data result based on success or failure of an operation</returns>
        PatternResult<DataResult> HandlePostDelete(string documentKey, TDocument documentTemplate, DocumentDetails documentDetails,
            OperationContext context, IDataBridge dataBridge);

    }
}
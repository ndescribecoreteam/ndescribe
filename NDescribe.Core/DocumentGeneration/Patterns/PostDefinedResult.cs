﻿using System;

namespace NDescribe.Core.DocumentGeneration.Patterns
{
    /// <summary>
    /// Result of a pattern post defined operation
    /// </summary>
    public class PostDefinedResult
    {
        /// <summary>
        /// Result of a pattern post defined operation
        /// </summary>
        /// <param name="success">Success of the operation</param>
        /// <param name="message">Message returned by the operation</param>
        /// <param name="exception">Exception returned by the operation</param>
        public PostDefinedResult(bool success, string message, Exception exception)
        {
            Success = success;
            Message = message;
            Exception = exception;
        }

        /// <summary>
        /// Success of the operation
        /// </summary>
        public bool Success { get; private set; }

        /// <summary>
        /// Message returned by the operation
        /// </summary>
        public string Message { get; private set; }

        /// <summary>
        /// Exception returned by the operation
        /// </summary>
        public Exception Exception { get; private set; }
    }
}
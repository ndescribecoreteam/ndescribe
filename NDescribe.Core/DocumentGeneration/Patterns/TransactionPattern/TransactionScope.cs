﻿using System;
using System.Collections.Generic;
using System.Linq;
using NDescribe.Core.DataBridge;

namespace NDescribe.Core.DocumentGeneration.Patterns.TransactionPattern
{
    /// <summary>
    /// Controls the scope of a transaction
    /// </summary>
    public class TransactionScope : IDisposable
    {
        [ThreadStatic] private static Transaction transaction;

        /// <summary>
        /// Holds the current transaction object as an internal property
        /// </summary>
        internal static Transaction Transaction => transaction;

        /// <summary>
        /// Create a new TransactionScope
        /// </summary>
        /// <param name="transactionConfiguration">Configuration for this particular transaction</param>
        /// <exception cref="NotSupportedException">Thrown if the transaction is in a nested state</exception>
        internal TransactionScope(TransactionConfiguration transactionConfiguration)
        {
            if (transaction != null)
            {
                throw new NotSupportedException("Cannot nest transactions");
            }

            transaction = new Transaction
            {
                TransactionHandlers = new List<ITransactionHandler>(),
                TransactionConfiguration = transactionConfiguration
            };
        }

        /// <summary>
        /// Cancel the current transaction explicitly
        /// </summary>
        internal void CancelTransaction()
        {
            if (transaction == null)
            {
                throw new NotSupportedException("No transaction for cancellation");
            }

            transaction.Cancelled = true;
        }

        /// <summary>
        /// Dispose this transaction
        /// </summary>
        /// <exception cref="TransactionException">Thrown when a transaction fails directly</exception>
        /// <exception cref="NDescribeException">Thrown if a transaction fails for an internal reason</exception>
        public void Dispose()
        {
            if (transaction.TransactionHandlers.Any())
            {
                DataResult dataResult;
                string operationText;

                if (transaction.Cancelled)
                {
                    operationText = "cancelling";

                    dataResult = transaction.TransactionHandlers[0].CancelTransaction(transaction.DataLocation,
                        transaction.DataBridge);
                }
                else
                {
                    operationText = "committing";

                    dataResult = transaction.TransactionHandlers[0].CommitTransaction(transaction.DataLocation,
                        transaction.DataBridge, transaction.TransactionId);
                }

                if (!dataResult.Success)
                {
                    throw new TransactionException(
                        $"Error {operationText} transaction: {dataResult.Message}",
                        dataResult.Exception);
                }

                foreach (var handler in transaction.TransactionHandlers)
                {
                    var unlockResult = handler.UnlockTransactions();

                    if (!unlockResult.Success)
                    {
                        throw new NDescribeException(unlockResult.Message,
                            unlockResult.Exception);
                    }
                }
            }

            transaction = null;
        }
    }
}
﻿using NDescribe.Core.DataBridge;

namespace NDescribe.Core.DocumentGeneration.Patterns.TransactionPattern
{
    internal interface ITransactionHandler
    {
        DataResult CommitTransaction(string dataLocation, IDataBridge dataBridge, long id);

        DataResult CancelTransaction(string dataLocation, IDataBridge dataBridge);

        DataResult UnlockTransactions();
    }
}
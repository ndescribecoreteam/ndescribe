﻿using System.Collections.Generic;

namespace NDescribe.Core.DocumentGeneration.Patterns.TransactionPattern
{
    internal class TransactionDocumentWrapper<TDocument> : IDocumentWrapper<TDocument>
    {
        public TransactionDocumentWrapper(string transactionKey, IDocumentWrapper<TDocument> documentWrapper)
        {
            TransactionKey = transactionKey;
            DocumentWrapper = documentWrapper;
        }

        public string TransactionKey { get; private set; }

        public TDocument Document => DocumentWrapper.Document;

        public IDictionary<string, object> Metadata => DocumentWrapper.Metadata;

        public IDocumentWrapper<TDocument> DocumentWrapper { get; }

        public string Version => DocumentWrapper.Version;
    }
}
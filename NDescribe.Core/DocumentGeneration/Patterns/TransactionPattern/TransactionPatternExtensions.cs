﻿namespace NDescribe.Core.DocumentGeneration.Patterns.TransactionPattern
{
    /// <summary>
    /// Extensions for managing the transaction pattern
    /// </summary>
    public static class TransactionPatternExtensions
    {
        /// <summary>
        /// This document should be used in transactions
        /// </summary>
        /// <typeparam name="TDocument">Document type to mark</typeparam>
        /// <param name="configurationBuilder">Configuration builder to add the transactional handler to</param>
        /// <returns>The configuration builder</returns>
        public static IDocumentConfigurationBuilder<TDocument> ForTransactions<TDocument>(this IDocumentConfigurationBuilder<TDocument> configurationBuilder)
        {
            var handler = new TransactionPatternHandler<TDocument>();
            configurationBuilder.AddHandler(handler);
            return configurationBuilder;
        }

        /// <summary>
        /// Start a transaction
        /// </summary>
        /// <param name="transactionConfiguration">The transaction configuration to use for this transaction</param>
        /// <returns>Transaction scope to manage the transaction scope with</returns>
        public static TransactionScope StartTransaction(this TransactionConfiguration transactionConfiguration)
        {
            return new TransactionScope(transactionConfiguration);
        }

        /// <summary>
        /// Cancel a transaction to prevent it committing
        /// </summary>
        /// <param name="transactionScope">Transaction scope to cancel the transaction for</param>
        public static void CancelTransaction(this TransactionScope transactionScope)
        {
            transactionScope.CancelTransaction();
        }
    }
}
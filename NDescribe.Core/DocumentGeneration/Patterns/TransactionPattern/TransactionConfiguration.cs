﻿using System;

namespace NDescribe.Core.DocumentGeneration.Patterns.TransactionPattern
{
    /// <summary>
    /// Contains the configuration required for starting a transaction
    /// </summary>
    public class TransactionConfiguration
    {
        /// <summary>
        /// Create configuration for starting a transaction
        /// </summary>
        /// <param name="transactionCounterKey">The counter key to use for this type of transaction</param>
        /// <param name="transactionDocumentKeyBuilder">The factory function for creating a key for the transaction based on the coutner value</param>
        public TransactionConfiguration(string transactionCounterKey, Func<long, string> transactionDocumentKeyBuilder)
        {
            TransactionCounterKey = transactionCounterKey;
            TransactionDocumentKeyBuilder = transactionDocumentKeyBuilder;
        }

        /// <summary>
        /// The counter key to use for this type of transaction
        /// </summary>
        public string TransactionCounterKey { get; }

        /// <summary>
        /// The factory function for creating a key for the transaction based ont he counter value
        /// </summary>
        public Func<long, string> TransactionDocumentKeyBuilder { get; }
    }
}
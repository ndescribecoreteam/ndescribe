﻿namespace NDescribe.Core.DocumentGeneration.Patterns.TransactionPattern
{
    /// <summary>
    /// Represents a transaction object
    /// </summary>
    public class TransactionDocument
    {
        /// <summary>
        /// Id of the transaction
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Whether the transaction is confirmed or not
        /// </summary>
        public bool Confirmed { get; set; }
    }
}
﻿using System;

namespace NDescribe.Core.DocumentGeneration.Patterns.TransactionPattern
{
    /// <summary>
    /// An exception thrown as a result of an error processing a transaction
    /// </summary>
    public class TransactionException : Exception
    {
        /// <summary>
        /// Create an exception thrown as a result of an error processing a transaction
        /// </summary>
        /// <param name="message">Reason for the failure</param>
        /// <param name="innerException">The exception that triggered this exception</param>
        public TransactionException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
﻿using System.Collections.Generic;
using System.Linq;
using NDescribe.Core.DataBridge;
using NDescribe.Core.DocumentGeneration.Patterns.VersionPattern;
using NDescribe.Core.Serialization;
using NDescribe.Core.Session;
using NDescribe.Core.DocumentGeneration.Patterns.LockingPattern;
using NDescribe.Core.DocumentGeneration.Traits;

namespace NDescribe.Core.DocumentGeneration.Patterns.TransactionPattern
{
    internal class TransactionPatternHandler<TDocument> : IPostDefinedHandler, IPreSerializationHandler<TDocument>,
        IPostDeserializationHandler<TDocument>, ITransactionHandler
    {
        private const string ProcessingContextMarkerKey = "TransactionHandlerProcessingPostRead";

        private const string ProcessingPreContextMarkerKey = "TransactionHandlerProcessingPreStorage";

        private ISerializationProvider serializationProvider;

        private IDataSession session;

        private readonly List<TDocument> lockedTransactions = new List<TDocument>();

        private DataResult WriteTransaction(string dataLocation, IDataBridge dataBridge, long id = 0,
            bool commit = false)
        {
            if (id == 0)
            {
                var transactionCounterResult = dataBridge.IncrementCounter(dataLocation,
                    TransactionScope.Transaction.TransactionConfiguration.TransactionCounterKey, new List<IDataTrait>());
                if (!transactionCounterResult.Success)
                {
                    return new DataResult(false,
                        $"Failed to create transaction counter: {transactionCounterResult.Message}",
                        transactionCounterResult.Exception);
                }

                id = transactionCounterResult.Document;
            }

            var transactionDocument = new TransactionDocument() {Id = id, Confirmed = commit};
            var serializationResult = serializationProvider.Serialize(transactionDocument);

            var transactionKey =
                TransactionScope.Transaction.TransactionConfiguration.TransactionDocumentKeyBuilder(
                    id);

            TransactionScope.Transaction.TransactionId = id;

            var storeResult = dataBridge.SaveDocument(dataLocation, transactionKey, serializationResult,
                new List<IDataTrait>());

            if (!storeResult.Success)
            {
                return new DataResult(false,
                    $"Error storing transaction document: {storeResult.Message}", storeResult.Exception);
            }

            return new DataResult(true, string.Empty, null);
        }

        private void QueryableSessionOnDataWriteOperationFailedEvent(object sender, DataWriteOperationFailedEventArgs dataWriteOperationFailedEventArgs)
        {
            if (TransactionScope.Transaction != null && TransactionScope.Transaction.TransactionInitiated)
            {
                TransactionScope.Transaction.Cancelled = true;
            }
        }

        public DataResult CommitTransaction(string dataLocation, IDataBridge dataBridge, long id)
        {
            return WriteTransaction(dataLocation, dataBridge, id, true);
        }

        public DataResult CancelTransaction(string dataLocation, IDataBridge dataBridge)
        {
            return dataBridge.DeleteDocument(dataLocation, GetCurrentTransactionKey(), new List<IDataTrait>());
        }

        private static string GetCurrentTransactionKey()
        {
            return TransactionScope.Transaction.TransactionConfiguration.TransactionDocumentKeyBuilder(TransactionScope.Transaction.TransactionId);
        }

        public PatternResult<IDocumentWrapper<TDocument>> HandlePreSerialization(IDocumentWrapper<TDocument> document,
            DocumentDetails documentDetails, string basicKey, OperationContext context, IDataBridge dataBridge)
        {
            if (TransactionScope.Transaction == null ||
                context.ContextualObjects.ContainsKey(ProcessingPreContextMarkerKey))
            {
                return new PatternResult<IDocumentWrapper<TDocument>>(document, true, string.Empty,
                    null, context);
            }

            var lockResult = session.AcquireLock(document.Document,
                new OperationContext
                {
                    ContextualObjects =
                    {{ProcessingPreContextMarkerKey, true}}
                });

            if (!lockResult.Success)
            {
                return new PatternResult<IDocumentWrapper<TDocument>>(document, false,
                    $"Error acquiring lock: {lockResult.Message}",
                    null, context);
            }

            if (!lockResult.Document)
            {
                return new PatternResult<IDocumentWrapper<TDocument>>(document, false,
                    "Could not acquire lock - document already locked", null, context);
            }

            lockedTransactions.Add(document.Document);

            if (!TransactionScope.Transaction.TransactionInitiated)
            {
                var dataLocation = documentDetails.DataLocation;

                var creationResult = WriteTransaction(dataLocation, dataBridge);
                if (!creationResult.Success)
                {
                    return new PatternResult<IDocumentWrapper<TDocument>>(document, false,
                        $"Could not create transaction: {creationResult.Message}", creationResult.Exception,
                        context);
                }

                TransactionScope.Transaction.TransactionInitiated = true;
                TransactionScope.Transaction.DataBridge = dataBridge;
                TransactionScope.Transaction.DataLocation = dataLocation;
            }

            if (!TransactionScope.Transaction.TransactionHandlers.Contains(this))
            {
                TransactionScope.Transaction.TransactionHandlers.Add(this);
            }

            var transactionDocumentWrapper = new TransactionDocumentWrapper<TDocument>(GetCurrentTransactionKey(), document);
            return new PatternResult<IDocumentWrapper<TDocument>>(transactionDocumentWrapper, true, string.Empty,
                null, context);
        }

        public PostDefinedResult HandlePostDefined(IDataSession dataSession, DocumentDetails documentDetails)
        {
            var queryableSession = (IQueryableSession) dataSession;
            serializationProvider = queryableSession.SerializationProvider;
            session = dataSession;

            var internalDefinition = (IInternalDefinedContext<TDocument>) documentDetails.DefinedContext;
            internalDefinition.AddVersioning();
            queryableSession.DataWriteOperationFailedEvent += QueryableSessionOnDataWriteOperationFailedEvent;
            return new PostDefinedResult(true, string.Empty, null);
        }

        public PatternResult<IDocumentWrapper<TDocument>> HandlePostDeserialization(
            IDocumentWrapper<TDocument> document, DocumentDetails documentDetails, OperationContext context,
            IDataBridge dataBridge)
        {
            var internalDefinedContext = (IInternalDefinedContext<TDocument>) documentDetails.DefinedContext;
            var traitsList = internalDefinedContext.DataTraits.Union(context.ContextualTraits.OfType<IDataTrait>()).ToList();
            var dataLocation = documentDetails.DataLocation;

            if (context.ContextualObjects.ContainsKey(ProcessingContextMarkerKey) ||
                !(document is TransactionDocumentWrapper<TDocument>))
            {
                return new PatternResult<IDocumentWrapper<TDocument>>(document, true, string.Empty,
                    null, context);
            }

            var transactionDocumentWrapper = (TransactionDocumentWrapper<TDocument>) document;

            var existsCheck = dataBridge.KeyExists(dataLocation, transactionDocumentWrapper.TransactionKey, traitsList);

            if (!existsCheck.Success)
            {
                return new PatternResult<IDocumentWrapper<TDocument>>(document, false,
                    $"Failed on existence check for transaction document: {existsCheck.Message}", existsCheck.Exception,
                    context);
            }

            TransactionDocument transactionDocument = null;

            if (existsCheck.Document)
            {
                var retrieveResult = dataBridge.GetDocument(dataLocation, transactionDocumentWrapper.TransactionKey, traitsList);

                if (!retrieveResult.Success)
                {
                    return new PatternResult<IDocumentWrapper<TDocument>>(document, false,
                        $"Error retrieving transaction document: {retrieveResult.Message}", retrieveResult.Exception,
                        context);
                }

                transactionDocument =
                    serializationProvider.Deserialize<TransactionDocument>(retrieveResult.Document);

                if (transactionDocument == null)
                {
                    return new PatternResult<IDocumentWrapper<TDocument>>(document, false,
                        $"Failed to deserialize transaction document: {retrieveResult.Document}", null, context);
                }
            }

            if (transactionDocument != null && existsCheck.Document && transactionDocument.Confirmed)
            {
                return new PatternResult<IDocumentWrapper<TDocument>>(document, true, string.Empty,
                    null, context);
            }

            long latestVersion;

            if (context.ContextualObjects.ContainsKey(VersionPatternHandler<TDocument>.DocumentContextVersionKey))
            {
                latestVersion =
                    (long) context.ContextualObjects[VersionPatternHandler<TDocument>.DocumentContextVersionKey];
            }
            else
            {
                var latestVersionResult = session.ReadLatestVersion(document.Document);
                if (!latestVersionResult.Success)
                {
                    return new PatternResult<IDocumentWrapper<TDocument>>(document, false,
                        $"Error getting latest version: {latestVersionResult.Message}", latestVersionResult.Exception,
                        context);
                }

                latestVersion = latestVersionResult.Document;
            }

            latestVersion--;

            if (latestVersion == 0)
            {
                return new PatternResult<IDocumentWrapper<TDocument>>(document, false,
                    "Could not find any versions with a committed transaction", null,
                    context);
            }

            var getVersionResult = session.ReadWithVersion(document.Document, latestVersion);

            if (!getVersionResult.Success)
            {
                return new PatternResult<IDocumentWrapper<TDocument>>(document, false, getVersionResult.Message,
                    getVersionResult.Exception, context);
            }

            return
                new PatternResult<IDocumentWrapper<TDocument>>(
                    new DocumentWrapper<TDocument>(document.Version, getVersionResult.Document, context.Metadata), true,
                    string.Empty, null, context);
        }

        /// <summary>
        /// Unlock all locked documents post-transaction
        /// </summary>
        /// <returns>Success of unlocks</returns>
        public DataResult UnlockTransactions()
        {
            foreach (var transaction in lockedTransactions)
            {
                var releaseResult = session.ReleaseLock(transaction);
                if (!releaseResult.Success)
                {
                    return new DataResult(false,
                        $"Error unlocking transactions: {releaseResult.Message}", releaseResult.Exception);
                }
            }

            lockedTransactions.Clear();

            return new DataResult(true, string.Empty, null);
        }
    }
}
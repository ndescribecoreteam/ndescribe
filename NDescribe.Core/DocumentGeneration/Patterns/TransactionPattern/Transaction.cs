﻿using NDescribe.Core.DataBridge;
using System.Collections.Generic;

namespace NDescribe.Core.DocumentGeneration.Patterns.TransactionPattern
{
    internal class Transaction
    {
        public bool TransactionInitiated { get; set; }

        public long TransactionId { get; set; }

        public IList<ITransactionHandler> TransactionHandlers { get; set; }

        public string DataLocation { get; set; }

        public IDataBridge DataBridge { get; set; }

        public TransactionConfiguration TransactionConfiguration { get; set; }

        public bool Cancelled { get; set; }
    }
}
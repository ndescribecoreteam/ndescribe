﻿using NDescribe.Core.DataBridge;
using NDescribe.Core.Session;

namespace NDescribe.Core.DocumentGeneration.Patterns
{
    /// <summary>
    /// Pattern handler for processing before the read takes place
    /// </summary>
    public interface IPreReadHandler : IPatternHandler
    {
        /// <summary>
        /// Process the step before reading takes place
        /// </summary>
        /// <param name="readOptions">Current options for reading</param>
        /// <param name="context">Current context for reading the document</param>
        /// <param name="documentDetails">Details of this document type including definition details</param>
        /// <param name="dataBridge">Databridge for doing data operations</param>
        /// <returns>The document to read</returns>
        PatternResult<ReadOptions> HandlePreRead(ReadOptions readOptions,
            OperationContext context, DocumentDetails documentDetails,
            IDataBridge dataBridge);
    }
}
﻿using NDescribe.Core.DataBridge;
using NDescribe.Core.Session;

namespace NDescribe.Core.DocumentGeneration.Patterns
{
    /// <summary>
    /// A pattern handler for taking part in key generation
    /// </summary>
    /// <typeparam name="TDocument">Document type that this pattern handler applies to</typeparam>
    public interface IKeyGenerationHandler<in TDocument> : IPatternHandler
    {
        /// <summary>
        /// Intercept key generation before reading
        /// </summary>
        /// <param name="currentKey">The currently generated key</param>
        /// <param name="templateDocument">Template document to use for key generation</param>
        /// <param name="documentDetails">Details about the current document and its definition</param>
        /// <param name="context">The current document context for this operation</param>
        /// <param name="dataBridge">Databridge to use for data interaction</param>
        /// 
        /// 
        /// <returns>
        /// The result of the operation including the modified key.
        /// If the key is to be unchanged, the value should be the passed in key, and not an empty or null value.
        /// </returns>
        PatternResult<string> HandlePreReadKeyGeneration(string currentKey, TDocument templateDocument,
            DocumentDetails documentDetails, OperationContext context, IDataBridge dataBridge);

        /// <summary>
        /// Intercept key generation before document storage and before prestorage
        /// </summary>
        /// <param name="currentKey">The currently generated key</param>
        /// <param name="document">Template document to use for key generation</param>
        /// <param name="documentDetails">Details about the current document and its definition</param>
        /// <param name="context">The current document context for this operation</param>
        /// <param name="dataBridge">Databridge to use for data interaction</param>
        /// 
        /// 
        /// <returns>
        /// The result of the operation including the modified key.
        /// If the key is to be unchanged, the value should be the passed in key, and not an empty or null value.
        /// </returns>
        PatternResult<string> HandlePreStoreKeyGeneration(string currentKey, TDocument document, DocumentDetails documentDetails,
            OperationContext context, IDataBridge dataBridge);

        /// <summary>
        /// Whether to include this pattern handler in key generation for simple non-data access queries
        /// </summary>
        /// <returns>Whether to include this in queries or only data-access calls</returns>
        bool IncludeInQueries();
    }
}
﻿namespace NDescribe.Core.DocumentGeneration.Patterns
{
    /// <summary>
    /// Stores options used for reading
    /// </summary>
    public class ReadOptions
    {
        /// <summary>
        /// Create a new set of read options
        /// </summary>
        /// <param name="key">Key of the document to read</param>
        public ReadOptions(string key)
        {
            Key = key;
        }

        /// <summary>
        /// Key of the document to read
        /// </summary>
        public string Key { get; }
    }
}
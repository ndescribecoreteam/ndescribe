﻿namespace NDescribe.Core.DocumentGeneration.Patterns
{
    /// <summary>
    /// Options for storing the document
    /// </summary>
    public class StorageOptions
    {
        /// <summary>
        /// Create a new set of storage options
        /// </summary>
        /// <param name="key">The key to use for storing the document</param>
        /// <param name="serializedDocument">The serialized document</param>
        public StorageOptions(string key, string serializedDocument)
        {
            Key = key;
            SerializedDocument = serializedDocument;
        }

        /// <summary>
        /// The key to use when storing the document
        /// </summary>
        public string Key { get; }

        /// <summary>
        /// The serialized document
        /// </summary>
        public string SerializedDocument { get; }
    }
}
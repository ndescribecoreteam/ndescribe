﻿namespace NDescribe.Core.DocumentGeneration.Patterns
{
    /// <summary>
    /// Only a single instance of this pattern should be loaded for any document
    /// </summary>
    public interface IUniquePattern
    {
    }
}
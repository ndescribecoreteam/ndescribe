﻿using NDescribe.Core.DataBridge;
using System.Collections.Generic;
using NDescribe.Core.Session;

namespace NDescribe.Core.DocumentGeneration.Patterns
{
    /// <summary>
    /// Process a range request
    /// </summary>
    public interface IRangeHandler : IUniquePattern, IPatternHandler
    {
        /// <summary>
        /// Process a range request for a range of keys
        /// </summary>
        /// <param name="key">Basic generated key for this document</param>
        /// <param name="documentDetails">Details about this document including definition details</param>
        /// <param name="context">The current context for this document</param>
        /// <param name="dataBridge">The data bridge for this document</param>
        /// <returns>A list of keys</returns>
        PatternResult<IEnumerable<string>> HandleRangeRequest(string key, DocumentDetails documentDetails,
            OperationContext context, IDataBridge dataBridge);
    }
}
﻿using NDescribe.Core.DataBridge;
using NDescribe.Core.Session;

namespace NDescribe.Core.DocumentGeneration.Patterns
{
    /// <summary>
    /// Processes documents immediately before they are stored
    /// </summary>
    public interface IPreStorageHandler : IPatternHandler
    {
        /// <summary>
        /// Process a document just before it is stored
        /// </summary>
        /// <param name="storageOptions">Currently set storage options</param>
        /// <param name="documentDetails">Details of this document type including definition details</param>
        /// <param name="context">Context of the current document</param>
        /// <param name="dataBridge">Data bridge for storing data</param>
        /// <returns>Result of the pre-storage operation and any modified storage options</returns>
        PatternResult<StorageOptions> HandlePreStorage(StorageOptions storageOptions, DocumentDetails documentDetails,
            OperationContext context, IDataBridge dataBridge);
    }
}
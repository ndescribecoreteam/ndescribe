﻿using System.Collections.Generic;
using NDescribe.Core.DataBridge;
using NDescribe.Core.DocumentGeneration.Traits;

namespace NDescribe.Core.DocumentGeneration.Patterns.LookupPattern
{
    internal interface ISimpleLookupHandler<in TDocument>
    {
        DataRetrievalResult<string> GetDocumentKeyFromLookupValue(TDocument templateDocument, string dataLocation,
            IDataBridge dataBridge, IEnumerable<IDataTrait> dataTraits);
    }
}
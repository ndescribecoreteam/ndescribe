﻿using NDescribe.Core.DataBridge;
using NDescribe.Core.DocumentGeneration.Patterns.ExpressionHelpers;
using System;
using System.Linq.Expressions;
using NDescribe.Core.Session;

namespace NDescribe.Core.DocumentGeneration.Patterns.LookupPattern
{
    /// <summary>
    /// Extensions for managing the lookup pattern
    /// </summary>
    public static class LookupPatternExtensions
    {
        /// <summary>
        /// Add a lookup to the configuration builder
        /// </summary>
        /// <typeparam name="TDocument">Document type for this lookup</typeparam>
        /// <typeparam name="TLookup">Lookup value type</typeparam>
        /// <param name="configurationBuilder">Current configuration builder</param>
        /// <param name="lookupValue">Value to use for the lookup</param>
        /// <param name="lookupKeyBuilder">Key builder for the lookup</param>
        /// <returns>The updated configuration builder</returns>
        public static IDocumentConfigurationBuilder<TDocument> WithLookup<TDocument, TLookup>(
            this IDocumentConfigurationBuilder<TDocument> configurationBuilder,
            Expression<Func<TDocument, TLookup>> lookupValue, Func<TLookup, string> lookupKeyBuilder)
        {
            var memberInfo = lookupValue.GetMemberInfo();
            var lookupPatternHandler = new LookupPatternHandler<TDocument, TLookup>(memberInfo, lookupKeyBuilder);
            configurationBuilder.AddHandler(lookupPatternHandler);
            return configurationBuilder;
        }

        /// <summary>
        /// Retrieve a document by providing only lookup values
        /// </summary>
        /// <typeparam name="TDocument">Document type to read</typeparam>
        /// <param name="dataSession">The data session we are calling from</param>
        /// <param name="templateDocument">A template document with the appropriate lookup value filled in</param>
        /// <returns>The found document</returns>
        public static DataRetrievalResult<TDocument> ReadFromLookup<TDocument>(this IDataSession dataSession,
            TDocument templateDocument)
        {
            var queryableSession = (IQueryableSession) dataSession;
            var patternHandler =
                queryableSession.GetPatternHandlerForDocument<TDocument, ISimpleLookupHandler<TDocument>>();
            var documentDetails = queryableSession.GetDocumentDetailsForType<TDocument>();
            var lookupKeyResult = patternHandler.GetDocumentKeyFromLookupValue(templateDocument,
                documentDetails.DataLocation, queryableSession.DataBridge, ((IInternalDefinedContext<TDocument>)documentDetails.DefinedContext).DataTraits);

            if (!lookupKeyResult.Success)
            {
                return new DataRetrievalResult<TDocument>(default(TDocument), false,
                    $"Error retrieving document key from lookup value: {lookupKeyResult.Message}",
                    lookupKeyResult.Exception);
            }

            return dataSession.Read<TDocument>(lookupKeyResult.Document);
        }
    }
}
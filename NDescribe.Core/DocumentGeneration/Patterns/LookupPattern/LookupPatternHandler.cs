﻿using System;
using NDescribe.Core.DataBridge;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using NDescribe.Core.DocumentGeneration.Traits;
using NDescribe.Core.Session;

namespace NDescribe.Core.DocumentGeneration.Patterns.LookupPattern
{
    internal class LookupPatternHandler<TDocument, TLookup> : IPostStorageHandler<TDocument>,
        IPreSerializationHandler<TDocument>, IPostDeleteHandler<TDocument>, ISimpleLookupHandler<TDocument>
    {
        private const string LookupValueMemberContexFormat = "LOOKUP_{0}_OLD_KEY";

        private readonly MemberInfo lookupValue;

        private readonly Func<TLookup, string> lookupKeyBuilder;

        public LookupPatternHandler(MemberInfo lookupValue, Func<TLookup, string> lookupKeyBuilder)
        {
            this.lookupValue = lookupValue;
            this.lookupKeyBuilder = lookupKeyBuilder;
        }

        public PatternResult<DataResult> HandlePostStore(string documentKey, IDocumentWrapper<TDocument> document, DocumentDetails documentDetails,
            OperationContext context, IDataBridge dataBridge, DataResult dataResult)
        {
            var internalDefinedContext = (IInternalDefinedContext<TDocument>) documentDetails.DefinedContext;
            var traitsList = internalDefinedContext.DataTraits.Union(context.ContextualTraits.OfType<IDataTrait>()).ToList();
            var dataLocation = documentDetails.DataLocation;

            object currentValue = GetLookupValue(document.Document);
            string currentKey = lookupKeyBuilder((TLookup) currentValue);

            string documentContextKey = GetLookupStorageKey();

            if (context.ContextualObjects.ContainsKey(documentContextKey))
            {
                string oldKey = context.ContextualObjects[documentContextKey] as string;

                var existsCheck = dataBridge.KeyExists(dataLocation, oldKey, traitsList);
                if (!existsCheck.Success)
                {
                    return new PatternResult<DataResult>(dataResult, false,
                        $"Failed to check if previous lookup exists: {existsCheck.Message}",
                        existsCheck.Exception, context);
                }

                if (existsCheck.Document)
                {
                    var result = dataBridge.DeleteDocument(dataLocation, oldKey, traitsList);
                    if (!result.Success)
                    {
                        return new PatternResult<DataResult>(dataResult, false,
                            $"Failed to delete previous lookup: {result.Message}",
                            result.Exception, context);
                    }
                }
            }

            var storeResult = dataBridge.SaveDocument(dataLocation, currentKey, documentKey, traitsList);

            if (!storeResult.Success)
            {
                return new PatternResult<DataResult>(dataResult, false,
                    $"LookupPatternHandler: Error storing lookup: {storeResult.Message}",
                    storeResult.Exception, context);
            }

            context.ContextualObjects[documentContextKey] = currentKey;

            return new PatternResult<DataResult>(dataResult, true, string.Empty, null, context);
        }

        private object GetLookupValue(TDocument document)
        {
            object value = null;

            if (lookupValue.MemberType == MemberTypes.Property)
            {
                value = ((PropertyInfo) lookupValue).GetValue(document);
            }
            else if (lookupValue.MemberType == MemberTypes.Field)
            {
                value = ((FieldInfo) lookupValue).GetValue(document);
            }

            return value;
        }

        private string GetLookupStorageKey()
        {
            return string.Format(LookupValueMemberContexFormat, lookupValue.Name);
        }

        public PatternResult<IDocumentWrapper<TDocument>> HandlePreSerialization(IDocumentWrapper<TDocument> document, DocumentDetails documentDetails, string basicKey, OperationContext context, IDataBridge dataBridge)
        {
            var currentValue = GetLookupValue(document.Document);
            var lookupKey = lookupKeyBuilder((TLookup) currentValue);
            context.ContextualObjects[GetLookupStorageKey()] = lookupKey;
            return new PatternResult<IDocumentWrapper<TDocument>>(document, true, string.Empty,
                null, context);
        }

        public DataRetrievalResult<string> GetDocumentKeyFromLookupValue(TDocument templateDocument, string dataLocation,
            IDataBridge dataBridge, IEnumerable<IDataTrait> dataTraits)
        {
            var value = GetLookupValue(templateDocument);
            var lookupKey = lookupKeyBuilder((TLookup) value);
            return dataBridge.GetDocument(dataLocation, lookupKey, dataTraits);
        }

        public PatternResult<DataResult> HandlePostDelete(string documentKey, TDocument documentTemplate, DocumentDetails documentDetails,
            OperationContext context, IDataBridge dataBridge)
        {
            var value = GetLookupValue(documentTemplate);
            var lookupKey = lookupKeyBuilder((TLookup) value);
            var deleteResult = dataBridge.DeleteDocument(documentDetails.DataLocation, lookupKey, context.ContextualTraits.OfType<IDataTrait>());
            return new PatternResult<DataResult>(deleteResult, true, string.Empty, null, context);
        }
    }
}
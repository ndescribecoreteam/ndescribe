﻿using NDescribe.Core.DataBridge;
using NDescribe.Core.Session;

namespace NDescribe.Core.DocumentGeneration.Patterns
{
    /// <summary>
    /// Perform pre-serialization processing
    /// </summary>
    /// <typeparam name="TDocument">Type of domain object to process</typeparam>
    public interface IPreSerializationHandler<TDocument> : IPatternHandler
    {
        /// <summary>
        /// Process a document before serialization
        /// </summary>
        /// <param name="document">Document to process</param>
        /// <param name="documentDetails">Details of this document type including definition details</param>
        /// <param name="basicKey">The basic key before any potential modification</param>
        /// <param name="context">Current context of the document being processed</param>
        /// <param name="dataBridge">Data bridge to use for processing if required</param>
        /// <returns>The result of the operation, including a modified document if required</returns>
        PatternResult<IDocumentWrapper<TDocument>> HandlePreSerialization(IDocumentWrapper<TDocument> document, DocumentDetails documentDetails,
            string basicKey, OperationContext context,
            IDataBridge dataBridge);
    }
}
﻿using System;
using System.Linq.Expressions;
using System.Reflection;

namespace NDescribe.Core.DocumentGeneration.Patterns.ExpressionHelpers
{
    /// <summary>
    /// A helper extension for expression objects
    /// </summary>
    public static class ExpressionExtensions
    {
        /// <summary>
        /// Get a member object from an expression
        /// </summary>
        /// <typeparam name="T">Type of expression</typeparam>
        /// <param name="expression">The epxression to analyze</param>
        /// <returns>The specified member</returns>
        public static MemberInfo GetMemberInfo<T>(this Expression<T> expression)
        {
            var memberBody = expression.Body as MemberExpression;
            if (memberBody == null)
            {
                throw new ArgumentException("Counter value is not a valid member definition.");
            }

            return memberBody.Member;
        }
    }
}
﻿using System;

namespace NDescribe.Core.DocumentGeneration.Patterns
{
    /// <summary>
    /// Result of a pattern operation which requires the return of an item
    /// </summary>
    /// <typeparam name="T">Item to return</typeparam>
    public class PatternResult<T>
    {
        /// <summary>
        /// Result of a pattern operation which requires the return of an item
        /// </summary>
        /// <param name="item">Item to return</param>
        /// <param name="success">Success of the operation</param>
        /// <param name="message">Message returned by the operation</param>
        /// <param name="exception">Exception returned by the operation</param>
        /// <param name="context">Updated context of the current document</param>
        public PatternResult(T item, bool success, string message, Exception exception,
            OperationContext context)
        {
            Item = item;
            Success = success;
            Message = message;
            Exception = exception;
            Context = context;
        }

        /// <summary>
        /// Item to include in the result
        /// </summary>
        public T Item { get; private set; }

        /// <summary>
        /// Success of the operation
        /// </summary>
        public bool Success { get; private set; }

        /// <summary>
        /// Message returned by the operation
        /// </summary>
        public string Message { get; private set; }

        /// <summary>
        /// Exception returned by the operation
        /// </summary>
        public Exception Exception { get; private set; }

        /// <summary>
        /// Updated context for the current document based on the operation
        /// </summary>
        public OperationContext Context { get; private set; }
    }
}
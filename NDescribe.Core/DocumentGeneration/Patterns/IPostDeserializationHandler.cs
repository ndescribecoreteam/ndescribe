﻿using NDescribe.Core.DataBridge;
using NDescribe.Core.Session;

namespace NDescribe.Core.DocumentGeneration.Patterns
{
    /// <summary>
    /// Handler that fires immediately after a document has been deserialized
    /// </summary>
    /// <typeparam name="TDocument">The document type that has been deserialized</typeparam>
    public interface IPostDeserializationHandler<TDocument> : IPatternHandler
    {
        /// <summary>
        /// Allows any post-reading processing
        /// </summary>
        /// <param name="document">Document to process</param>
        /// <param name="documentDetails">Details about the current document</param>
        /// <param name="context">Current context of the document being processed</param>
        /// <param name="dataBridge">IDataBridge to process data with if required</param>
        /// <returns>The result of the operation and potentially modified DocumentWrapper</returns>
        PatternResult<IDocumentWrapper<TDocument>> HandlePostDeserialization(IDocumentWrapper<TDocument> document,
            DocumentDetails documentDetails, OperationContext context,
            IDataBridge dataBridge);
    }
}
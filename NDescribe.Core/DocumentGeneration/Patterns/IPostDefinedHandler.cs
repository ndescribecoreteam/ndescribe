﻿using NDescribe.Core.Session;

namespace NDescribe.Core.DocumentGeneration.Patterns
{
    /// <summary>
    /// A handler that fires immediately after a type has been fully defined
    /// </summary>
    public interface IPostDefinedHandler : IPatternHandler
    {
        /// <summary>
        /// Handle a post definition event
        /// </summary>
        /// <param name="dataSession">The IDataSession which was used for defining this type</param>
        /// <param name="documentDetails">Details of the defined document</param>
        /// <returns>The result of the handler call</returns>
        PostDefinedResult HandlePostDefined(IDataSession dataSession, DocumentDetails documentDetails);
    }
}
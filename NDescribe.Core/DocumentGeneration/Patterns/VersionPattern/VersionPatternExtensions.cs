﻿using NDescribe.Core.DataBridge;
using System;
using System.Linq;
using NDescribe.Core.Session;

namespace NDescribe.Core.DocumentGeneration.Patterns.VersionPattern
{
    /// <summary>
    /// Extensions for managing the version pattern
    /// </summary>
    public static class VersionPatternExtensions
    {
        private const string DefaultKeyFormat = "{0}::{1}";

        private const string DefaultVersionKeyFormat = "{0}::Version";

        /// <summary>
        /// Add versioning to a document configuration
        /// </summary>
        /// <typeparam name="TDocument">Document to add</typeparam>
        /// <param name="configurationBuilder">Configuration builder currently active for this document</param>
        /// <param name="documentKeyModifier">Document key modifier for post-version</param>
        /// <param name="versionCounterKey">Version counter key</param>
        /// <returns>The updated configuration builder</returns>
        public static IDocumentConfigurationBuilder<TDocument> WithVersioning<TDocument>(
            this IDocumentConfigurationBuilder<TDocument> configurationBuilder,
            Func<string, long, string> documentKeyModifier, Func<string, string> versionCounterKey)
        {
            var handler = new VersionPatternHandler<TDocument>(documentKeyModifier, versionCounterKey);
            configurationBuilder.AddHandler(handler);
            return configurationBuilder;
        }

        /// <summary>
        /// Add versioning to a document configuration
        /// </summary>
        /// <typeparam name="TDocument">Document to add</typeparam>
        /// <param name="configurationBuilder">Configuration builder currently active for this document</param>
        /// <returns>The updated configuration builder</returns>
        public static IDocumentConfigurationBuilder<TDocument> WithVersioning<TDocument>(
            this IDocumentConfigurationBuilder<TDocument> configurationBuilder)
        {
            return WithVersioning(configurationBuilder, (x, y) => string.Format(DefaultKeyFormat, x, y),
                x => string.Format(DefaultVersionKeyFormat, x));
        }

        /// <summary>
        /// Add versioning to an already defined document context
        /// </summary>
        /// <typeparam name="TDocument">Document type to add versioning for</typeparam>
        /// <param name="document">Document to add versioning to</param>
        internal static void AddVersioning<TDocument>(this IInternalDefinedContext<TDocument> document)
        {
            if (document.KeyGenerationHandlers.Count(x => x is VersionPatternHandler<TDocument>) == 0)
            {
                var handler = new VersionPatternHandler<TDocument>((x, y) => string.Format(DefaultKeyFormat, x, y),
                    x => string.Format(DefaultVersionKeyFormat, x));
                document.KeyGenerationHandlers.Add(handler);
            }
        }

        /// <summary>
        /// Read a specific version of a document
        /// </summary>
        /// <typeparam name="TDocument">Type of document to read</typeparam>
        /// <param name="dataSession">Data session to use</param>
        /// <param name="documentTemplate">Document template to use for building the initial key</param>
        /// <param name="version">Version of the document</param>
        /// <returns>The result of reading the document</returns>
        public static DataRetrievalResult<TDocument> ReadWithVersion<TDocument>(this IDataSession dataSession,
            TDocument documentTemplate, long version)
        {
            var documentContext = new OperationContext
            {
                ContextualObjects = {[VersionPatternHandler<TDocument>.DocumentContextVersionKey] = version}
            };

            return dataSession.Read(documentTemplate, documentContext);
        }

        /// <summary>
        /// Save a version
        /// </summary>
        /// <typeparam name="TDocument">Document type to save</typeparam>
        /// <param name="dataSession">Data session to use for saving</param>
        /// <param name="document">Document to save</param>
        /// <param name="version">Version to save this document to</param>
        /// <returns></returns>
        public static DataResult SaveWithVersion<TDocument>(this IDataSession dataSession, TDocument document,
            long version)
        {
            var documentContext = new OperationContext
            {
                ContextualObjects = {{VersionPatternHandler<TDocument>.DocumentContextVersionKey, version}}
            };
            return dataSession.Save(document, documentContext);
        }

        /// <summary>
        /// Get the latest version number of a document
        /// </summary>
        /// <typeparam name="TDocument">Type of document to retrieve</typeparam>
        /// <param name="dataSession">Data session this applies to</param>
        /// <param name="documentTemplate">The document template to use for generating a base key</param>
        /// <returns>The retrieved version number</returns>
        public static DataRetrievalResult<long> ReadLatestVersion<TDocument>(this IDataSession dataSession,
            TDocument documentTemplate)
        {
            return ReadLatestVersion(dataSession, documentTemplate, new OperationContext());
        }

        /// <summary>
        /// Get the latest version number of a document
        /// </summary>
        /// <typeparam name="TDocument">Type of document to retrieve</typeparam>
        /// <param name="dataSession">Data session this applies to</param>
        /// <param name="documentTemplate">The document template to use for generating a base key</param>
        /// <param name="context">The document context to use</param>
        /// <returns>The retrieved version number</returns>
        public static DataRetrievalResult<long> ReadLatestVersion<TDocument>(this IDataSession dataSession,
            TDocument documentTemplate, OperationContext context)
        {
            var queryableSession = (IQueryableSession) dataSession;
            var handler = queryableSession.GetPatternHandlerForDocument<TDocument, VersionPatternHandler<TDocument>>();
            return handler.GetLatestVersionNumber(dataSession, documentTemplate, context);
        }
    }
}
﻿using System;
using System.Linq;
using NDescribe.Core.DataBridge;
using NDescribe.Core.DocumentGeneration.Traits;
using NDescribe.Core.Session;

namespace NDescribe.Core.DocumentGeneration.Patterns.VersionPattern
{
    /// <summary>
    /// Handler for the version pattern
    /// </summary>
    /// <typeparam name="TDocument">Document to be versioned</typeparam>
    public class VersionPatternHandler<TDocument> : IKeyGenerationHandler<TDocument>, IUniquePattern
    {
        /// <summary>
        /// Key used for storing the version in the document context
        /// </summary>
        public const string DocumentContextVersionKey = "VERSION_PATTERN_VERSION";

        private const string DocumentContextIgnoreOnRead = "VERSION_PATTERN_IGNORE_READ_EVENT";

        private readonly Func<string, long, string> documentKeyModifier;

        private readonly Func<string, string> versionCounterKey;

        /// <summary>
        /// Create a new version pattern handler
        /// </summary>
        /// <param name="documentKeyModifier">The key used to modify the document key with the version number</param>
        /// <param name="versionCounterKey">The key for the version counter</param>
        public VersionPatternHandler(Func<string, long, string> documentKeyModifier,
            Func<string, string> versionCounterKey)
        {
            this.documentKeyModifier = documentKeyModifier;
            this.versionCounterKey = versionCounterKey;
        }

        /// <summary>
        /// Get the latest version number for a given document
        /// </summary>
        /// <param name="dataSession">The data session that this document belongs to</param>
        /// <param name="documentTemplate">The template of the document to retrieve the version for</param>
        /// <param name="context">Current context of this document</param>
        /// <returns>The latest version number of the document</returns>
        /// <exception cref="Exception">A delegate callback throws an exception.</exception>
        public DataRetrievalResult<long> GetLatestVersionNumber(IDataSession dataSession, TDocument documentTemplate,
            OperationContext context)
        {
            var queryableSession = (IQueryableSession) dataSession;
            var documentDetails = queryableSession.GetDocumentDetailsForType<TDocument>();
            var internalDefinedContext = (IInternalDefinedContext<TDocument>) documentDetails.DefinedContext;
            var key = internalDefinedContext.GenerateKeyToHandler(documentTemplate, documentDetails, this, context, queryableSession.DataBridge);

            var counterKey = versionCounterKey(key);
            return queryableSession.DataBridge.GetCounterValue(documentDetails.DataLocation, counterKey, internalDefinedContext.DataTraits.Union(context.ContextualTraits.OfType<IDataTrait>()));
        }

        /// <summary>
        /// Handle the generation of the versioning key before a read occurs
        /// </summary>
        /// <param name="currentKey">The current key of the document</param>
        /// <param name="templateDocument">The template document to generate a key for</param>
        /// <param name="documentDetails">Details of the document</param>
        /// <param name="context">Context of the current operation</param>
        /// <param name="dataBridge">Data bridge to use for accessing additional data</param>
        /// <returns>Result of adding the version key</returns>
        /// <exception cref="Exception">A delegate callback throws an exception.</exception>
        public PatternResult<string> HandlePreReadKeyGeneration(string currentKey, TDocument templateDocument, DocumentDetails documentDetails,
            OperationContext context, IDataBridge dataBridge)
        {
            var dataLocation = documentDetails.DataLocation;
            var dataTraits = ((IInternalDefinedContext<TDocument>)documentDetails.DefinedContext).DataTraits.Union(context.ContextualTraits.OfType<IDataTrait>()).ToList();

            if (context.ContextualObjects.ContainsKey(DocumentContextIgnoreOnRead))
            {
                return new PatternResult<string>(currentKey, true, string.Empty, null, context);
            }

            long currentVersion;

            if (context.ContextualObjects.ContainsKey(DocumentContextVersionKey))
            {
                currentVersion = (long)context.ContextualObjects[DocumentContextVersionKey];
            }
            else
            {
                string currentVersionCounterKey = versionCounterKey(currentKey);
                var counterResult = dataBridge.GetCounterValue(dataLocation, currentVersionCounterKey, dataTraits);

                if (!counterResult.Success)
                {
                    return new PatternResult<string>(currentKey, false,
                        $"VersionPatternHandler - error reading counter for version: {counterResult.Message}", counterResult.Exception, context);
                }

                currentVersion = counterResult.Document;
            }

            string documentKey = documentKeyModifier(currentKey, currentVersion);

            while (currentVersion > 0 && !dataBridge.KeyExists(dataLocation, documentKey, dataTraits).Document)
            {
                currentVersion--;
                documentKey = documentKeyModifier(currentKey, currentVersion);
            }

            if (currentVersion <= 0)
            {
                return new PatternResult<string>(documentKey, false, "Failed to find any version of the document", null, context);
            }

            return new PatternResult<string>(documentKey, true, string.Empty, null, context);
        }

        /// <summary>
        /// Handle the generation of a versioned key before storing a document
        /// </summary>
        /// <param name="currentKey">Name of the current key</param>
        /// <param name="document">Document that is being stored</param>
        /// <param name="documentDetails">Details of the document to be stored</param>
        /// <param name="context">Current context of the operation</param>
        /// <param name="dataBridge">Data bridge to use when storing or retrieving data for this document</param>
        /// <returns>The result of adding the version to the key</returns>
        /// <exception cref="Exception">A delegate callback throws an exception.</exception>
        public PatternResult<string> HandlePreStoreKeyGeneration(string currentKey, TDocument document, DocumentDetails documentDetails,
            OperationContext context, IDataBridge dataBridge)
        {
            var traitsList = ((IInternalDefinedContext<TDocument>)documentDetails.DefinedContext).DataTraits.Union(context.ContextualTraits.OfType<IDataTrait>()).ToList();
            var dataLocation = documentDetails.DataLocation;

            string counterKey = versionCounterKey(currentKey);

            long counterValue = 0;

            if (context.ContextualObjects.ContainsKey(DocumentContextVersionKey))
            {
                counterValue = (long)context.ContextualObjects[DocumentContextVersionKey];
                dataBridge.SetCounterValue(dataLocation, counterKey, counterValue, traitsList);
            }

            if (counterValue == 0)
            {
                var counterResult = dataBridge.IncrementCounter(dataLocation, counterKey, traitsList);

                if (!counterResult.Success)
                {
                    return new PatternResult<string>(currentKey, false,
                        $"VersionPatternHandler - error in prestorage incrementing counter: {counterResult.Message}", counterResult.Exception, context);
                }

                counterValue = counterResult.Document;
            }
            string documentKey = documentKeyModifier(currentKey, counterValue);
            return new PatternResult<string>(documentKey, true, string.Empty, null, context);
        }

        /// <summary>
        /// Whether to use the key generation when conducting a query
        /// </summary>
        /// <returns>False as no queries should have the versioning pattern included</returns>
        public bool IncludeInQueries()
        {
            return false;
        }
    }
}
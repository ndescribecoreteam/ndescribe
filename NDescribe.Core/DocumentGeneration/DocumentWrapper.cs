﻿using System.Collections.Generic;

namespace NDescribe.Core.DocumentGeneration
{
    /// <summary>
    /// Document wrapper
    /// </summary>
    /// <typeparam name="TDocument">The type from the domain model</typeparam>
    internal class DocumentWrapper<TDocument> : IDocumentWrapper<TDocument>
    {
        /// <summary>
        /// Document wrapper
        /// </summary>
        /// <param name="version">Version of this document</param>
        /// <param name="document">Document to use</param>
        /// <param name="metadata">Metadata about the document</param>
        public DocumentWrapper(string version, TDocument document, IDictionary<string, object> metadata)
        {
            Version = version;
            Document = document;
        }

        /// <summary>
        /// Version of this document
        /// </summary>
        public string Version { get; }

        /// <summary>
        /// Document to wrap
        /// </summary>
        public TDocument Document { get; }

        /// <summary>
        /// Metadata about the document
        /// </summary>
        public IDictionary<string, object> Metadata { get; }
    }
}
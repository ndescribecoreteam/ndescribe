﻿using System;
using System.Collections.Generic;
using System.Linq;
using NDescribe.Core.DataBridge;
using NDescribe.Core.DocumentGeneration.Patterns;
using NDescribe.Core.DocumentGeneration.Traits;
using NDescribe.Core.Session;

namespace NDescribe.Core.DocumentGeneration
{
    internal class DefinedContext<TDocument> : IInternalDefinedContext<TDocument>, IDefinedContext
    {
        public DefinedContext(Func<TDocument, string> keyGenerator,
            IEnumerable<IPostDeserializationHandler<TDocument>> postDeserializationHandlers,
            IEnumerable<IPostStorageHandler<TDocument>> postStorageHandlers,
            IEnumerable<IPreReadHandler> preReadHandlers,
            IEnumerable<IPreSerializationHandler<TDocument>> preSerializationHandlers,
            IEnumerable<IPreStorageHandler> preStorageHandlers, IEnumerable<IPostDefinedHandler> postDefinedHandlers,
            IEnumerable<IKeyGenerationHandler<TDocument>> keyGenerationHandlers,
            IEnumerable<IPostDeleteHandler<TDocument>> preDeleteHandlers,
            IEnumerable<IDataTrait> dataTraits)
        {
            PostDeserializationHandlers = new List<IPostDeserializationHandler<TDocument>>(postDeserializationHandlers);
            PostStorageHandlers = new List<IPostStorageHandler<TDocument>>(postStorageHandlers);
            PreReadHandlers = new List<IPreReadHandler>(preReadHandlers);
            PreSerializationHandlers = new List<IPreSerializationHandler<TDocument>>(preSerializationHandlers);
            PreStorageHandlers = new List<IPreStorageHandler>(preStorageHandlers);
            PostDefinedHandlers = new List<IPostDefinedHandler>(postDefinedHandlers);
            this.keyGenerator = keyGenerator;
            KeyGenerationHandlers = new List<IKeyGenerationHandler<TDocument>>(keyGenerationHandlers);
            PostDeleteHandlers = new List<IPostDeleteHandler<TDocument>>(preDeleteHandlers);
            DataTraits = new List<IDataTrait>(dataTraits);
        }

        private readonly Func<TDocument, string> keyGenerator;

        public List<IPostDeserializationHandler<TDocument>> PostDeserializationHandlers { get; }

        public List<IPostStorageHandler<TDocument>> PostStorageHandlers { get; }

        public List<IPreReadHandler> PreReadHandlers { get; }

        public List<IKeyGenerationHandler<TDocument>> KeyGenerationHandlers { get; }

        public List<IPreSerializationHandler<TDocument>> PreSerializationHandlers { get; }

        public List<IPreStorageHandler> PreStorageHandlers { get; }

        public List<IPostDefinedHandler> PostDefinedHandlers { get; }

        public List<IPostDeleteHandler<TDocument>> PostDeleteHandlers { get; }

        public List<IDataTrait> DataTraits { get; } 

        public Type DocumentType => typeof (TDocument);

        public IEnumerable<IPatternHandler> AllHandlers
        {
            get
            {
                foreach (var handler in PostDeserializationHandlers)
                {
                    yield return handler;
                }
                foreach (var handler in PostStorageHandlers)
                {
                    yield return handler;
                }
                foreach (var handler in PostStorageHandlers)
                {
                    yield return handler;
                }
                foreach (var handler in PreReadHandlers)
                {
                    yield return handler;
                }
                foreach (var handler in KeyGenerationHandlers)
                {
                    yield return handler;
                }
                foreach (var handler in PreSerializationHandlers)
                {
                    yield return handler;
                }
                foreach (var handler in PreStorageHandlers)
                {
                    yield return handler;
                }
                foreach (var handler in PostDefinedHandlers)
                {
                    yield return handler;
                }
            }
        }

        public string GenerateInitialKey(TDocument document)
        {
            return keyGenerator(document);
        }

        public string GenerateKeyToHandler<THandler>(TDocument document, DocumentDetails documentDetails, THandler handler, OperationContext context, IDataBridge dataBridge) where THandler : IKeyGenerationHandler<TDocument>
        {
            var key = GenerateInitialKey(document);

            var handlerRange =
                KeyGenerationHandlers.TakeWhile(
                    keyGenerationHandler => keyGenerationHandler != (IKeyGenerationHandler<TDocument>) handler);

            return GenerateKeyFromHandlerRange(key, document, documentDetails, context, dataBridge, handlerRange);
        }

        public string GenerateKeyFromHandler<THandler>(TDocument document, DocumentDetails documentDetails, THandler handler, string currentKey, OperationContext context,
            IDataBridge dataBridge) where THandler : IKeyGenerationHandler<TDocument>
        {
            var handlerRange =
                KeyGenerationHandlers.SkipWhile(
                    keyGenerationHandler => keyGenerationHandler != (IKeyGenerationHandler<TDocument>)handler).Skip(1);

            return GenerateKeyFromHandlerRange(currentKey, document, documentDetails, context, dataBridge, handlerRange);
        }

        private static string GenerateKeyFromHandlerRange(string key, TDocument document, DocumentDetails documentDetails,
            OperationContext context, IDataBridge dataBridge, IEnumerable<IKeyGenerationHandler<TDocument>> handlerRange)
        {
            var keyGenerationResult =
                handlerRange.Aggregate(
                    new PatternResult<string>(key, true, string.Empty, null, context),
                    (result, handler) =>
                        result.Success
                            ? handler.HandlePreReadKeyGeneration(result.Item, document, documentDetails,
                                context, dataBridge)
                            : result);

            if (!keyGenerationResult.Success)
            {
                throw new NDescribeException($"Error generating key: {(!string.IsNullOrEmpty(keyGenerationResult.Message) ? keyGenerationResult.Message : "Unknown error")}",
                    keyGenerationResult.Exception);
            }

            key = keyGenerationResult.Item;

            return key;
        }

        public string GenerateFullKey(TDocument document, DocumentDetails documentDetails, OperationContext context, IDataBridge dataBridge, bool dataAccessQuery)
        {
            var handlers = KeyGenerationHandlers;
            if (!dataAccessQuery)
            {
                handlers = handlers.Where(x => x.IncludeInQueries()).ToList();
            }

            var initialKey = GenerateInitialKey(document);
            return GenerateKeyFromHandlerRange(initialKey, document, documentDetails, context, dataBridge, handlers);
        }

        public PostDefinedResult RunPostDefinedHandlers(IDataSession dataSession, DocumentDetails documentDetails)
        {
            foreach (
                var result in
                    PostDefinedHandlers.Select(
                        postDefinedHandler => postDefinedHandler.HandlePostDefined(dataSession, documentDetails))
                        .Where(result => !result.Success))
            {
                return new PostDefinedResult(false,
                    $"Error running post defined handler: {result.Message}", result.Exception);
            }

            return new PostDefinedResult(true, string.Empty, null);
        }
    }
}
﻿using NDescribe.Core.Configuration;
using System.Collections.Generic;
using NDescribe.Core.Session;

namespace NDescribe.Core
{
    /// <summary>
    /// Used to create data sessions
    /// </summary>
    public class DataSessionFactory
    {
        /// <summary>
        /// Default version of data sessions
        /// </summary>
        public const string DefaultVersion = "1.0.0";

        /// <summary>
        /// Create a session with provided options
        /// </summary>
        /// <param name="dataContexts">Data contexts to associate with this session</param>
        /// <param name="configurationOptions">Configuration options to use for this session</param>
        /// <param name="version">Version to use for this data session (to allow strict versioning)</param>
        /// <returns>A data session created with the provided options</returns>
        public IDataSession CreateSession(IEnumerable<DataContext> dataContexts,
            ConfigurationOptions configurationOptions, string version = DefaultVersion)
        {
            return new DataSession(configurationOptions, dataContexts, version);
        }
    }
}
﻿using NDescribe.Core.DataBridge;
using NDescribe.Core.Serialization;

namespace NDescribe.Core.Configuration
{
    /// <summary>
    /// Holds configuration options for the main session
    /// </summary>
    public class ConfigurationOptions
    {
        /// <summary>
        /// Default configuration options constructor
        /// </summary>
        public ConfigurationOptions(IDataBridgeConfigurationProvider dataBridgeConfigurationProvider)
        {
            DataBridgeConfigurationProvider = dataBridgeConfigurationProvider;
            SerializationProvider = new NewtonsoftSerializationProvider();
        }

        /// <summary>
        /// Specify the provider for the data bridge
        /// </summary>
        public IDataBridgeConfigurationProvider DataBridgeConfigurationProvider { get; set; }

        /// <summary>
        /// Specify the serialization provider
        /// </summary>
        public ISerializationProvider SerializationProvider { get; set; }
    }
}
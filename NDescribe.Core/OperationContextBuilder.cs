﻿using System.Collections.Generic;
using NDescribe.Core.DocumentGeneration.Traits;

namespace NDescribe.Core
{
    /// <summary>
    /// Allows a way of building an operation context up
    /// </summary>
    public class OperationContextBuilder
    {
        private readonly IDictionary<string, object> metadata = new Dictionary<string, object>();

        /// <summary>
        /// Add metadata to the operation context
        /// </summary>
        /// <param name="key">Key for the metadata</param>
        /// <param name="value">Value for the metadata</param>
        /// <returns>The metadata</returns>
        public OperationContextBuilder WithMetaData(string key, object value)
        {
            metadata.Add(key, value);
            return this;
        }

        /// <summary>
        /// Build an operation context object
        /// </summary>
        /// <returns>The built OperationContext</returns>
        public OperationContext Build()
        {
            return new OperationContext
            {
                ContextualObjects = new Dictionary<string, object>(),
                ContextualTraits = new List<ITrait>(),
                Metadata = metadata
            };
        }
    }
}
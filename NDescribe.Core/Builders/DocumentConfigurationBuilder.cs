﻿using System;
using System.Collections.Generic;
using NDescribe.Core.DocumentGeneration;
using NDescribe.Core.DocumentGeneration.Patterns;
using NDescribe.Core.DocumentGeneration.Traits;

namespace NDescribe.Core.Builders
{
    internal class DocumentConfigurationBuilder<TDocument> : IDocumentConfigurationBuilder<TDocument>
    {
        private readonly Func<TDocument, string> keyBuilder;

        private readonly List<IPostDeserializationHandler<TDocument>> postDeserializationHandlers =
            new List<IPostDeserializationHandler<TDocument>>();

        private readonly List<Type> uniquePostDeserializationHandlers = new List<Type>();

        private readonly List<IPostStorageHandler<TDocument>> postStorageHandlers = new List<IPostStorageHandler<TDocument>>();

        private readonly List<Type> uniquePostStorageHandlers = new List<Type>();

        private readonly List<IPreSerializationHandler<TDocument>> preSerializationHandlers =
            new List<IPreSerializationHandler<TDocument>>();

        private readonly List<Type> uniquePreSerializationHandlers = new List<Type>();

        private readonly List<IPreStorageHandler> preStorageHandlers = new List<IPreStorageHandler>();

        private readonly List<Type> uniquePreStorageHandlers = new List<Type>();

        private readonly List<IPreReadHandler> preReadHandlers = new List<IPreReadHandler>();

        private readonly List<Type> uniquePreReadHandlers = new List<Type>();

        private readonly List<IPostDefinedHandler> postDefinedHandlers = new List<IPostDefinedHandler>();

        private readonly List<IKeyGenerationHandler<TDocument>> keyGenerationHandlers = new List<IKeyGenerationHandler<TDocument>>();

        private readonly List<Type> uniqueKeyGenerationHandlers = new List<Type>(); 

        private readonly List<Type> uniquePostDefinedHandlers = new List<Type>();

        private readonly List<IPostDeleteHandler<TDocument>>  preDeleteHandlers = new List<IPostDeleteHandler<TDocument>>();

        private readonly List<Type> uniquePreDeleteHandlers = new List<Type>(); 

        private readonly List<IDataTrait> dataTraits = new List<IDataTrait>();

        private readonly DataContext context;

        public DocumentConfigurationBuilder(Func<TDocument, string> keyBuilder, DataContext context)
        {
            this.keyBuilder = keyBuilder;
            this.context = context;
        }

        public void AddHandler(IPatternHandler handler)
        {
            var definedHandler = handler as IPostDefinedHandler;
            var postDeserializationHandler = handler as IPostDeserializationHandler<TDocument>;
            var postStorageHandler = handler as IPostStorageHandler<TDocument>;
            var preSerializationHandler = handler as IPreSerializationHandler<TDocument>;
            var preStorageHandler = handler as IPreStorageHandler;
            var preReadHandler = handler as IPreReadHandler;
            var keyGenerationHandler = handler as IKeyGenerationHandler<TDocument>;
            var preDeleteHandler = handler as IPostDeleteHandler<TDocument>;

            SafeAddGeneric(definedHandler, AddPostDefinedHandler);
            SafeAddGeneric(keyGenerationHandler, AddKeyGenerationHandler);
            SafeAddGeneric(preReadHandler, AddPreReadHandler);
            SafeAddGeneric(postDeserializationHandler, AddPostDeserializationHandler);
            SafeAddGeneric(preSerializationHandler, AddPreSerializationHandler);
            SafeAddGeneric(preStorageHandler, AddPreStorageHandler);
            SafeAddGeneric(postStorageHandler, AddPostStorageHandler);
            SafeAddGeneric(preDeleteHandler, AddPreDeleteHandler);
        }

        public void AddTrait(ITrait trait)
        {
            var dataTrait = trait as IDataTrait;

            SafeAddGeneric(dataTrait, x => dataTraits.Add(x));
        }

        private void SafeAddGeneric<T>(T item, Action<T> addAction)
        {
            if (item != null)
            {
                addAction(item);
            }
        }

        private void AddPostDeserializationHandler(IPostDeserializationHandler<TDocument> postDeserializationHandler)
        {
            // ReSharper disable once SuspiciousTypeConversion.Global
            DoUniqueChecks(postDeserializationHandler as IUniquePattern, uniquePostDeserializationHandlers);
            postDeserializationHandlers.Add(postDeserializationHandler);
        }

        private void AddPostStorageHandler(IPostStorageHandler<TDocument> postStorageHandler)
        {
            // ReSharper disable once SuspiciousTypeConversion.Global
            DoUniqueChecks(postStorageHandler as IUniquePattern, uniquePostStorageHandlers);
            postStorageHandlers.Add(postStorageHandler);
        }

        private void AddPreReadHandler(IPreReadHandler preReadHandler)
        {
            // ReSharper disable once SuspiciousTypeConversion.Global
            DoUniqueChecks(preReadHandler as IUniquePattern, uniquePreReadHandlers);
            preReadHandlers.Add(preReadHandler);
        }

        private void AddPreSerializationHandler(IPreSerializationHandler<TDocument> preSerializationHandler)
        {
            // ReSharper disable once SuspiciousTypeConversion.Global
            DoUniqueChecks(preSerializationHandler as IUniquePattern, uniquePreSerializationHandlers);
            preSerializationHandlers.Add(preSerializationHandler);
        }

        private void AddPreStorageHandler(IPreStorageHandler preStoragePatternHandler)
        {
            DoUniqueChecks(preStoragePatternHandler as IUniquePattern, uniquePreStorageHandlers);
            preStorageHandlers.Add(preStoragePatternHandler);
        }

        private void AddKeyGenerationHandler(IKeyGenerationHandler<TDocument> keyGenerationHandler)
        {
            DoUniqueChecks(keyGenerationHandler as IUniquePattern, uniqueKeyGenerationHandlers);
            keyGenerationHandlers.Add(keyGenerationHandler);
        }

        private void AddPreDeleteHandler(IPostDeleteHandler<TDocument> postDeleteHandler)
        {
            // ReSharper disable once SuspiciousTypeConversion.Global
            DoUniqueChecks(postDeleteHandler as IUniquePattern, uniquePreDeleteHandlers);
            preDeleteHandlers.Add(postDeleteHandler);
        }

        private void AddPostDefinedHandler(IPostDefinedHandler postDefinedHandler)
        {
            // ReSharper disable once SuspiciousTypeConversion.Global
            DoUniqueChecks(postDefinedHandler as IUniquePattern, uniquePostDefinedHandlers);
            postDefinedHandlers.Add(postDefinedHandler);
        }

        /// <summary>
        /// Mark this document definition as complete
        /// </summary>
        /// <returns>The original data context to allow chaining of document definitions</returns>
        public DataContext Defined()
        {
            var definedContext = new DefinedContext<TDocument>(keyBuilder, postDeserializationHandlers, postStorageHandlers,
                preReadHandlers, preSerializationHandlers, preStorageHandlers, postDefinedHandlers, keyGenerationHandlers, preDeleteHandlers, dataTraits);
            context.AddDefinedContext(definedContext);
            return context;
        }


        private void DoUniqueChecks(IUniquePattern uniquePattern, List<Type> storedUniqueTypes)
        {
            if (uniquePattern == null)
            {
                return;
            }

            Type patternType = uniquePattern.GetType();

            if (storedUniqueTypes.Contains(patternType))
            {
                throw new ArgumentException(
                    $"Cannot bind more than one {patternType.Name} as it is declared a unique pattern");
            }

            storedUniqueTypes.Add(patternType);
        }
    }
}
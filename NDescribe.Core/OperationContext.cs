﻿using System.Collections.Generic;
using NDescribe.Core.DocumentGeneration.Traits;

namespace NDescribe.Core
{
    /// <summary>
    /// Context for one operation
    /// </summary>
    public class OperationContext
    {
        /// <summary>
        /// Contextual objects for this document operation
        /// </summary>
        public IDictionary<string, object> ContextualObjects { get; set; } = new Dictionary<string, object>();

        /// <summary>
        /// Any traits related to this document only for this context
        /// </summary>
        public IList<ITrait> ContextualTraits { get; set; } = new List<ITrait>();

        /// <summary>
        /// Metadata for this operation
        /// </summary>
        public IDictionary<string, object> Metadata { get; set; } = new Dictionary<string, object>(); 
    }
}
﻿using NDescribe.Core.DocumentGeneration.Patterns;
using System;
using NDescribe.Core.Session;

namespace NDescribe.Core
{
    /// <summary>
    /// Context once it has been defined.
    /// </summary>
    public interface IDefinedContext
    {
        /// <summary>
        /// Represents the type that this context operates on
        /// </summary>
        Type DocumentType { get; }

        /// <summary>
        /// Run all post defined handlers for this context
        /// </summary>
        /// <param name="dataSession">Data session for this document definition to use</param>
        /// <param name="documentDetails">Details of the containing document for this definition to use</param>
        /// <returns>Success result, or failure of first one to fail</returns>
        PostDefinedResult RunPostDefinedHandlers(IDataSession dataSession, DocumentDetails documentDetails);
    }
}
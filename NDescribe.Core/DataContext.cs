﻿using System;
using NDescribe.Core.Builders;
using System.Collections.Generic;

namespace NDescribe.Core
{
    /// <summary>
    /// Used to create document data context information
    /// </summary>
    public class DataContext
    {
        private readonly List<IDefinedContext> definedContexts = new List<IDefinedContext>();

        /// <summary>
        /// Create a new document data context
        /// </summary>
        /// <param name="dataLocation">Location for these documents to reside</param>
        public DataContext(string dataLocation)
        {
            DataLocation = dataLocation;
        }

        /// <summary>
        /// Get the list of defined contexts for <see langword="internal"/> use
        /// </summary>
        internal IEnumerable<IDefinedContext> DefinedContexts => definedContexts;

        /// <summary>
        /// Get the data location for this data context
        /// </summary>
        public string DataLocation { get; }

        /// <summary>
        /// Define a new document type
        /// </summary>
        /// <typeparam name="TDocument">Type to define</typeparam>
        /// <param name="keyBuilder">Main key builder for this type</param>
        /// <returns>A builder for further configuring this type</returns>
        public IDocumentConfigurationBuilder<TDocument> Define<TDocument>(Func<TDocument, string> keyBuilder)
        {
            return new DocumentConfigurationBuilder<TDocument>(keyBuilder, this);
        }

        /// <summary>
        /// Add a fully defined type to this data context
        /// </summary>
        /// <param name="context">Defined context to add</param>
        internal void AddDefinedContext(IDefinedContext context)
        {
            definedContexts.Add(context);
        }
    }
}
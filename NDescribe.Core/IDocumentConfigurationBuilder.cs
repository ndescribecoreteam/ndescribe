﻿using NDescribe.Core.DocumentGeneration.Patterns;
using NDescribe.Core.DocumentGeneration.Traits;

namespace NDescribe.Core
{
    /// <summary>
    /// Used to build the configuration for a document for use in a context
    /// </summary>
    /// <typeparam name="TDocument">Document for this configuration builder</typeparam>
    // ReSharper disable once UnusedTypeParameter - enables easier resolution of types in other usages
    public interface IDocumentConfigurationBuilder<TDocument>
    {
        /// <summary>
        /// Complete this definition block
        /// </summary>
        /// <returns>The original context</returns>
        DataContext Defined();

        /// <summary>
        /// Add a specific pattern handler (see extension methods for common patterns)
        /// </summary>
        /// <param name="handler">Handler to add</param>
        void AddHandler(IPatternHandler handler);

        /// <summary>
        /// Add a specific trait to the document definition
        /// </summary>
        /// <param name="trait">Trait to add</param>
        void AddTrait(ITrait trait);
    }
}
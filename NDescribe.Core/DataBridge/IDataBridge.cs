﻿using System.Collections.Generic;
using NDescribe.Core.DocumentGeneration.Traits;

namespace NDescribe.Core.DataBridge
{
    /// <summary>
    /// Core data bridge interface
    /// </summary>
    public interface IDataBridge
    {
        /// <summary>
        /// Attempt to save a document, will overwrite if the document already exists
        /// </summary>
        /// <param name="key">Key of the document to store</param>
        /// <param name="document">Serialized document to store</param>
        /// <param name="dataLocation">Location of the document to store</param>
        /// <param name="traits">Any data traits for this document</param>
        /// <returns>The result of the operation</returns>
        DataResult SaveDocument(string dataLocation, string key, string document, IEnumerable<IDataTrait> traits);

        /// <summary>
        /// Attempt to create a document, will fail if the document already exists
        /// </summary>
        /// <param name="key">Key of the document to store</param>
        /// <param name="document">Serialized document to store</param>
        /// <param name="dataLocation">Location of the document to store</param>
        /// <param name="traits">Any data traits for this document</param>
        /// <returns>The result of the operation</returns>
        DataResult CreateDocument(string dataLocation, string key, string document, IEnumerable<IDataTrait> traits);

        /// <summary>
        /// Create a lock document
        /// </summary>
        /// <param name="dataLocation">Location to create the lock</param>
        /// <param name="key">Key to create the lock with</param>
        /// <param name="expiry">Expiry time for creating a lock</param>
        /// <param name="traits">Any data traits for this document</param>
        /// <returns>Success of the operation and whether the document exists or not</returns>
        DataRetrievalResult<bool> CreateLock(string dataLocation, string key, long expiry, IEnumerable<IDataTrait> traits);

        /// <summary>
        /// Attempt to get a document
        /// </summary>
        /// <param name="key">Key of the document to get</param>
        /// <param name="dataLocation">Location of the document to get</param>
        /// <param name="traits">Any data traits for this document</param>
        /// <returns>The result of the operation, and serialized document if retrieved successfully</returns>
        DataRetrievalResult<string> GetDocument(string dataLocation, string key, IEnumerable<IDataTrait> traits);

        /// <summary>
        /// Attempt to get multiple documents
        /// </summary>
        /// <param name="dataLocation">Location of the data to retrieve</param>
        /// <param name="keys">Collection of keys to retrieve</param>
        /// <param name="traits">Any data traits for this document</param>
        /// <returns></returns>
        DataRetrievalResult<IDictionary<string, DataRetrievalResult<string>>> GetDocuments(string dataLocation, IEnumerable<string> keys,
            IEnumerable<IDataTrait> traits);

            /// <summary>
        /// Get a counter value
        /// </summary>
        /// <param name="dataLocation">Location of the counter value</param>
        /// <param name="key">Key of the counter value</param>
        /// <param name="traits">Any data traits for this document</param>
        /// <returns>The value of the counter</returns>
        DataRetrievalResult<long> GetCounterValue(string dataLocation, string key, IEnumerable<IDataTrait> traits);

        /// <summary>
        /// Set a counter value explicitly
        /// </summary>
        /// <param name="dataLocation">Location of the counter to set</param>
        /// <param name="key">Key of the counter to set</param>
        /// <param name="value">Value of the counter</param>
        /// <param name="traits">Any data traits for this document</param>
        /// <returns>Result of the set operation</returns>
        DataResult SetCounterValue(string dataLocation, string key, long value, IEnumerable<IDataTrait> traits);

        /// <summary>
        /// Increments a counter
        /// </summary>
        /// <param name="dataLocation">Location of the counter to increment</param>
        /// <param name="key">Key of the counter to increment</param>
        /// <param name="traits">Any data traits for this document</param>
        /// <param name="incrementValue">Value to increment by</param>
        /// <returns>Result of the operation</returns>
        DataRetrievalResult<long> IncrementCounter(string dataLocation, string key, IEnumerable<IDataTrait> traits, long incrementValue = 1);

        /// <summary>
        /// Delete the specified document
        /// </summary>
        /// <param name="dataLocation">Location of the document to delete</param>
        /// <param name="documentKey">Document key to delete</param>
        /// <param name="traits">Any data traits for this document</param>
        /// <returns>Result of the operation</returns>
        DataResult DeleteDocument(string dataLocation, string documentKey, IEnumerable<IDataTrait> traits);

        /// <summary>
        /// Check if the specified key exists in the datastore
        /// </summary>
        /// <param name="dataLocation">Location to search</param>
        /// <param name="key">Key to check</param>
        /// <param name="traits">Any data traits for this document</param>
        /// <returns>True if the key exists</returns>
        DataRetrievalResult<bool> KeyExists(string dataLocation, string key, IEnumerable<IDataTrait> traits);
    }
}
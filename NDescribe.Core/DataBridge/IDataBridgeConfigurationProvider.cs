﻿namespace NDescribe.Core.DataBridge
{
    /// <summary>
    /// Provides configuration for an IDataBridge
    /// </summary>
    public interface IDataBridgeConfigurationProvider
    {
        /// <summary>
        /// Create an IDataBridge based on the configuration
        /// </summary>
        /// <returns>The created IDataBridge</returns>
        IDataBridge Create();
    }
}
﻿using System;

namespace NDescribe.Core.DataBridge
{
    /// <summary>
    /// Result of a data retrieval operation
    /// </summary>
    /// <typeparam name="TDocument">Type of the object to retrieve</typeparam>
    public class DataRetrievalResult<TDocument>
    {
        /// <summary>
        /// Result of a data retrieval operation
        /// </summary>
        /// <param name="document">Retrieved document</param>
        /// <param name="success">Success of the operation</param>
        /// <param name="message">Message returned by the operation</param>
        /// <param name="exception">Exception returned by the operation</param>
        public DataRetrievalResult(TDocument document, bool success, string message, Exception exception)
        {
            Document = document;
            Success = success;
            Message = message;
            Exception = exception;
        }

        /// <summary>
        /// The serialized document
        /// </summary>
        public TDocument Document { get; private set; }

        /// <summary>
        /// Success or failure of the operation
        /// </summary>
        public bool Success { get; private set; }

        /// <summary>
        /// Message returned by the operation
        /// </summary>
        public string Message { get; private set; }

        /// <summary>
        /// Exception returned by the operation
        /// </summary>
        public Exception Exception { get; private set; }
    }
}
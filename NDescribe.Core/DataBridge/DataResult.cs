﻿using System;

namespace NDescribe.Core.DataBridge
{
    /// <summary>
    /// Result of a data operation
    /// </summary>
    public class DataResult
    {
        /// <summary>
        /// Result of a data operation
        /// </summary>
        /// <param name="success">Success of the operation</param>
        /// <param name="message">Message returned by the operation</param>
        /// <param name="exception">Exception returned by the operation</param>
        public DataResult(bool success, string message, Exception exception)
        {
            Success = success;
            Message = message;
            Exception = exception;
        }

        /// <summary>
        /// Success or failure of the operation
        /// </summary>
        public bool Success { get; private set; }

        /// <summary>
        /// Message returned by the operation
        /// </summary>
        public string Message { get; private set; }

        /// <summary>
        /// Exception returned by the operation
        /// </summary>
        public Exception Exception { get; private set; }
    }
}